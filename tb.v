 import "DPI" function void gen_image (input reg [31:0]  x[4*320*320-1:0] );

module tb;
reg clk,rst_n,valid_n;
wire ready_n,WRITE0,WRITE1,READ0,READ1;
integer latency;
reg [7:0] angle;
wire [31:0] addr_wr0, addr_wr1, addr_rd0, addr_rd1, datain0, datain1, dataout0, dataout1;
integer num_images,num_images_rcv;
event image_done;

reg temp_state;

rotate rotate_inst(.clk(clk), .rst_n(rst_n), .valid_n(valid_n) , .ready_n(ready_n), .angle(angle), .WRITE0(WRITE0) , .WRITE1(WRITE1),.READ0(READ0), .READ1(READ1), .addr_wr0(addr_wr0), .addr_wr1(addr_wr1), .addr_rd0(addr_rd0), .addr_rd1(addr_rd1), .datain0(datain0), .datain1(datain1), .dataout0(dataout0), .dataout1(dataout1));

ram ram(.clk(clk),.WRITE0(WRITE0),.WRITE1(WRITE1),.READ0(READ0),.READ1(READ1),.addr_wr0(addr_wr0),.addr_wr1(addr_wr1),.addr_rd0(addr_rd0),.addr_rd1(addr_rd1),.datain0(datain0) ,.datain1(datain1),.dataout0(dataout0),.dataout1(dataout1));


initial 
  begin
     $readmemh("mem.image",ram.ramdata);
     num_images= 5;                        //Number of images
     num_images_rcv=0; 
  end

// Initialization
initial begin
   valid_n = 1;
   clk=0; rst_n=1; #100;
   repeat(5) @(posedge clk);
   rst_n=0;  #1000;
   repeat(5) @(posedge clk);
   rst_n=1; 
   temp_state= 1;
   // Done with reset sequence

   repeat(num_images) begin
      @(posedge clk);#100;
      valid_n <= 0;
      //$display("valid_n going low in tb(), t= %f latency %d", $time, latency); 
      angle <= 10;                                                         //Angle value
      latency<=0;
      #100;
      @(posedge clk);
      valid_n <= 1; #100;
      repeat(5) @(posedge clk);
      // Wait for Image to be rotated;
      @(image_done);
   end
end


always @(posedge clk)  begin
  latency <=latency+1;
  if (ready_n==1'b0) begin
     $display("Received image, latency is %d",latency); //$finish;
     latency=0;
     num_images_rcv = num_images_rcv + 1;
     gen_image(ram.ramdata);
     if (num_images_rcv == num_images) $finish;
     ->image_done;
  end
end

// Clock Generator
always #15 clk = !clk;

//initial @(posedge rst_n) $display("fff");
initial 

  @(posedge rst_n)
  #100
    if (ready_n === 1'bz || ready_n === 1'bx) begin
      $display("[ERROR] Ready_n Error!!!  Please drive ready_n to 1'b1 at start of simulation!");
      $display("Your ready_n is %d",ready_n);
      $finish;
    end


initial forever begin
#100 @(negedge ready_n);
@(posedge clk);
@(posedge clk);
if (ready_n === 0) begin
  $display("[ERROR] Ready_n error, ready_n should go to 0 for 1 clock cycle!!!!");
  $finish;
end
end
endmodule



module ram (clk, WRITE0, WRITE1, READ0, READ1, addr_wr0, addr_wr1, addr_rd0, addr_rd1, datain0, datain1, dataout0, dataout1);
   input clk,WRITE0,WRITE1,READ0,READ1;
   input [31:0] addr_wr0,addr_wr1,addr_rd0,addr_rd1;
   input [31:0] datain0,datain1;
   output reg [31:0] dataout0,dataout1;
   reg [31:0]        ramdata [4*320*320-1:0];
   reg [17:0]        i;
   
   always @(posedge clk) 
     if  (READ0)
       begin
          dataout0 <= ramdata[addr_rd0];
          ramdata[addr_rd0] <= 0;
       end
   
   always @(posedge clk)
     if (READ1)
       begin
          dataout1 <= ramdata[addr_rd1];
          ramdata[addr_rd1] <= 0;
       end
   
   always @(posedge clk)
     if (WRITE0) ramdata[addr_wr0] <= datain0;
   
   always @(posedge clk)
     if (WRITE1) ramdata[addr_wr1] <= datain1;
   
endmodule
