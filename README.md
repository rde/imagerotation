Project: Image processor to rotate an image by any given angle for any 
given number of times

--------------------------------------------------------------
Copyright (C) 2013 by Ruchira De

PLEASE SEE LICENCE FILE FOR LICENCE RESTRICTIONS

Description:

This project implements a verilog module that rotates an image by a certain 
angle for a certain number of times.

User provides an image and the number of times the image is to be rotated.

The verilog test bench loads the image on the RAM and then rotates using a 
scratch space in the RAM. The code is synthesizable. 

The design closed on DC 2012.06 but on DC 2008.09 the WNS was -5.35 ns.

Code limitation: Image quality: The generated images have dots in them since 
the resolution of the sine and cosine of the angles is low.

To compile:
vcs rotate.v tb.v image.c -sverilog +v2k

To simulate:
./simv

Simulation output:
vcs simulator loads up the image file in RGB format specified in image.mem and 
produces 5 rotations (can be changed from the testbench tb.v) of the image. 
The rotated images are in ./output/

To synthesize:
dc_shell-t -f synthesize.tcl

Cell library:
A cell library is provided in lib/lsi_10k.v which is used by synthesize.tcl

Synthesis log:
Log for Synopsys DC synthesis are in the folder ./log/

Synthesized verilog:
The synthesized verilog is in the folder ./synthesis/

Questions/Comments:
For any question/comment related to the project send an email to the author:
Ruchira De ruchira.de@gmail.com
