set link_library { * ./lib/lsi_10k.db }
set target_library { ./lib/lsi_10k.db }
analyze -format Verilog rotate.v
elaborate rotate

create_clock clk -period 30
set_input_delay -clock clk 0 [all_inputs]
set_output_delay -clock clk 0 [all_outputs]

compile
report_area
report_timing
write -f verilog -hier -out rotate_impl.v
exit
