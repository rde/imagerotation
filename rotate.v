module rotate(input clk,rst_n, input valid_n, output reg ready_n, input [7:0] angle,
output WRITE0,WRITE1,READ0,READ1,
output reg  [31:0] addr_wr0,addr_wr1,addr_rd0,addr_rd1,
output reg [31:0] datain0,datain1,
input  [31:0] dataout0,dataout1);
   
   reg        data_valid,data_valid2,data_valid3,data_valid4; 
   reg [63:0] data,data2,data3;
   reg [4:0] state;
   wire [31:0] pixel_count, pixel_count2;
   
always @(posedge clk, negedge rst_n)
 if (!rst_n) begin
    ready_n <= 1; 
    state <= 0;
 end
 else 
   case (state) 
     0: 
       begin
          if (!valid_n)
            state <= 1; 
       end
     1:
       begin
          if (pixel_count2 == 320*320-1) 
            begin
               ready_n <= 0;  
               state <= 2; 
            end
       end
     2:
       begin
          ready_n <= 1;
          state <= 0;
       end
   endcase // case (state)

   /*
    always @(posedge clk, negedge rst_n)
    $monitor("Rotate pixel_count %d pixel_count2 %d\n", pixel_count, pixel_count2);
    */
   
   read_mem read_orig(clk,rst_n,valid_n,READ0,addr_rd0,dataout0,data,data_valid);
   write_mem_scratch #(18'h2_0000) write_scratch(clk, valid_n, data, data_valid, WRITE0, addr_wr0, datain0, pixel_count);
   read_mem #(18'h2_0000) read_scratch(clk, rst_n, !(pixel_count == 102399), READ1, addr_rd1, dataout1, data2, data_valid2);
   //Rotate around the center
   compute_xy_center rotated_scratch(clk, valid_n, data2, angle, data_valid2, data3, data_valid3, pixel_count2);
   write_mem write_orig (clk, data3, data_valid3, WRITE1,addr_wr1, datain1);
endmodule

module write_mem_scratch(input clk, input valid_n,
                         input [63:0]      data, input valid, 
                         output reg        WRITE, 
		         output reg [31:0] addr, output reg [31:0] datain, output reg [31:0] pixel_count); 
   parameter OFFSET = 0; 
   wire [8:0]                      x,y;
   
   assign x = data[49:41]; 
   assign y = data[40:32]; 
   
   always @(posedge clk)
     WRITE <= valid;
   
   always @(posedge clk) 
     // addr <= data[41:24];
     addr <= OFFSET | (x + y*320) ;

   always @(posedge clk)
     begin
        if(!valid_n) pixel_count <= 0;
        else
          if(valid)
            if (pixel_count == 102399) pixel_count <= 0;
            else pixel_count <= pixel_count + 1;
     end
   
   always @(posedge clk)
     datain <= data [31:0];
   
   /*
    initial
    $monitor("write_mem addr %b pixel %h x %d y %d\n", 
    addr, datain, x, y);
    */
endmodule

module write_mem(input clk, 
                 input [63:0]      data, input valid, 
                 output reg        WRITE, 
		 output reg [31:0] addr, output reg [31:0] datain);
   parameter OFFSET = 0; 
   reg [8:0]                       x,y;
   
   always @(posedge clk)
     WRITE <= valid;
   
   always @(posedge clk) 
     // addr <= data[41:24];
     begin
        if(data[49:41] >= 0 && data[49:41] <= 319)
          x <= data[49:41];
        else
          x <= 0;
        
        if(data[40:32] >= 0 && data[40:32] <= 319)
          y <= data[40:32];
        else
          y <= 0;
        
        addr <= OFFSET | (x + y*320) ;
     end
   
   always @(posedge clk)
     datain <= data [31:0];
   
   /*
    initial
    $monitor("write_mem addr %b pixel %h x %d y %d\n", 
    addr, datain, x, y);
    */
endmodule

module compute_xy_center(input clk, input valid_n, 
                         input [63:0]      data, input [7:0] angle, input data_valid, 
			 output reg [63:0] data2, output reg data_valid2, 
                         output reg [31:0] pixel_count); 
   // { x,y, datain }
   parameter OFFSET = 0;
   reg [31:0] 				    pixel;
   reg [15:0] 				    x, y;
   reg signed [15:0]                        xtemp, ytemp, xoffset, yoffset;
   reg [15:0]                               sin_val, cos_val;
   
   always @(posedge clk)
     begin
        //initialize the address to 0
        data2[63:0] <= 0;
        
        x <= data[49:41];
	y <= data[40:32];

        if(!valid_n)
          pixel_count <= 0;
        else
          if(data_valid)
            if (pixel_count == 102399) pixel_count <= 0;
            else pixel_count <= pixel_count + 1;

        /* Formula from paper FPGA Implementation of Image Rotation Using
	 Modified Compensated CORDIC Xiao-Gang JIANG, Jian-Yang ZHOU, Jiang-Hong SHI and Hui-Huang CHEN */
        xtemp <= (x*cos_val - y*sin_val)/64 + xoffset;
        ytemp <= (x*sin_val + y*cos_val)/64 + yoffset;
        
        data2[49:41] <= xtemp;
        data2[40:32] <= ytemp;
        data2[31:0]  <= data[31:0];
     end // always @ (posedge clk)
   
   always @(posedge clk) 
     data_valid2 <= data_valid;
   
   sine sine1(angle, sin_val);
   cosine cosine1(angle, cos_val);
   xcompute xcompute_inst(angle, xoffset);
   ycompute ycompute_inst(angle, yoffset);
endmodule // compute_xy_center

module xcompute(input [7:0] angle,
               output reg signed [15:0] offset);
   //offsetcosterm  <= OFFSET*cos_val;
   //offsetsinterm  <= OFFSET*sin_val;
   
   //xoffset <= -offsetcosterm + offsetsinterm + OFFSET;
   parameter OFFSET = 160;
   
   always @ (angle)
     begin
        case(angle)
          0: offset = 0;
          10: offset = -OFFSET*0.98480775301 + OFFSET*0.17364817766 + OFFSET;
          20: offset = -OFFSET*0.93969262078 + OFFSET*0.3420201433 + OFFSET;
          30: offset = -OFFSET*0.86602540378 + OFFSET*0.5000000000 + OFFSET;
          40: offset = -OFFSET*0.76604444311 + OFFSET*0.64278760968 + OFFSET;
          50: offset = -OFFSET*0.64278760968 + OFFSET*0.76604444311 + OFFSET;
          60: offset = -OFFSET*0.5 + OFFSET*0.86602540378 + OFFSET;
          70: offset = -OFFSET*0.34202014332 + OFFSET*0.93969262078 + OFFSET;
          80: offset = -OFFSET*0.17364817766 + OFFSET*0.98480775301 + OFFSET;
          90: offset =  OFFSET + OFFSET + 1;
          default: offset = 0;
        endcase // case (angle)
     end    
endmodule // xoffset

module ycompute(input [7:0] angle,
                output reg signed [15:0] offset);
   parameter OFFSET = 160;
   //offsetcosterm  <= OFFSET*cos_val;
   //offsetsinterm  <= OFFSET*sin_val;
   
    //yoffset <= -offsetsinterm - offsetcosterm + OFFSET;
   always @ (angle)
     begin
        case(angle)
          0: offset = 0;
          10: offset = -OFFSET*0.17364817766 - OFFSET*0.98480775301 + OFFSET;
          20: offset = -OFFSET*0.34202014332 - OFFSET*0.93969262078 + OFFSET;
          30: offset = -OFFSET*0.5000000000 - OFFSET*0.86602540378 + OFFSET;
          40: offset = -OFFSET*0.64278760968 - OFFSET*0.76604444311 + OFFSET;
          50: offset = -OFFSET*0.76604444311 - OFFSET*0.64278760968 + OFFSET;
          60: offset = -OFFSET*0.86602540378 - OFFSET*0.5 + OFFSET;
          70: offset = -OFFSET*0.93969262078 - OFFSET*0.34202014332 + OFFSET;
          80: offset = -OFFSET*0.98480775301 - OFFSET*0.17364817766 + OFFSET;
          90: offset = 0;
          default: offset = 0;
        endcase // case (angle)
     end       
endmodule

module sine(input [7:0] angle,
	    output reg [15:0] sin_val);

   always @ (angle)
     begin
	case(angle)
          0: sin_val = 0;
	  10: sin_val = 0.17364817766 * 64;
          20: sin_val = 0.34202014332 * 64;
          30: sin_val = 0.5000000000 * 64;
          40: sin_val = 0.64278760968 * 64;
          50: sin_val = 0.76604444311 * 64;
          60: sin_val = 0.86602540378 * 64;
          70: sin_val = 0.93969262078 * 64;
          80: sin_val = 0.98480775301 * 64;
	  90: sin_val = 1.0000000000 * 64;
          default: sin_val = 0;
	endcase // case (angle)
     end // always @ (angle)
   
endmodule // sine

module cosine(input [7:0] angle,
	      output reg [15:0] cos_val);
 
   always @ (angle)   
     begin
	case(angle)
	  0: cos_val = 1 * 64;
	  10: cos_val = 0.98480775301 * 64;
          20: cos_val = 0.93969262078 * 64;
          30: cos_val = 0.86602540378 * 64;
          40: cos_val = 0.76604444311 * 64;
          50: cos_val = 0.64278760968 * 64;
          60: cos_val = 0.5 * 64;
          70: cos_val = 0.34202014332 * 64;
          80: cos_val = 0.17364817766 * 64;
	  90: cos_val = 0;
          default: cos_val = 1 * 64;
	endcase // case (angle)
     end // always @ (angle)

endmodule // cosine

module  read_mem(input clk,rst_n,valid_n, 
		 output reg READ,output reg [31:0] addr, input [31:0] datain, 
		 output reg [63:0] data, output reg data_valid);
parameter OFFSET = 0; 
reg [31:0] index; 
reg [8:0] x,y; 
reg [3:0] state; 
reg [3:0] cycles;

   //Image is saved as M[y][x]
   //y and x both need how many bits ? x - 0 ... 319 , y : 0 ... 319, 319 ~ 2^9 bits  
   /*
    initial
    $monitor("clk %b READ %b addr %b x %b y %b datain %h \n", 
    clk, READ, addr, x, y, datain);
   */
always @(posedge clk,negedge rst_n) 
 if (!rst_n) begin 
   index<=0; 
   state <= 0; 
   READ <= 0; 
   data_valid <= 0; 
 end
 else 
 case (state)
   0: begin
      index<=0; x<=0; y<=0;
      if (!valid_n) state <= 1; 
   end
   1: begin
      READ <= 1; 
      addr <= OFFSET | index;
      state <= 2;
   end
   2: begin
      READ <= 0; 
      state <= 3; 
   end
   3: begin
      data <=  {  x,y, datain } ;
      data_valid <= 1'b1;
      state <= 4; 
   end
   4: begin
      index <= index + 1; 
      x <= (x==319) ? 0 : x+1; 
      y <= (x==319) ? y+1 : y;  
      data_valid <= 1'b0;
      if (index == 320*320-1) 
        begin 
           state <= 0;
        end 
      else
        begin
           state <= 1;
        end
   end // case: 4
 endcase

endmodule // read_mem
