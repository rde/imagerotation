
module compute_xy_center_DW01_sub_1 ( A, B, CI, DIFF, CO );
  input [14:0] A;
  input [14:0] B;
  output [14:0] DIFF;
  input CI;
  output CO;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44,
         n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58,
         n59, n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72,
         n73, n74, n75, n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86,
         n87;

  ND2 U3 ( .A(A[12]), .B(n39), .Z(n30) );
  ND2 U4 ( .A(A[7]), .B(n55), .Z(n16) );
  ND2 U5 ( .A(A[13]), .B(n34), .Z(n23) );
  IVAP U6 ( .A(A[13]), .Z(n27) );
  IVA U7 ( .A(B[13]), .Z(n34) );
  ND2 U8 ( .A(B[12]), .B(n40), .Z(n32) );
  ENP U9 ( .A(A[14]), .B(B[14]), .Z(n21) );
  EOP U10 ( .A(n21), .B(n22), .Z(DIFF[14]) );
  AO7 U11 ( .A(n28), .B(n29), .C(n30), .Z(n25) );
  ND2 U12 ( .A(n32), .B(n30), .Z(n3) );
  ND2 U13 ( .A(n35), .B(n36), .Z(n31) );
  ND2 U14 ( .A(n33), .B(n23), .Z(n7) );
  IVP U15 ( .A(A[10]), .Z(n87) );
  IVP U16 ( .A(A[11]), .Z(n47) );
  AO7P U17 ( .A(n49), .B(n50), .C(n13), .Z(n8) );
  IV U18 ( .A(n12), .Z(n50) );
  IV U19 ( .A(n32), .Z(n28) );
  ND2P U20 ( .A(n10), .B(n48), .Z(n44) );
  IV U21 ( .A(A[8]), .Z(n52) );
  IVA U22 ( .A(B[12]), .Z(n39) );
  IVA U23 ( .A(B[11]), .Z(n46) );
  EN U24 ( .A(n1), .B(n11), .Z(DIFF[8]) );
  ND2 U25 ( .A(n12), .B(n13), .Z(n1) );
  EN U26 ( .A(n2), .B(n14), .Z(DIFF[7]) );
  ND2 U27 ( .A(n15), .B(n16), .Z(n2) );
  ENP U28 ( .A(n3), .B(n31), .Z(DIFF[12]) );
  EN U29 ( .A(n4), .B(n37), .Z(DIFF[11]) );
  ND2 U30 ( .A(n38), .B(n35), .Z(n4) );
  EN U31 ( .A(n5), .B(n44), .Z(DIFF[10]) );
  ND2 U32 ( .A(n45), .B(n43), .Z(n5) );
  ND2 U33 ( .A(n37), .B(n38), .Z(n36) );
  EN U34 ( .A(n6), .B(n8), .Z(DIFF[9]) );
  ND2 U35 ( .A(n9), .B(n10), .Z(n6) );
  ND2 U36 ( .A(n9), .B(n8), .Z(n48) );
  IVP U37 ( .A(B[8]), .Z(n51) );
  IV U38 ( .A(B[7]), .Z(n55) );
  IV U39 ( .A(A[7]), .Z(n83) );
  EO U40 ( .A(n17), .B(n18), .Z(DIFF[6]) );
  ND2 U41 ( .A(n19), .B(n20), .Z(n18) );
  ND2 U42 ( .A(n25), .B(n26), .Z(n24) );
  ENP U43 ( .A(n7), .B(n25), .Z(DIFF[13]) );
  ND2 U44 ( .A(A[11]), .B(n46), .Z(n35) );
  IVP U45 ( .A(A[12]), .Z(n40) );
  ND2 U46 ( .A(A[10]), .B(n86), .Z(n43) );
  IV U47 ( .A(B[10]), .Z(n86) );
  IV U48 ( .A(A[9]), .Z(n84) );
  ND2 U49 ( .A(A[9]), .B(n85), .Z(n10) );
  IV U50 ( .A(B[9]), .Z(n85) );
  ND2 U51 ( .A(B[6]), .B(n82), .Z(n19) );
  IV U52 ( .A(A[6]), .Z(n82) );
  ND2 U53 ( .A(A[6]), .B(n57), .Z(n20) );
  IV U54 ( .A(B[6]), .Z(n57) );
  ND2 U55 ( .A(n63), .B(n64), .Z(n60) );
  ND2 U56 ( .A(B[4]), .B(n66), .Z(n63) );
  ND2 U57 ( .A(B[5]), .B(n65), .Z(n64) );
  ND2 U58 ( .A(A[4]), .B(n67), .Z(n66) );
  IV U59 ( .A(n58), .Z(n17) );
  AO7 U60 ( .A(n59), .B(n60), .C(n61), .Z(n58) );
  NR2 U61 ( .A(n67), .B(A[4]), .Z(n59) );
  ND2 U62 ( .A(A[5]), .B(n62), .Z(n61) );
  IVA U63 ( .A(B[5]), .Z(n62) );
  ND2 U64 ( .A(n68), .B(n69), .Z(n67) );
  ND2 U65 ( .A(A[3]), .B(n81), .Z(n68) );
  AO3 U66 ( .A(n70), .B(n71), .C(n72), .D(n73), .Z(n69) );
  IV U67 ( .A(B[3]), .Z(n81) );
  ND2 U68 ( .A(B[3]), .B(n74), .Z(n72) );
  IVP U69 ( .A(A[3]), .Z(n74) );
  IV U70 ( .A(A[5]), .Z(n65) );
  NR2 U71 ( .A(B[2]), .B(n80), .Z(n70) );
  IVP U72 ( .A(A[2]), .Z(n80) );
  ND2 U73 ( .A(B[2]), .B(n80), .Z(n73) );
  AO7 U74 ( .A(n75), .B(n76), .C(n77), .Z(n71) );
  NR2 U75 ( .A(A[0]), .B(n78), .Z(n76) );
  NR2 U76 ( .A(A[1]), .B(n79), .Z(n75) );
  ND2 U77 ( .A(A[1]), .B(n79), .Z(n77) );
  IVP U78 ( .A(B[1]), .Z(n79) );
  NR2 U79 ( .A(1'b0), .B(B[0]), .Z(n78) );
  ND2 U80 ( .A(B[13]), .B(n27), .Z(n33) );
  ND2 U81 ( .A(B[13]), .B(n27), .Z(n26) );
  ND2 U83 ( .A(n23), .B(n24), .Z(n22) );
  IVA U84 ( .A(n31), .Z(n29) );
  AO7P U85 ( .A(n41), .B(n42), .C(n43), .Z(n37) );
  IVA U86 ( .A(n44), .Z(n42) );
  IVA U87 ( .A(n45), .Z(n41) );
  ND2P U88 ( .A(B[11]), .B(n47), .Z(n38) );
  ND2P U89 ( .A(A[8]), .B(n51), .Z(n13) );
  ND2P U90 ( .A(B[8]), .B(n52), .Z(n12) );
  IVA U91 ( .A(n11), .Z(n49) );
  AO7P U92 ( .A(n53), .B(n54), .C(n16), .Z(n11) );
  IVA U93 ( .A(n14), .Z(n54) );
  AO7P U94 ( .A(n56), .B(n17), .C(n20), .Z(n14) );
  IVA U95 ( .A(n19), .Z(n56) );
  IVA U96 ( .A(n15), .Z(n53) );
  ND2P U97 ( .A(B[7]), .B(n83), .Z(n15) );
  ND2P U98 ( .A(B[9]), .B(n84), .Z(n9) );
  ND2P U99 ( .A(B[10]), .B(n87), .Z(n45) );
endmodule


module compute_xy_center_DW01_add_9 ( A, B, CI, SUM, CO );
  input [8:0] A;
  input [8:0] B;
  output [8:0] SUM;
  input CI;
  output CO;
  wire   n2, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18,
         n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32,
         n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46,
         n47, n48, n49;

  AN2P U2 ( .A(n47), .B(n46), .Z(SUM[0]) );
  ND2P U3 ( .A(B[7]), .B(A[7]), .Z(n12) );
  AN2P U4 ( .A(n12), .B(n13), .Z(n11) );
  B2I U5 ( .A(n14), .Z2(n2) );
  IVP U6 ( .A(n20), .Z(n16) );
  ENP U7 ( .A(n15), .B(n2), .Z(SUM[7]) );
  ND2 U8 ( .A(B[6]), .B(A[6]), .Z(n18) );
  OR2P U9 ( .A(B[3]), .B(A[3]), .Z(n35) );
  ND2 U10 ( .A(n41), .B(n42), .Z(n39) );
  ND2P U11 ( .A(n23), .B(n24), .Z(n22) );
  OR2P U12 ( .A(B[4]), .B(A[4]), .Z(n31) );
  OR2P U13 ( .A(B[1]), .B(A[1]), .Z(n44) );
  IVDA U14 ( .A(A[0]), .Y(n49) );
  OR2P U15 ( .A(B[2]), .B(A[2]), .Z(n40) );
  ND2 U16 ( .A(n43), .B(n44), .Z(n42) );
  EN U17 ( .A(n45), .B(n43), .Z(SUM[1]) );
  ND2 U18 ( .A(n44), .B(n41), .Z(n45) );
  ND2 U19 ( .A(n34), .B(n35), .Z(n33) );
  EN U20 ( .A(n5), .B(n23), .Z(SUM[5]) );
  ND2 U21 ( .A(n24), .B(n21), .Z(n5) );
  ND2 U22 ( .A(B[1]), .B(A[1]), .Z(n41) );
  EN U23 ( .A(n6), .B(n30), .Z(SUM[4]) );
  ND2 U24 ( .A(n31), .B(n29), .Z(n6) );
  IVA U25 ( .A(n46), .Z(n43) );
  EN U26 ( .A(n7), .B(n34), .Z(SUM[3]) );
  ND2 U27 ( .A(n35), .B(n32), .Z(n7) );
  EN U28 ( .A(n8), .B(n39), .Z(SUM[2]) );
  ND2 U29 ( .A(n40), .B(n38), .Z(n8) );
  OR2 U30 ( .A(B[5]), .B(A[5]), .Z(n24) );
  ND2 U31 ( .A(B[5]), .B(A[5]), .Z(n21) );
  ND2 U32 ( .A(B[4]), .B(A[4]), .Z(n29) );
  EN U33 ( .A(n9), .B(n19), .Z(SUM[6]) );
  ND2 U34 ( .A(n20), .B(n18), .Z(n9) );
  ND2 U35 ( .A(B[3]), .B(A[3]), .Z(n32) );
  ND2 U36 ( .A(B[2]), .B(A[2]), .Z(n38) );
  ND2 U37 ( .A(B[0]), .B(A[0]), .Z(n46) );
  ND2 U38 ( .A(n48), .B(n49), .Z(n47) );
  IVP U39 ( .A(B[0]), .Z(n48) );
  IVP U40 ( .A(B[6]), .Z(n25) );
  IVP U41 ( .A(A[6]), .Z(n26) );
  AO7 U42 ( .A(A[7]), .B(B[7]), .C(n14), .Z(n13) );
  AO7 U43 ( .A(A[7]), .B(B[7]), .C(n12), .Z(n15) );
  EOP U44 ( .A(n10), .B(n11), .Z(SUM[8]) );
  ENP U45 ( .A(A[8]), .B(B[8]), .Z(n10) );
  AO7P U46 ( .A(n16), .B(n17), .C(n18), .Z(n14) );
  IVA U47 ( .A(n19), .Z(n17) );
  ND2P U48 ( .A(n21), .B(n22), .Z(n19) );
  ND2P U49 ( .A(n25), .B(n26), .Z(n20) );
  AO7P U50 ( .A(n27), .B(n28), .C(n29), .Z(n23) );
  IVA U51 ( .A(n30), .Z(n28) );
  IVA U52 ( .A(n31), .Z(n27) );
  ND2P U53 ( .A(n32), .B(n33), .Z(n30) );
  AO7P U54 ( .A(n36), .B(n37), .C(n38), .Z(n34) );
  IVA U55 ( .A(n39), .Z(n37) );
  IVA U56 ( .A(n40), .Z(n36) );
endmodule


module compute_xy_center_DW01_add_8 ( A, B, CI, SUM, CO );
  input [14:0] A;
  input [14:0] B;
  output [14:0] SUM;
  input CI;
  output CO;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44,
         n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58,
         n59, n60, n61, n62, n63;

  ND2 U2 ( .A(B[7]), .B(A[7]), .Z(n17) );
  ND2 U3 ( .A(B[9]), .B(A[9]), .Z(n11) );
  ND2 U4 ( .A(n47), .B(n48), .Z(n18) );
  AN2P U5 ( .A(n20), .B(n46), .Z(n1) );
  AN2 U6 ( .A(n53), .B(n54), .Z(n2) );
  ND2P U7 ( .A(n28), .B(n26), .Z(n27) );
  OR2P U8 ( .A(A[13]), .B(B[13]), .Z(n26) );
  EOP U9 ( .A(A[14]), .B(B[14]), .Z(n21) );
  EOP U10 ( .A(n21), .B(n22), .Z(SUM[14]) );
  ENP U11 ( .A(n5), .B(n40), .Z(SUM[10]) );
  ND2P U12 ( .A(n29), .B(n30), .Z(n25) );
  ENP U13 ( .A(n4), .B(n31), .Z(SUM[12]) );
  ND2P U14 ( .A(n32), .B(n29), .Z(n4) );
  ENP U15 ( .A(n3), .B(n36), .Z(SUM[11]) );
  ND2P U16 ( .A(n37), .B(n35), .Z(n3) );
  AO7P U17 ( .A(n33), .B(n34), .C(n35), .Z(n31) );
  IV U18 ( .A(n37), .Z(n33) );
  OR2P U19 ( .A(B[9]), .B(A[9]), .Z(n10) );
  OR2P U20 ( .A(B[6]), .B(A[6]), .Z(n19) );
  OR2 U21 ( .A(B[3]), .B(A[3]), .Z(n58) );
  ND2 U22 ( .A(n41), .B(n38), .Z(n5) );
  ND2 U23 ( .A(n31), .B(n32), .Z(n30) );
  EN U24 ( .A(n6), .B(n9), .Z(SUM[9]) );
  ND2 U25 ( .A(n10), .B(n11), .Z(n6) );
  EN U26 ( .A(n7), .B(n12), .Z(SUM[8]) );
  ND2 U27 ( .A(n13), .B(n14), .Z(n7) );
  ND2 U28 ( .A(n38), .B(n39), .Z(n36) );
  ND2 U29 ( .A(n40), .B(n41), .Z(n39) );
  EO U30 ( .A(n1), .B(n15), .Z(SUM[7]) );
  ND2 U31 ( .A(n16), .B(n17), .Z(n15) );
  ND2 U32 ( .A(n14), .B(n44), .Z(n9) );
  ND2 U33 ( .A(n13), .B(n12), .Z(n44) );
  ND2 U34 ( .A(n18), .B(n19), .Z(n46) );
  EN U35 ( .A(n8), .B(n18), .Z(SUM[6]) );
  ND2 U36 ( .A(n19), .B(n20), .Z(n8) );
  NR2 U37 ( .A(n2), .B(n52), .Z(n49) );
  ND2 U38 ( .A(B[13]), .B(A[13]), .Z(n28) );
  OR2 U39 ( .A(B[12]), .B(A[12]), .Z(n32) );
  ND2 U40 ( .A(B[12]), .B(A[12]), .Z(n29) );
  OR2 U41 ( .A(B[11]), .B(A[11]), .Z(n37) );
  ND2 U42 ( .A(B[11]), .B(A[11]), .Z(n35) );
  OR2 U43 ( .A(B[10]), .B(A[10]), .Z(n41) );
  ND2 U44 ( .A(B[10]), .B(A[10]), .Z(n38) );
  OR2 U45 ( .A(B[8]), .B(A[8]), .Z(n13) );
  OR2 U46 ( .A(B[7]), .B(A[7]), .Z(n16) );
  ND2 U47 ( .A(B[8]), .B(A[8]), .Z(n14) );
  ND2 U48 ( .A(B[5]), .B(A[5]), .Z(n47) );
  AO3 U49 ( .A(B[4]), .B(n49), .C(n50), .D(n51), .Z(n48) );
  ND2 U50 ( .A(n2), .B(n52), .Z(n50) );
  ND2 U51 ( .A(B[6]), .B(A[6]), .Z(n20) );
  OR2 U52 ( .A(B[5]), .B(A[5]), .Z(n51) );
  ND2 U53 ( .A(B[3]), .B(A[3]), .Z(n53) );
  AO3 U54 ( .A(n55), .B(n56), .C(n57), .D(n58), .Z(n54) );
  IV U55 ( .A(A[4]), .Z(n52) );
  ND2 U56 ( .A(n59), .B(n60), .Z(n57) );
  IVP U57 ( .A(B[2]), .Z(n59) );
  IVP U58 ( .A(A[2]), .Z(n60) );
  AO7 U59 ( .A(n61), .B(n62), .C(n63), .Z(n56) );
  AO5 U60 ( .A(1'b0), .B(A[0]), .C(B[0]), .Z(n62) );
  NR2 U61 ( .A(A[1]), .B(B[1]), .Z(n61) );
  ND2 U62 ( .A(B[1]), .B(A[1]), .Z(n63) );
  AO7P U64 ( .A(n23), .B(n24), .C(n28), .Z(n22) );
  IVA U65 ( .A(n25), .Z(n24) );
  IVA U66 ( .A(n26), .Z(n23) );
  ENP U67 ( .A(n25), .B(n27), .Z(SUM[13]) );
  IVA U68 ( .A(n36), .Z(n34) );
  AO7P U69 ( .A(n42), .B(n43), .C(n11), .Z(n40) );
  IVA U70 ( .A(n10), .Z(n43) );
  IVA U71 ( .A(n9), .Z(n42) );
  AO7P U72 ( .A(n45), .B(n1), .C(n17), .Z(n12) );
  AN2P U73 ( .A(B[2]), .B(A[2]), .Z(n55) );
  IVA U74 ( .A(n16), .Z(n45) );
endmodule


module compute_xy_center_DW01_add_7 ( A, B, CI, SUM, CO );
  input [8:0] A;
  input [8:0] B;
  output [8:0] SUM;
  input CI;
  output CO;
  wire   n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42;

  AO7P U2 ( .A(A[7]), .B(B[8]), .C(n9), .Z(n12) );
  AN2P U3 ( .A(n42), .B(n41), .Z(SUM[0]) );
  AO7P U4 ( .A(n31), .B(n32), .C(n33), .Z(n29) );
  ND2 U5 ( .A(B[6]), .B(A[6]), .Z(n15) );
  IV U6 ( .A(n35), .Z(n31) );
  ND2P U7 ( .A(A[7]), .B(B[8]), .Z(n9) );
  ENP U8 ( .A(n12), .B(n11), .Z(SUM[7]) );
  OR2P U9 ( .A(B[6]), .B(A[6]), .Z(n17) );
  OR2P U10 ( .A(B[5]), .B(A[5]), .Z(n21) );
  OR2P U11 ( .A(B[4]), .B(A[4]), .Z(n26) );
  OR2P U12 ( .A(B[2]), .B(A[2]), .Z(n35) );
  OR2P U13 ( .A(B[3]), .B(A[3]), .Z(n30) );
  OR2P U14 ( .A(B[1]), .B(A[1]), .Z(n39) );
  ND2 U15 ( .A(n20), .B(n21), .Z(n19) );
  EN U16 ( .A(n2), .B(n16), .Z(SUM[6]) );
  ND2 U17 ( .A(n17), .B(n15), .Z(n2) );
  ND2 U18 ( .A(n29), .B(n30), .Z(n28) );
  ND2 U19 ( .A(n38), .B(n39), .Z(n37) );
  EN U20 ( .A(n3), .B(n20), .Z(SUM[5]) );
  ND2 U21 ( .A(n21), .B(n18), .Z(n3) );
  IVA U22 ( .A(n41), .Z(n38) );
  EN U23 ( .A(n4), .B(n25), .Z(SUM[4]) );
  ND2 U24 ( .A(n26), .B(n24), .Z(n4) );
  EN U25 ( .A(n5), .B(n29), .Z(SUM[3]) );
  ND2 U26 ( .A(n30), .B(n27), .Z(n5) );
  EN U27 ( .A(n6), .B(n34), .Z(SUM[2]) );
  ND2 U28 ( .A(n35), .B(n33), .Z(n6) );
  EN U29 ( .A(n40), .B(n38), .Z(SUM[1]) );
  ND2 U30 ( .A(n39), .B(n36), .Z(n40) );
  ND2 U31 ( .A(B[5]), .B(A[5]), .Z(n18) );
  ND2 U32 ( .A(B[4]), .B(A[4]), .Z(n24) );
  ND2 U33 ( .A(B[3]), .B(A[3]), .Z(n27) );
  ND2 U34 ( .A(B[2]), .B(A[2]), .Z(n33) );
  ND2 U35 ( .A(B[8]), .B(A[0]), .Z(n41) );
  ND2 U36 ( .A(B[1]), .B(A[1]), .Z(n36) );
  EOP U37 ( .A(n7), .B(n8), .Z(SUM[8]) );
  EOP U38 ( .A(A[8]), .B(B[8]), .Z(n7) );
  ND2 U39 ( .A(n9), .B(n10), .Z(n8) );
  AO7 U40 ( .A(A[7]), .B(B[8]), .C(n11), .Z(n10) );
  AO7P U41 ( .A(n13), .B(n14), .C(n15), .Z(n11) );
  IVA U42 ( .A(n16), .Z(n14) );
  IVA U43 ( .A(n17), .Z(n13) );
  ND2P U44 ( .A(n18), .B(n19), .Z(n16) );
  AO7P U45 ( .A(n22), .B(n23), .C(n24), .Z(n20) );
  IVA U46 ( .A(n25), .Z(n23) );
  IVA U47 ( .A(n26), .Z(n22) );
  ND2P U48 ( .A(n27), .B(n28), .Z(n25) );
  IVA U49 ( .A(n34), .Z(n32) );
  ND2P U50 ( .A(n36), .B(n37), .Z(n34) );
  OR2 U51 ( .A(A[0]), .B(B[8]), .Z(n42) );
endmodule


module compute_xy_center_DW02_mult_3 ( A, B, TC, PRODUCT );
  input [14:0] A;
  input [14:0] B;
  output [29:0] PRODUCT;
  input TC;
  wire   \ab[8][5] , \ab[8][4] , \ab[8][3] , \ab[8][2] , \ab[8][1] ,
         \ab[8][0] , \ab[7][5] , \ab[7][4] , \ab[7][3] , \ab[7][2] ,
         \ab[7][1] , \ab[7][0] , \ab[6][5] , \ab[6][4] , \ab[6][3] ,
         \ab[6][2] , \ab[6][1] , \ab[6][0] , \ab[5][5] , \ab[5][4] ,
         \ab[5][3] , \ab[5][2] , \ab[5][1] , \ab[5][0] , \ab[4][5] ,
         \ab[4][4] , \ab[4][3] , \ab[4][2] , \ab[4][1] , \ab[4][0] ,
         \ab[3][5] , \ab[3][4] , \ab[3][3] , \ab[3][2] , \ab[3][1] ,
         \ab[3][0] , \ab[2][5] , \ab[2][4] , \ab[2][3] , \ab[2][2] ,
         \ab[2][1] , \ab[2][0] , \ab[1][6] , \ab[1][5] , \ab[1][4] ,
         \ab[1][3] , \ab[1][2] , \ab[1][1] , \ab[1][0] , \ab[0][6] ,
         \ab[0][5] , \ab[0][4] , \ab[0][3] , \ab[0][2] , \ab[0][1] ,
         \CARRYB[13][0] , \CARRYB[12][1] , \CARRYB[12][0] , \CARRYB[11][2] ,
         \CARRYB[11][1] , \CARRYB[11][0] , \CARRYB[10][3] , \CARRYB[10][2] ,
         \CARRYB[10][1] , \CARRYB[10][0] , \CARRYB[9][4] , \CARRYB[9][3] ,
         \CARRYB[9][2] , \CARRYB[9][1] , \CARRYB[9][0] , \CARRYB[8][5] ,
         \CARRYB[8][4] , \CARRYB[8][3] , \CARRYB[8][2] , \CARRYB[8][1] ,
         \CARRYB[8][0] , \CARRYB[7][5] , \CARRYB[7][4] , \CARRYB[7][3] ,
         \CARRYB[7][2] , \CARRYB[7][1] , \CARRYB[7][0] , \CARRYB[6][5] ,
         \CARRYB[6][4] , \CARRYB[6][3] , \CARRYB[6][2] , \CARRYB[6][1] ,
         \CARRYB[6][0] , \CARRYB[5][5] , \CARRYB[5][4] , \CARRYB[5][3] ,
         \CARRYB[5][2] , \CARRYB[5][1] , \CARRYB[5][0] , \CARRYB[4][5] ,
         \CARRYB[4][4] , \CARRYB[4][3] , \CARRYB[4][2] , \CARRYB[4][1] ,
         \CARRYB[4][0] , \CARRYB[3][5] , \CARRYB[3][4] , \CARRYB[3][3] ,
         \CARRYB[3][2] , \CARRYB[3][1] , \CARRYB[3][0] , \CARRYB[2][5] ,
         \CARRYB[2][4] , \CARRYB[2][3] , \CARRYB[2][2] , \CARRYB[2][1] ,
         \CARRYB[2][0] , \CARRYB[1][5] , \CARRYB[1][4] , \CARRYB[1][3] ,
         \CARRYB[1][2] , \CARRYB[1][1] , \CARRYB[1][0] , \SUMB[13][1] ,
         \SUMB[12][2] , \SUMB[12][1] , \SUMB[11][3] , \SUMB[11][2] ,
         \SUMB[11][1] , \SUMB[10][4] , \SUMB[10][3] , \SUMB[10][2] ,
         \SUMB[10][1] , \SUMB[9][5] , \SUMB[9][4] , \SUMB[9][3] , \SUMB[9][2] ,
         \SUMB[9][1] , \SUMB[8][6] , \SUMB[8][5] , \SUMB[8][4] , \SUMB[8][3] ,
         \SUMB[8][2] , \SUMB[8][1] , \SUMB[7][6] , \SUMB[7][5] , \SUMB[7][4] ,
         \SUMB[7][3] , \SUMB[7][2] , \SUMB[7][1] , \SUMB[6][6] , \SUMB[6][5] ,
         \SUMB[6][4] , \SUMB[6][3] , \SUMB[6][2] , \SUMB[6][1] , \SUMB[5][6] ,
         \SUMB[5][5] , \SUMB[5][4] , \SUMB[5][3] , \SUMB[5][2] , \SUMB[5][1] ,
         \SUMB[4][6] , \SUMB[4][5] , \SUMB[4][4] , \SUMB[4][3] , \SUMB[4][2] ,
         \SUMB[4][1] , \SUMB[3][6] , \SUMB[3][5] , \SUMB[3][4] , \SUMB[3][3] ,
         \SUMB[3][2] , \SUMB[3][1] , \SUMB[2][6] , \SUMB[2][5] , \SUMB[2][4] ,
         \SUMB[2][3] , \SUMB[2][2] , \SUMB[2][1] , \SUMB[1][6] , \SUMB[1][5] ,
         \SUMB[1][4] , \SUMB[1][3] , \SUMB[1][2] , \SUMB[1][1] , n7, n8, n11,
         n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25,
         n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39,
         n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52, n53,
         n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n65, n66;

  FA1AP S2_13_1 ( .A(\CARRYB[12][1] ), .B(1'b0), .CI(\SUMB[12][2] ), .S(
        \SUMB[13][1] ) );
  FA1AP S2_12_2 ( .A(1'b0), .B(\CARRYB[11][2] ), .CI(\SUMB[11][3] ), .S(
        \SUMB[12][2] ) );
  FA1AP S2_10_3 ( .A(1'b0), .B(\CARRYB[9][3] ), .CI(\SUMB[9][4] ), .CO(
        \CARRYB[10][3] ), .S(\SUMB[10][3] ) );
  FA1AP S2_9_3 ( .A(1'b0), .B(\CARRYB[8][3] ), .CI(\SUMB[8][4] ), .CO(
        \CARRYB[9][3] ), .S(\SUMB[9][3] ) );
  FA1AP S2_7_4 ( .A(\ab[7][4] ), .B(\CARRYB[6][4] ), .CI(\SUMB[6][5] ), .CO(
        \CARRYB[7][4] ), .S(\SUMB[7][4] ) );
  FA1AP S2_7_5 ( .A(\ab[7][5] ), .B(\CARRYB[6][5] ), .CI(\SUMB[6][6] ), .CO(
        \CARRYB[7][5] ), .S(\SUMB[7][5] ) );
  FA1AP S2_5_5 ( .A(\ab[5][5] ), .B(\CARRYB[4][5] ), .CI(\SUMB[4][6] ), .CO(
        \CARRYB[5][5] ), .S(\SUMB[5][5] ) );
  FA1AP S4_0 ( .A(\CARRYB[13][0] ), .B(1'b0), .CI(\SUMB[13][1] ), .S(
        PRODUCT[14]) );
  FA1P S1_9_0 ( .A(\CARRYB[8][0] ), .B(1'b0), .CI(\SUMB[8][1] ), .CO(
        \CARRYB[9][0] ), .S(PRODUCT[9]) );
  FA1 S2_6_3 ( .A(\CARRYB[5][3] ), .B(\ab[6][3] ), .CI(\SUMB[5][4] ), .CO(
        \CARRYB[6][3] ), .S(\SUMB[6][3] ) );
  FA1P S1_6_0 ( .A(\ab[6][0] ), .B(\CARRYB[5][0] ), .CI(\SUMB[5][1] ), .CO(
        \CARRYB[6][0] ), .S(PRODUCT[6]) );
  FA1P S1_10_0 ( .A(1'b0), .B(\CARRYB[9][0] ), .CI(\SUMB[9][1] ), .CO(
        \CARRYB[10][0] ), .S(PRODUCT[10]) );
  FA1 S2_3_1 ( .A(\ab[3][1] ), .B(\CARRYB[2][1] ), .CI(\SUMB[2][2] ), .CO(
        \CARRYB[3][1] ), .S(\SUMB[3][1] ) );
  FA1A S2_4_1 ( .A(\ab[4][1] ), .B(\CARRYB[3][1] ), .CI(\SUMB[3][2] ), .CO(
        \CARRYB[4][1] ), .S(\SUMB[4][1] ) );
  FA1A S2_11_1 ( .A(\CARRYB[10][1] ), .B(1'b0), .CI(\SUMB[10][2] ), .CO(
        \CARRYB[11][1] ), .S(\SUMB[11][1] ) );
  FA1AP S2_12_1 ( .A(\CARRYB[11][1] ), .B(1'b0), .CI(\SUMB[11][2] ), .CO(
        \CARRYB[12][1] ), .S(\SUMB[12][1] ) );
  FA1A S2_6_5 ( .A(\ab[6][5] ), .B(\CARRYB[5][5] ), .CI(\SUMB[5][6] ), .CO(
        \CARRYB[6][5] ), .S(\SUMB[6][5] ) );
  FA1A S1_2_0 ( .A(\ab[2][0] ), .B(\CARRYB[1][0] ), .CI(\SUMB[1][1] ), .CO(
        \CARRYB[2][0] ), .S(PRODUCT[2]) );
  EO3 S0_6 ( .A(1'b0), .B(\ab[1][6] ), .C(1'b0), .Z(\SUMB[1][6] ) );
  EO3 S2_11_3 ( .A(1'b0), .B(\CARRYB[10][3] ), .C(\SUMB[10][4] ), .Z(
        \SUMB[11][3] ) );
  FA1A S0_1 ( .A(\ab[0][2] ), .B(\ab[1][1] ), .CI(1'b0), .CO(\CARRYB[1][1] ), 
        .S(\SUMB[1][1] ) );
  FA1P S2_2_1 ( .A(\ab[2][1] ), .B(\CARRYB[1][1] ), .CI(\SUMB[1][2] ), .CO(
        \CARRYB[2][1] ), .S(\SUMB[2][1] ) );
  FA1A S0_0 ( .A(\ab[0][1] ), .B(\ab[1][0] ), .CI(1'b0), .CO(\CARRYB[1][0] ), 
        .S(PRODUCT[1]) );
  EO3 S2_10_4 ( .A(\CARRYB[9][4] ), .B(1'b0), .C(\SUMB[9][5] ), .Z(
        \SUMB[10][4] ) );
  FA1P S2_5_1 ( .A(\ab[5][1] ), .B(\CARRYB[4][1] ), .CI(\SUMB[4][2] ), .CO(
        \CARRYB[5][1] ), .S(\SUMB[5][1] ) );
  FA1 S0_2 ( .A(\ab[0][3] ), .B(\ab[1][2] ), .CI(1'b0), .CO(\CARRYB[1][2] ), 
        .S(\SUMB[1][2] ) );
  FA1A S2_11_2 ( .A(1'b0), .B(\CARRYB[10][2] ), .CI(\SUMB[10][3] ), .CO(
        \CARRYB[11][2] ), .S(\SUMB[11][2] ) );
  FA1AP S2_2_5 ( .A(\CARRYB[1][5] ), .B(\ab[2][5] ), .CI(\SUMB[1][6] ), .CO(
        \CARRYB[2][5] ), .S(\SUMB[2][5] ) );
  FA1 S2_8_3 ( .A(\ab[8][3] ), .B(\CARRYB[7][3] ), .CI(\SUMB[7][4] ), .CO(
        \CARRYB[8][3] ), .S(\SUMB[8][3] ) );
  FA1A S2_9_4 ( .A(1'b0), .B(\CARRYB[8][4] ), .CI(\SUMB[8][5] ), .CO(
        \CARRYB[9][4] ), .S(\SUMB[9][4] ) );
  FA1AP S2_5_3 ( .A(\ab[5][3] ), .B(\SUMB[4][4] ), .CI(\CARRYB[4][3] ), .CO(
        \CARRYB[5][3] ), .S(\SUMB[5][3] ) );
  FA1P S2_2_4 ( .A(\ab[2][4] ), .B(\CARRYB[1][4] ), .CI(\SUMB[1][5] ), .CO(
        \CARRYB[2][4] ), .S(\SUMB[2][4] ) );
  FA1P S2_6_1 ( .A(\CARRYB[5][1] ), .B(\ab[6][1] ), .CI(\SUMB[5][2] ), .CO(
        \CARRYB[6][1] ), .S(\SUMB[6][1] ) );
  FA1P S2_10_2 ( .A(\CARRYB[9][2] ), .B(1'b0), .CI(\SUMB[9][3] ), .CO(
        \CARRYB[10][2] ), .S(\SUMB[10][2] ) );
  FA1 S1_5_0 ( .A(\ab[5][0] ), .B(\CARRYB[4][0] ), .CI(\SUMB[4][1] ), .CO(
        \CARRYB[5][0] ), .S(PRODUCT[5]) );
  FA1 S1_3_0 ( .A(\ab[3][0] ), .B(\CARRYB[2][0] ), .CI(\SUMB[2][1] ), .CO(
        \CARRYB[3][0] ), .S(PRODUCT[3]) );
  FA1 S1_4_0 ( .A(\ab[4][0] ), .B(\CARRYB[3][0] ), .CI(\SUMB[3][1] ), .CO(
        \CARRYB[4][0] ), .S(PRODUCT[4]) );
  FA1P S2_5_4 ( .A(\CARRYB[4][4] ), .B(\ab[5][4] ), .CI(\SUMB[4][5] ), .CO(
        \CARRYB[5][4] ), .S(\SUMB[5][4] ) );
  FA1AP S2_7_3 ( .A(\CARRYB[6][3] ), .B(\ab[7][3] ), .CI(\SUMB[6][4] ), .CO(
        \CARRYB[7][3] ), .S(\SUMB[7][3] ) );
  FA1P S0_3 ( .A(\ab[0][4] ), .B(\ab[1][3] ), .CI(1'b0), .CO(\CARRYB[1][3] ), 
        .S(\SUMB[1][3] ) );
  FA1P S0_4 ( .A(\ab[0][5] ), .B(\ab[1][4] ), .CI(1'b0), .CO(\CARRYB[1][4] ), 
        .S(\SUMB[1][4] ) );
  FA1A S2_4_5 ( .A(\ab[4][5] ), .B(\CARRYB[3][5] ), .CI(\SUMB[3][6] ), .CO(
        \CARRYB[4][5] ), .S(\SUMB[4][5] ) );
  FA1A S2_9_2 ( .A(\CARRYB[8][2] ), .B(1'b0), .CI(\SUMB[8][3] ), .CO(
        \CARRYB[9][2] ), .S(\SUMB[9][2] ) );
  FA1A S2_6_4 ( .A(\ab[6][4] ), .B(\CARRYB[5][4] ), .CI(\SUMB[5][5] ), .CO(
        \CARRYB[6][4] ), .S(\SUMB[6][4] ) );
  FA1 S2_8_2 ( .A(\CARRYB[7][2] ), .B(\ab[8][2] ), .CI(\SUMB[7][3] ), .CO(
        \CARRYB[8][2] ), .S(\SUMB[8][2] ) );
  FA1P S2_4_3 ( .A(\CARRYB[3][3] ), .B(\ab[4][3] ), .CI(\SUMB[3][4] ), .CO(
        \CARRYB[4][3] ), .S(\SUMB[4][3] ) );
  FA1 S2_8_4 ( .A(\ab[8][4] ), .B(\CARRYB[7][4] ), .CI(\SUMB[7][5] ), .CO(
        \CARRYB[8][4] ), .S(\SUMB[8][4] ) );
  FA1A S2_8_5 ( .A(\ab[8][5] ), .B(\CARRYB[7][5] ), .CI(\SUMB[7][6] ), .CO(
        \CARRYB[8][5] ), .S(\SUMB[8][5] ) );
  FA1 S1_12_0 ( .A(1'b0), .B(\CARRYB[11][0] ), .CI(\SUMB[11][1] ), .CO(
        \CARRYB[12][0] ), .S(PRODUCT[12]) );
  FA1 S1_8_0 ( .A(\CARRYB[7][0] ), .B(\ab[8][0] ), .CI(\SUMB[7][1] ), .CO(
        \CARRYB[8][0] ), .S(PRODUCT[8]) );
  FA1AP S2_4_4 ( .A(\CARRYB[3][4] ), .B(\ab[4][4] ), .CI(\SUMB[3][5] ), .CO(
        \CARRYB[4][4] ), .S(\SUMB[4][4] ) );
  FA1A S2_7_2 ( .A(\CARRYB[6][2] ), .B(\ab[7][2] ), .CI(\SUMB[6][3] ), .CO(
        \CARRYB[7][2] ), .S(\SUMB[7][2] ) );
  FA1 S1_7_0 ( .A(\ab[7][0] ), .B(\CARRYB[6][0] ), .CI(\SUMB[6][1] ), .CO(
        \CARRYB[7][0] ), .S(PRODUCT[7]) );
  FA1P S2_2_3 ( .A(\ab[2][3] ), .B(\CARRYB[1][3] ), .CI(\SUMB[1][4] ), .CO(
        \CARRYB[2][3] ), .S(\SUMB[2][3] ) );
  FA1 S2_3_3 ( .A(\CARRYB[2][3] ), .B(\ab[3][3] ), .CI(\SUMB[2][4] ), .CO(
        \CARRYB[3][3] ), .S(\SUMB[3][3] ) );
  FA1A S2_2_2 ( .A(\ab[2][2] ), .B(\CARRYB[1][2] ), .CI(\SUMB[1][3] ), .CO(
        \CARRYB[2][2] ), .S(\SUMB[2][2] ) );
  IVAP U2 ( .A(n58), .Z(\CARRYB[13][0] ) );
  IVAP U3 ( .A(n21), .Z(\CARRYB[11][0] ) );
  IVAP U4 ( .A(n41), .Z(\CARRYB[10][1] ) );
  IVAP U5 ( .A(n12), .Z(\CARRYB[9][1] ) );
  IVAP U6 ( .A(n56), .Z(\CARRYB[1][5] ) );
  ND2 U7 ( .A(n39), .B(n40), .Z(\SUMB[3][4] ) );
  IVDAP U8 ( .A(B[0]), .Z(n59) );
  B2I U9 ( .A(B[3]), .Z2(n60) );
  EO3 U10 ( .A(\CARRYB[2][3] ), .B(\ab[3][3] ), .C(\SUMB[2][4] ), .Z(n7) );
  IVDA U11 ( .A(B[4]), .Z(n8) );
  AN2P U12 ( .A(A[4]), .B(B[4]), .Z(\ab[4][4] ) );
  AN2P U13 ( .A(B[4]), .B(A[0]), .Z(\ab[0][4] ) );
  AN2P U14 ( .A(B[5]), .B(A[0]), .Z(\ab[0][5] ) );
  EO U15 ( .A(\SUMB[4][3] ), .B(n13), .Z(\SUMB[5][2] ) );
  B3I U16 ( .A(n17), .Z1(\SUMB[7][1] ) );
  ND2 U17 ( .A(\CARRYB[5][2] ), .B(\ab[6][2] ), .Z(n31) );
  EOP U18 ( .A(\SUMB[12][1] ), .B(n57), .Z(PRODUCT[13]) );
  IVP U19 ( .A(n29), .Z(n20) );
  AN2P U20 ( .A(n60), .B(A[0]), .Z(\ab[0][3] ) );
  AN2P U21 ( .A(B[5]), .B(A[1]), .Z(\ab[1][5] ) );
  EOP U22 ( .A(\CARRYB[12][0] ), .B(1'b0), .Z(n57) );
  EOP U23 ( .A(\CARRYB[8][1] ), .B(1'b0), .Z(n11) );
  EOP U24 ( .A(\SUMB[8][2] ), .B(n11), .Z(\SUMB[9][1] ) );
  ND2P U25 ( .A(\SUMB[8][2] ), .B(\CARRYB[8][1] ), .Z(n12) );
  EOP U26 ( .A(\CARRYB[4][2] ), .B(\ab[5][2] ), .Z(n13) );
  ND2P U27 ( .A(\SUMB[4][3] ), .B(\CARRYB[4][2] ), .Z(n14) );
  ND2P U28 ( .A(\SUMB[4][3] ), .B(\ab[5][2] ), .Z(n15) );
  ND2 U29 ( .A(\CARRYB[4][2] ), .B(\ab[5][2] ), .Z(n16) );
  ND3P U30 ( .A(n14), .B(n15), .C(n16), .Z(\CARRYB[5][2] ) );
  EN3P U31 ( .A(\CARRYB[6][1] ), .B(\SUMB[6][2] ), .C(\ab[7][1] ), .Z(n17) );
  EO U32 ( .A(\ab[1][5] ), .B(n55), .Z(\SUMB[1][5] ) );
  ND2 U33 ( .A(\SUMB[2][3] ), .B(\CARRYB[2][2] ), .Z(n47) );
  FA1AP U34 ( .A(\CARRYB[1][5] ), .B(\ab[2][5] ), .CI(\SUMB[1][6] ), .S(n18)
         );
  EO U35 ( .A(\ab[3][5] ), .B(\SUMB[2][6] ), .Z(n19) );
  EO U36 ( .A(\CARRYB[2][5] ), .B(n19), .Z(\SUMB[3][5] ) );
  IV U37 ( .A(B[6]), .Z(n62) );
  ND2P U38 ( .A(\SUMB[2][5] ), .B(\ab[3][4] ), .Z(n45) );
  EO3 U39 ( .A(\CARRYB[10][0] ), .B(1'b0), .C(\SUMB[10][1] ), .Z(PRODUCT[11])
         );
  ND2P U40 ( .A(\CARRYB[10][0] ), .B(\SUMB[10][1] ), .Z(n21) );
  ND2P U41 ( .A(\CARRYB[2][4] ), .B(\ab[3][4] ), .Z(n44) );
  ND2P U42 ( .A(\CARRYB[2][4] ), .B(n18), .Z(n43) );
  ND2 U43 ( .A(\ab[7][1] ), .B(\CARRYB[6][1] ), .Z(n22) );
  ND2 U44 ( .A(\ab[7][1] ), .B(\SUMB[6][2] ), .Z(n23) );
  ND2 U45 ( .A(\CARRYB[6][1] ), .B(\SUMB[6][2] ), .Z(n24) );
  ND3P U46 ( .A(n22), .B(n23), .C(n24), .Z(\CARRYB[7][1] ) );
  EOP U47 ( .A(\ab[8][1] ), .B(\SUMB[7][2] ), .Z(n25) );
  EOP U48 ( .A(n25), .B(\CARRYB[7][1] ), .Z(\SUMB[8][1] ) );
  ND2 U49 ( .A(\ab[8][1] ), .B(\SUMB[7][2] ), .Z(n26) );
  ND2P U50 ( .A(\ab[8][1] ), .B(\CARRYB[7][1] ), .Z(n27) );
  ND2P U51 ( .A(\SUMB[7][2] ), .B(\CARRYB[7][1] ), .Z(n28) );
  ND3P U52 ( .A(n26), .B(n27), .C(n28), .Z(\CARRYB[8][1] ) );
  EO3P U53 ( .A(\CARRYB[5][2] ), .B(\ab[6][2] ), .C(\SUMB[5][3] ), .Z(
        \SUMB[6][2] ) );
  ND2 U54 ( .A(\CARRYB[5][2] ), .B(\SUMB[5][3] ), .Z(n30) );
  ND2 U55 ( .A(\SUMB[5][3] ), .B(\ab[6][2] ), .Z(n32) );
  ND3 U56 ( .A(n30), .B(n31), .C(n32), .Z(\CARRYB[6][2] ) );
  ND2 U57 ( .A(\CARRYB[2][4] ), .B(n38), .Z(n39) );
  IV U58 ( .A(\CARRYB[2][4] ), .Z(n37) );
  AN2P U59 ( .A(A[2]), .B(B[4]), .Z(\ab[2][4] ) );
  IVDA U60 ( .A(B[5]), .Y(n29), .Z(n33) );
  AN2P U61 ( .A(A[2]), .B(n20), .Z(\ab[2][5] ) );
  AN2P U62 ( .A(A[4]), .B(n33), .Z(\ab[4][5] ) );
  ND2P U63 ( .A(\CARRYB[2][5] ), .B(\ab[3][5] ), .Z(n34) );
  ND2P U64 ( .A(\CARRYB[2][5] ), .B(\SUMB[2][6] ), .Z(n35) );
  ND2 U65 ( .A(\ab[3][5] ), .B(\SUMB[2][6] ), .Z(n36) );
  ND3P U66 ( .A(n34), .B(n35), .C(n36), .Z(\CARRYB[3][5] ) );
  ND3P U67 ( .A(n51), .B(n52), .C(n53), .Z(\CARRYB[4][2] ) );
  AN2P U68 ( .A(B[4]), .B(A[1]), .Z(\ab[1][4] ) );
  ND2 U69 ( .A(n37), .B(n42), .Z(n40) );
  IVP U70 ( .A(n42), .Z(n38) );
  EO3P U71 ( .A(\SUMB[9][2] ), .B(1'b0), .C(\CARRYB[9][1] ), .Z(\SUMB[10][1] )
         );
  ND2 U72 ( .A(\SUMB[9][2] ), .B(\CARRYB[9][1] ), .Z(n41) );
  EOP U73 ( .A(n18), .B(\ab[3][4] ), .Z(n42) );
  ND3P U74 ( .A(n43), .B(n44), .C(n45), .Z(\CARRYB[3][4] ) );
  EO U75 ( .A(\CARRYB[2][2] ), .B(\ab[3][2] ), .Z(n46) );
  EOP U76 ( .A(\SUMB[2][3] ), .B(n46), .Z(\SUMB[3][2] ) );
  ND2P U77 ( .A(\SUMB[2][3] ), .B(\ab[3][2] ), .Z(n48) );
  ND2 U78 ( .A(\CARRYB[2][2] ), .B(\ab[3][2] ), .Z(n49) );
  ND3P U79 ( .A(n47), .B(n48), .C(n49), .Z(\CARRYB[3][2] ) );
  EOP U80 ( .A(\CARRYB[8][5] ), .B(n54), .Z(\SUMB[9][5] ) );
  EOP U81 ( .A(n7), .B(\ab[4][2] ), .Z(n50) );
  EOP U82 ( .A(\CARRYB[3][2] ), .B(n50), .Z(\SUMB[4][2] ) );
  ND2 U83 ( .A(\CARRYB[3][2] ), .B(\SUMB[3][3] ), .Z(n51) );
  ND2 U84 ( .A(\CARRYB[3][2] ), .B(\ab[4][2] ), .Z(n52) );
  ND2 U85 ( .A(\SUMB[3][3] ), .B(\ab[4][2] ), .Z(n53) );
  EO U86 ( .A(\SUMB[8][6] ), .B(1'b0), .Z(n54) );
  EOP U87 ( .A(\ab[0][6] ), .B(1'b0), .Z(n55) );
  ND2 U88 ( .A(\ab[1][5] ), .B(\ab[0][6] ), .Z(n56) );
  AN2 U89 ( .A(n59), .B(A[1]), .Z(\ab[1][0] ) );
  ND2 U90 ( .A(\SUMB[12][1] ), .B(\CARRYB[12][0] ), .Z(n58) );
  NR2 U91 ( .A(n62), .B(n66), .Z(\SUMB[7][6] ) );
  IVP U92 ( .A(n66), .Z(n61) );
  IVP U93 ( .A(n65), .Z(n63) );
  NR2 U94 ( .A(n62), .B(n65), .Z(\SUMB[8][6] ) );
  IVP U95 ( .A(A[7]), .Z(n66) );
  IVP U96 ( .A(A[8]), .Z(n65) );
  AN2 U97 ( .A(A[5]), .B(B[2]), .Z(\ab[5][2] ) );
  AN2 U98 ( .A(B[2]), .B(A[1]), .Z(\ab[1][2] ) );
  AN2 U99 ( .A(B[2]), .B(A[0]), .Z(\ab[0][2] ) );
  AN2 U100 ( .A(A[5]), .B(B[1]), .Z(\ab[5][1] ) );
  AN2 U101 ( .A(A[4]), .B(B[1]), .Z(\ab[4][1] ) );
  AN2 U102 ( .A(A[3]), .B(B[1]), .Z(\ab[3][1] ) );
  AN2 U103 ( .A(A[2]), .B(B[1]), .Z(\ab[2][1] ) );
  AN2 U104 ( .A(B[1]), .B(A[0]), .Z(\ab[0][1] ) );
  AN2 U105 ( .A(B[1]), .B(A[1]), .Z(\ab[1][1] ) );
  AN2P U106 ( .A(A[1]), .B(B[6]), .Z(\ab[1][6] ) );
  AN2P U107 ( .A(A[0]), .B(B[6]), .Z(\ab[0][6] ) );
  AN2P U108 ( .A(n60), .B(A[1]), .Z(\ab[1][3] ) );
  AN2P U109 ( .A(A[2]), .B(B[6]), .Z(\SUMB[2][6] ) );
  AN2P U110 ( .A(A[2]), .B(n60), .Z(\ab[2][3] ) );
  AN2P U111 ( .A(A[2]), .B(B[2]), .Z(\ab[2][2] ) );
  AN2P U112 ( .A(A[2]), .B(n59), .Z(\ab[2][0] ) );
  AN2P U113 ( .A(A[3]), .B(B[6]), .Z(\SUMB[3][6] ) );
  AN2P U114 ( .A(A[3]), .B(n20), .Z(\ab[3][5] ) );
  AN2P U115 ( .A(A[3]), .B(B[4]), .Z(\ab[3][4] ) );
  AN2P U116 ( .A(A[3]), .B(n60), .Z(\ab[3][3] ) );
  AN2P U117 ( .A(A[3]), .B(B[2]), .Z(\ab[3][2] ) );
  AN2P U118 ( .A(A[3]), .B(n59), .Z(\ab[3][0] ) );
  AN2P U119 ( .A(A[4]), .B(B[6]), .Z(\SUMB[4][6] ) );
  AN2P U120 ( .A(A[4]), .B(n60), .Z(\ab[4][3] ) );
  AN2P U121 ( .A(A[4]), .B(B[2]), .Z(\ab[4][2] ) );
  AN2P U122 ( .A(A[4]), .B(n59), .Z(\ab[4][0] ) );
  AN2P U123 ( .A(A[5]), .B(B[6]), .Z(\SUMB[5][6] ) );
  AN2P U124 ( .A(A[5]), .B(n33), .Z(\ab[5][5] ) );
  AN2P U125 ( .A(A[5]), .B(n8), .Z(\ab[5][4] ) );
  AN2P U126 ( .A(A[5]), .B(n60), .Z(\ab[5][3] ) );
  AN2P U127 ( .A(A[5]), .B(n59), .Z(\ab[5][0] ) );
  AN2P U128 ( .A(A[6]), .B(B[6]), .Z(\SUMB[6][6] ) );
  AN2P U129 ( .A(A[6]), .B(n33), .Z(\ab[6][5] ) );
  AN2P U130 ( .A(A[6]), .B(n8), .Z(\ab[6][4] ) );
  AN2P U131 ( .A(A[6]), .B(n60), .Z(\ab[6][3] ) );
  AN2P U132 ( .A(A[6]), .B(B[2]), .Z(\ab[6][2] ) );
  AN2P U133 ( .A(A[6]), .B(B[1]), .Z(\ab[6][1] ) );
  AN2P U134 ( .A(A[6]), .B(n59), .Z(\ab[6][0] ) );
  AN2P U135 ( .A(n61), .B(n33), .Z(\ab[7][5] ) );
  AN2P U136 ( .A(n61), .B(n8), .Z(\ab[7][4] ) );
  AN2P U137 ( .A(n61), .B(n60), .Z(\ab[7][3] ) );
  AN2P U138 ( .A(n61), .B(B[2]), .Z(\ab[7][2] ) );
  AN2P U139 ( .A(n61), .B(B[1]), .Z(\ab[7][1] ) );
  AN2P U140 ( .A(n61), .B(n59), .Z(\ab[7][0] ) );
  AN2P U141 ( .A(n63), .B(n33), .Z(\ab[8][5] ) );
  AN2P U142 ( .A(n63), .B(n8), .Z(\ab[8][4] ) );
  AN2P U143 ( .A(n63), .B(n60), .Z(\ab[8][3] ) );
  AN2P U144 ( .A(n63), .B(B[2]), .Z(\ab[8][2] ) );
  AN2P U145 ( .A(n63), .B(B[1]), .Z(\ab[8][1] ) );
  AN2P U146 ( .A(n63), .B(n59), .Z(\ab[8][0] ) );
  AN2P U147 ( .A(n59), .B(A[0]), .Z(PRODUCT[0]) );
endmodule


module compute_xy_center_DW02_mult_2 ( A, B, TC, PRODUCT );
  input [14:0] A;
  input [14:0] B;
  output [29:0] PRODUCT;
  input TC;
  wire   \ab[6][8] , \ab[6][7] , \ab[6][6] , \ab[6][5] , \ab[6][4] ,
         \ab[6][3] , \ab[6][2] , \ab[6][1] , \ab[6][0] , \ab[5][8] ,
         \ab[5][7] , \ab[5][6] , \ab[5][5] , \ab[5][4] , \ab[5][3] ,
         \ab[5][2] , \ab[5][1] , \ab[5][0] , \ab[4][8] , \ab[4][7] ,
         \ab[4][6] , \ab[4][5] , \ab[4][4] , \ab[4][3] , \ab[4][2] ,
         \ab[4][1] , \ab[4][0] , \ab[3][8] , \ab[3][7] , \ab[3][6] ,
         \ab[3][5] , \ab[3][4] , \ab[3][3] , \ab[3][2] , \ab[3][1] ,
         \ab[3][0] , \ab[2][8] , \ab[2][7] , \ab[2][6] , \ab[2][5] ,
         \ab[2][4] , \ab[2][3] , \ab[2][2] , \ab[2][1] , \ab[2][0] ,
         \ab[1][8] , \ab[1][7] , \ab[1][6] , \ab[1][5] , \ab[1][4] ,
         \ab[1][3] , \ab[1][2] , \ab[1][1] , \ab[1][0] , \ab[0][8] ,
         \ab[0][7] , \ab[0][6] , \ab[0][5] , \ab[0][4] , \ab[0][3] ,
         \ab[0][2] , \ab[0][1] , \CARRYB[13][0] , \CARRYB[12][1] ,
         \CARRYB[12][0] , \CARRYB[11][2] , \CARRYB[11][1] , \CARRYB[10][3] ,
         \CARRYB[10][2] , \CARRYB[10][1] , \CARRYB[9][4] , \CARRYB[9][3] ,
         \CARRYB[9][2] , \CARRYB[9][1] , \CARRYB[9][0] , \CARRYB[8][5] ,
         \CARRYB[8][4] , \CARRYB[8][3] , \CARRYB[8][2] , \CARRYB[8][1] ,
         \CARRYB[8][0] , \CARRYB[7][6] , \CARRYB[7][5] , \CARRYB[7][4] ,
         \CARRYB[7][3] , \CARRYB[7][2] , \CARRYB[7][1] , \CARRYB[7][0] ,
         \CARRYB[6][7] , \CARRYB[6][6] , \CARRYB[6][5] , \CARRYB[6][4] ,
         \CARRYB[6][3] , \CARRYB[6][2] , \CARRYB[6][1] , \CARRYB[6][0] ,
         \CARRYB[5][7] , \CARRYB[5][6] , \CARRYB[5][5] , \CARRYB[5][4] ,
         \CARRYB[5][3] , \CARRYB[5][2] , \CARRYB[5][1] , \CARRYB[5][0] ,
         \CARRYB[4][7] , \CARRYB[4][6] , \CARRYB[4][5] , \CARRYB[4][4] ,
         \CARRYB[4][3] , \CARRYB[4][2] , \CARRYB[4][1] , \CARRYB[4][0] ,
         \CARRYB[3][7] , \CARRYB[3][6] , \CARRYB[3][5] , \CARRYB[3][4] ,
         \CARRYB[3][3] , \CARRYB[3][2] , \CARRYB[3][1] , \CARRYB[3][0] ,
         \CARRYB[2][7] , \CARRYB[2][6] , \CARRYB[2][5] , \CARRYB[2][4] ,
         \CARRYB[2][3] , \CARRYB[2][2] , \CARRYB[2][1] , \CARRYB[2][0] ,
         \CARRYB[1][7] , \CARRYB[1][6] , \CARRYB[1][5] , \CARRYB[1][4] ,
         \CARRYB[1][3] , \CARRYB[1][2] , \CARRYB[1][1] , \CARRYB[1][0] ,
         \SUMB[13][1] , \SUMB[12][2] , \SUMB[12][1] , \SUMB[11][3] ,
         \SUMB[11][2] , \SUMB[11][1] , \SUMB[10][4] , \SUMB[10][3] ,
         \SUMB[10][2] , \SUMB[10][1] , \SUMB[9][5] , \SUMB[9][4] ,
         \SUMB[9][3] , \SUMB[9][2] , \SUMB[9][1] , \SUMB[8][6] , \SUMB[8][5] ,
         \SUMB[8][4] , \SUMB[8][3] , \SUMB[8][2] , \SUMB[8][1] , \SUMB[7][7] ,
         \SUMB[7][6] , \SUMB[7][5] , \SUMB[7][4] , \SUMB[7][3] , \SUMB[7][2] ,
         \SUMB[7][1] , \SUMB[6][7] , \SUMB[6][6] , \SUMB[6][5] , \SUMB[6][4] ,
         \SUMB[6][3] , \SUMB[6][2] , \SUMB[6][1] , \SUMB[5][7] , \SUMB[5][6] ,
         \SUMB[5][5] , \SUMB[5][4] , \SUMB[5][3] , \SUMB[5][2] , \SUMB[5][1] ,
         \SUMB[4][7] , \SUMB[4][6] , \SUMB[4][5] , \SUMB[4][4] , \SUMB[4][3] ,
         \SUMB[4][2] , \SUMB[4][1] , \SUMB[3][7] , \SUMB[3][6] , \SUMB[3][5] ,
         \SUMB[3][4] , \SUMB[3][3] , \SUMB[3][2] , \SUMB[3][1] , \SUMB[2][7] ,
         \SUMB[2][6] , \SUMB[2][5] , \SUMB[2][4] , \SUMB[2][3] , \SUMB[2][2] ,
         \SUMB[2][1] , \SUMB[1][7] , \SUMB[1][6] , \SUMB[1][5] , \SUMB[1][4] ,
         \SUMB[1][3] , \SUMB[1][2] , \SUMB[1][1] , n6, n8, n9, n10, n12, n13,
         n14, n15, n16, n17, n18, n21, n22, n23, n24, n26, n27, n28, n29, n30,
         n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44,
         n45, n46, n47, n48, n49, n50, n51, n52, n54, n55, n56, n57;

  FA1AP S2_12_2 ( .A(1'b0), .B(\CARRYB[11][2] ), .CI(\SUMB[11][3] ), .S(
        \SUMB[12][2] ) );
  FA1AP S2_6_5 ( .A(\ab[6][5] ), .B(\CARRYB[5][5] ), .CI(\SUMB[5][6] ), .CO(
        \CARRYB[6][5] ), .S(\SUMB[6][5] ) );
  FA1AP S2_5_5 ( .A(\ab[5][5] ), .B(\CARRYB[4][5] ), .CI(\SUMB[4][6] ), .CO(
        \CARRYB[5][5] ), .S(\SUMB[5][5] ) );
  FA1AP S2_5_7 ( .A(\ab[5][7] ), .B(\CARRYB[4][7] ), .CI(\ab[4][8] ), .CO(
        \CARRYB[5][7] ), .S(\SUMB[5][7] ) );
  FA1AP S2_4_5 ( .A(\ab[4][5] ), .B(\CARRYB[3][5] ), .CI(\SUMB[3][6] ), .CO(
        \CARRYB[4][5] ), .S(\SUMB[4][5] ) );
  FA1AP S2_4_6 ( .A(\ab[4][6] ), .B(\CARRYB[3][6] ), .CI(\SUMB[3][7] ), .CO(
        \CARRYB[4][6] ), .S(\SUMB[4][6] ) );
  FA1AP S2_3_6 ( .A(\ab[3][6] ), .B(\CARRYB[2][6] ), .CI(\SUMB[2][7] ), .CO(
        \CARRYB[3][6] ), .S(\SUMB[3][6] ) );
  FA1AP S2_2_6 ( .A(\ab[2][6] ), .B(\CARRYB[1][6] ), .CI(\SUMB[1][7] ), .CO(
        \CARRYB[2][6] ), .S(\SUMB[2][6] ) );
  FA1A S2_3_1 ( .A(\ab[3][1] ), .B(\CARRYB[2][1] ), .CI(\SUMB[2][2] ), .CO(
        \CARRYB[3][1] ), .S(\SUMB[3][1] ) );
  FA1 S0_1 ( .A(\ab[0][2] ), .B(\ab[1][1] ), .CI(1'b0), .CO(\CARRYB[1][1] ), 
        .S(\SUMB[1][1] ) );
  FA1A S2_2_1 ( .A(\ab[2][1] ), .B(\CARRYB[1][1] ), .CI(\SUMB[1][2] ), .CO(
        \CARRYB[2][1] ), .S(\SUMB[2][1] ) );
  FA1P S2_4_1 ( .A(\CARRYB[3][1] ), .B(\ab[4][1] ), .CI(\SUMB[3][2] ), .CO(
        \CARRYB[4][1] ), .S(\SUMB[4][1] ) );
  FA1AP S4_0 ( .A(\CARRYB[13][0] ), .B(1'b0), .CI(\SUMB[13][1] ), .S(
        PRODUCT[14]) );
  FA1P S1_7_0 ( .A(1'b0), .B(\CARRYB[6][0] ), .CI(\SUMB[6][1] ), .CO(
        \CARRYB[7][0] ), .S(PRODUCT[7]) );
  FA1P S1_8_0 ( .A(1'b0), .B(\CARRYB[7][0] ), .CI(\SUMB[7][1] ), .CO(
        \CARRYB[8][0] ), .S(PRODUCT[8]) );
  FA1P S2_6_3 ( .A(\ab[6][3] ), .B(\CARRYB[5][3] ), .CI(\SUMB[5][4] ), .CO(
        \CARRYB[6][3] ), .S(\SUMB[6][3] ) );
  FA1A S1_2_0 ( .A(\ab[2][0] ), .B(\CARRYB[1][0] ), .CI(\SUMB[1][1] ), .CO(
        \CARRYB[2][0] ), .S(PRODUCT[2]) );
  FA1A S0_0 ( .A(\ab[0][1] ), .B(\ab[1][0] ), .CI(1'b0), .CO(\CARRYB[1][0] ), 
        .S(PRODUCT[1]) );
  EO3 S2_7_7 ( .A(1'b0), .B(\CARRYB[6][7] ), .C(\ab[6][8] ), .Z(\SUMB[7][7] )
         );
  EO3 S2_8_6 ( .A(1'b0), .B(\CARRYB[7][6] ), .C(\SUMB[7][7] ), .Z(\SUMB[8][6] ) );
  EO3 S2_9_5 ( .A(1'b0), .B(\CARRYB[8][5] ), .C(\SUMB[8][6] ), .Z(\SUMB[9][5] ) );
  EO3 S2_10_4 ( .A(\SUMB[9][5] ), .B(\CARRYB[9][4] ), .C(1'b0), .Z(
        \SUMB[10][4] ) );
  FA1A S2_9_4 ( .A(1'b0), .B(\CARRYB[8][4] ), .CI(\SUMB[8][5] ), .CO(
        \CARRYB[9][4] ), .S(\SUMB[9][4] ) );
  FA1A S2_6_1 ( .A(\CARRYB[5][1] ), .B(\ab[6][1] ), .CI(\SUMB[5][2] ), .CO(
        \CARRYB[6][1] ), .S(\SUMB[6][1] ) );
  FA1 S2_8_5 ( .A(1'b0), .B(\CARRYB[7][5] ), .CI(\SUMB[7][6] ), .CO(
        \CARRYB[8][5] ), .S(\SUMB[8][5] ) );
  FA1 S1_4_0 ( .A(\ab[4][0] ), .B(\CARRYB[3][0] ), .CI(\SUMB[3][1] ), .CO(
        \CARRYB[4][0] ), .S(PRODUCT[4]) );
  FA1 S2_5_2 ( .A(\SUMB[4][3] ), .B(\CARRYB[4][2] ), .CI(\ab[5][2] ), .CO(
        \CARRYB[5][2] ), .S(\SUMB[5][2] ) );
  FA1 S2_5_1 ( .A(\ab[5][1] ), .B(\CARRYB[4][1] ), .CI(\SUMB[4][2] ), .CO(
        \CARRYB[5][1] ), .S(\SUMB[5][1] ) );
  FA1 S1_6_0 ( .A(\ab[6][0] ), .B(\CARRYB[5][0] ), .CI(\SUMB[5][1] ), .CO(
        \CARRYB[6][0] ), .S(PRODUCT[6]) );
  FA1P S1_3_0 ( .A(\ab[3][0] ), .B(\CARRYB[2][0] ), .CI(\SUMB[2][1] ), .CO(
        \CARRYB[3][0] ), .S(PRODUCT[3]) );
  FA1AP S0_5 ( .A(\ab[0][6] ), .B(\ab[1][5] ), .CI(1'b0), .CO(\CARRYB[1][5] ), 
        .S(\SUMB[1][5] ) );
  FA1P S2_7_1 ( .A(\CARRYB[6][1] ), .B(1'b0), .CI(\SUMB[6][2] ), .CO(
        \CARRYB[7][1] ), .S(\SUMB[7][1] ) );
  FA1P S2_3_2 ( .A(\ab[3][2] ), .B(\CARRYB[2][2] ), .CI(\SUMB[2][3] ), .CO(
        \CARRYB[3][2] ), .S(\SUMB[3][2] ) );
  FA1A S0_2 ( .A(\ab[0][3] ), .B(\ab[1][2] ), .CI(1'b0), .CO(\CARRYB[1][2] ), 
        .S(\SUMB[1][2] ) );
  FA1A S2_2_3 ( .A(\ab[2][3] ), .B(\CARRYB[1][3] ), .CI(\SUMB[1][4] ), .CO(
        \CARRYB[2][3] ), .S(\SUMB[2][3] ) );
  FA1P S2_3_5 ( .A(\CARRYB[2][5] ), .B(\ab[3][5] ), .CI(\SUMB[2][6] ), .CO(
        \CARRYB[3][5] ), .S(\SUMB[3][5] ) );
  FA1AP S2_12_1 ( .A(\CARRYB[11][1] ), .B(1'b0), .CI(\SUMB[11][2] ), .CO(
        \CARRYB[12][1] ), .S(\SUMB[12][1] ) );
  FA1AP S2_13_1 ( .A(\CARRYB[12][1] ), .B(1'b0), .CI(\SUMB[12][2] ), .S(
        \SUMB[13][1] ) );
  FA1A S2_3_3 ( .A(\ab[3][3] ), .B(\CARRYB[2][3] ), .CI(\SUMB[2][4] ), .CO(
        \CARRYB[3][3] ), .S(\SUMB[3][3] ) );
  FA1AP S2_3_4 ( .A(\CARRYB[2][4] ), .B(\ab[3][4] ), .CI(\SUMB[2][5] ), .CO(
        \CARRYB[3][4] ), .S(\SUMB[3][4] ) );
  FA1 S2_5_6 ( .A(\ab[5][6] ), .B(\CARRYB[4][6] ), .CI(\SUMB[4][7] ), .CO(
        \CARRYB[5][6] ), .S(\SUMB[5][6] ) );
  FA1AP S2_6_4 ( .A(\CARRYB[5][4] ), .B(\ab[6][4] ), .CI(\SUMB[5][5] ), .CO(
        \CARRYB[6][4] ), .S(\SUMB[6][4] ) );
  FA1P S2_6_7 ( .A(\ab[6][7] ), .B(\CARRYB[5][7] ), .CI(\ab[5][8] ), .CO(
        \CARRYB[6][7] ), .S(\SUMB[6][7] ) );
  FA1 S2_7_6 ( .A(1'b0), .B(\CARRYB[6][6] ), .CI(\SUMB[6][7] ), .CO(
        \CARRYB[7][6] ), .S(\SUMB[7][6] ) );
  FA1P S0_7 ( .A(\ab[0][8] ), .B(\ab[1][7] ), .CI(1'b0), .CO(\CARRYB[1][7] ), 
        .S(\SUMB[1][7] ) );
  FA1 S2_2_7 ( .A(\ab[2][7] ), .B(\CARRYB[1][7] ), .CI(\ab[1][8] ), .CO(
        \CARRYB[2][7] ), .S(\SUMB[2][7] ) );
  FA1P S2_4_4 ( .A(\CARRYB[3][4] ), .B(\ab[4][4] ), .CI(\SUMB[3][5] ), .CO(
        \CARRYB[4][4] ), .S(\SUMB[4][4] ) );
  FA1AP S2_9_3 ( .A(\CARRYB[8][3] ), .B(1'b0), .CI(\SUMB[8][4] ), .CO(
        \CARRYB[9][3] ), .S(\SUMB[9][3] ) );
  FA1A S2_3_7 ( .A(\ab[3][7] ), .B(\CARRYB[2][7] ), .CI(\ab[2][8] ), .CO(
        \CARRYB[3][7] ), .S(\SUMB[3][7] ) );
  FA1A S2_4_7 ( .A(\ab[4][7] ), .B(\CARRYB[3][7] ), .CI(\ab[3][8] ), .CO(
        \CARRYB[4][7] ), .S(\SUMB[4][7] ) );
  FA1 S0_3 ( .A(\ab[0][4] ), .B(\ab[1][3] ), .CI(1'b0), .CO(\CARRYB[1][3] ), 
        .S(\SUMB[1][3] ) );
  FA1P S2_5_3 ( .A(\ab[5][3] ), .B(\CARRYB[4][3] ), .CI(\SUMB[4][4] ), .CO(
        \CARRYB[5][3] ), .S(\SUMB[5][3] ) );
  FA1AP S2_8_3 ( .A(\CARRYB[7][3] ), .B(1'b0), .CI(\SUMB[7][4] ), .CO(
        \CARRYB[8][3] ), .S(\SUMB[8][3] ) );
  FA1AP S2_10_2 ( .A(\CARRYB[9][2] ), .B(1'b0), .CI(\SUMB[9][3] ), .CO(
        \CARRYB[10][2] ), .S(\SUMB[10][2] ) );
  FA1 S2_7_2 ( .A(\SUMB[6][3] ), .B(1'b0), .CI(\CARRYB[6][2] ), .CO(
        \CARRYB[7][2] ), .S(\SUMB[7][2] ) );
  FA1A S1_10_0 ( .A(\CARRYB[9][0] ), .B(1'b0), .CI(\SUMB[9][1] ), .S(
        PRODUCT[10]) );
  FA1AP S2_6_2 ( .A(\CARRYB[5][2] ), .B(\ab[6][2] ), .CI(\SUMB[5][3] ), .S(
        \SUMB[6][2] ) );
  FA1A S2_8_1 ( .A(\CARRYB[7][1] ), .B(1'b0), .CI(\SUMB[7][2] ), .CO(
        \CARRYB[8][1] ), .S(\SUMB[8][1] ) );
  FA1P S0_4 ( .A(\ab[0][5] ), .B(\ab[1][4] ), .CI(1'b0), .CO(\CARRYB[1][4] ), 
        .S(\SUMB[1][4] ) );
  FA1AP S2_7_4 ( .A(1'b0), .B(\CARRYB[6][4] ), .CI(\SUMB[6][5] ), .CO(
        \CARRYB[7][4] ), .S(\SUMB[7][4] ) );
  FA1 S1_9_0 ( .A(\CARRYB[8][0] ), .B(\SUMB[8][1] ), .CI(1'b0), .CO(
        \CARRYB[9][0] ), .S(PRODUCT[9]) );
  FA1AP S2_10_3 ( .A(1'b0), .B(\CARRYB[9][3] ), .CI(\SUMB[9][4] ), .CO(
        \CARRYB[10][3] ), .S(\SUMB[10][3] ) );
  FA1AP S2_11_2 ( .A(\CARRYB[10][2] ), .B(1'b0), .CI(\SUMB[10][3] ), .CO(
        \CARRYB[11][2] ), .S(\SUMB[11][2] ) );
  FA1 S2_5_4 ( .A(\ab[5][4] ), .B(\CARRYB[4][4] ), .CI(\SUMB[4][5] ), .CO(
        \CARRYB[5][4] ), .S(\SUMB[5][4] ) );
  FA1A S2_4_2 ( .A(\ab[4][2] ), .B(\CARRYB[3][2] ), .CI(\SUMB[3][3] ), .CO(
        \CARRYB[4][2] ), .S(\SUMB[4][2] ) );
  FA1A S2_8_4 ( .A(1'b0), .B(\CARRYB[7][4] ), .CI(\SUMB[7][5] ), .CO(
        \CARRYB[8][4] ), .S(\SUMB[8][4] ) );
  FA1A S2_7_5 ( .A(1'b0), .B(\CARRYB[6][5] ), .CI(\SUMB[6][6] ), .CO(
        \CARRYB[7][5] ), .S(\SUMB[7][5] ) );
  FA1A S2_6_6 ( .A(\ab[6][6] ), .B(\CARRYB[5][6] ), .CI(\SUMB[5][7] ), .CO(
        \CARRYB[6][6] ), .S(\SUMB[6][6] ) );
  FA1A S1_5_0 ( .A(\ab[5][0] ), .B(\CARRYB[4][0] ), .CI(\SUMB[4][1] ), .CO(
        \CARRYB[5][0] ), .S(PRODUCT[5]) );
  IVAP U2 ( .A(n48), .Z(\CARRYB[13][0] ) );
  IVAP U3 ( .A(n42), .Z(n29) );
  IVAP U4 ( .A(n42), .Z(\CARRYB[12][0] ) );
  IVAP U5 ( .A(n44), .Z(\CARRYB[11][1] ) );
  ND3 U6 ( .A(n31), .B(1'b1), .C(1'b1), .Z(n10) );
  ND3 U7 ( .A(n31), .B(1'b1), .C(1'b1), .Z(n8) );
  ND3 U8 ( .A(n35), .B(1'b1), .C(1'b1), .Z(\CARRYB[10][1] ) );
  ND3 U9 ( .A(n35), .B(1'b1), .C(1'b1), .Z(n26) );
  IVAP U10 ( .A(n18), .Z(\CARRYB[9][2] ) );
  IVAP U11 ( .A(n22), .Z(\CARRYB[7][3] ) );
  IVAP U12 ( .A(n23), .Z(\CARRYB[8][2] ) );
  IVAP U13 ( .A(n33), .Z(\CARRYB[9][1] ) );
  IVAP U14 ( .A(n28), .Z(\CARRYB[1][6] ) );
  ND3 U16 ( .A(n38), .B(n39), .C(n40), .Z(\CARRYB[2][4] ) );
  IVDA U17 ( .A(\SUMB[9][2] ), .Z(n6) );
  EO3 U18 ( .A(\CARRYB[8][2] ), .B(1'b0), .C(\SUMB[8][3] ), .Z(\SUMB[9][2] )
         );
  FA1A U19 ( .A(1'b0), .B(\CARRYB[9][0] ), .CI(\SUMB[9][1] ), .CO(n9) );
  IVDA U20 ( .A(A[5]), .Z(n57) );
  EOP U21 ( .A(\SUMB[1][6] ), .B(\ab[2][5] ), .Z(n12) );
  EOP U22 ( .A(\CARRYB[1][5] ), .B(n12), .Z(\SUMB[2][5] ) );
  ND2P U23 ( .A(\CARRYB[1][5] ), .B(\SUMB[1][6] ), .Z(n13) );
  ND2P U24 ( .A(\CARRYB[1][5] ), .B(\ab[2][5] ), .Z(n14) );
  ND2 U25 ( .A(\SUMB[1][6] ), .B(\ab[2][5] ), .Z(n15) );
  ND3P U26 ( .A(n13), .B(n14), .C(n15), .Z(\CARRYB[2][5] ) );
  IVDA U27 ( .A(\SUMB[7][3] ), .Z(n16) );
  EOP U28 ( .A(\SUMB[10][2] ), .B(n43), .Z(n17) );
  ND2 U29 ( .A(\CARRYB[8][2] ), .B(\SUMB[8][3] ), .Z(n18) );
  EO3 U30 ( .A(\CARRYB[6][3] ), .B(1'b0), .C(\SUMB[6][4] ), .Z(\SUMB[7][3] )
         );
  ND3P U31 ( .A(n45), .B(n46), .C(n47), .Z(\CARRYB[4][3] ) );
  B2I U32 ( .A(A[3]), .Z2(n55) );
  EOP U33 ( .A(\CARRYB[7][2] ), .B(1'b0), .Z(n21) );
  EOP U34 ( .A(n21), .B(n16), .Z(\SUMB[8][2] ) );
  ND2 U35 ( .A(\CARRYB[6][3] ), .B(\SUMB[6][4] ), .Z(n22) );
  ND2 U36 ( .A(\CARRYB[7][2] ), .B(\SUMB[7][3] ), .Z(n23) );
  AN2P U37 ( .A(A[1]), .B(B[7]), .Z(\ab[1][7] ) );
  AO5 U38 ( .A(\ab[6][2] ), .B(\SUMB[5][3] ), .C(\CARRYB[5][2] ), .Z(n24) );
  IV U39 ( .A(n24), .Z(\CARRYB[6][2] ) );
  IVDA U40 ( .A(A[4]), .Z(n56) );
  EO U41 ( .A(\SUMB[1][5] ), .B(n37), .Z(\SUMB[2][4] ) );
  ND2P U42 ( .A(\SUMB[11][1] ), .B(n8), .Z(n42) );
  AN2P U43 ( .A(B[2]), .B(n54), .Z(\ab[0][2] ) );
  ND2P U44 ( .A(\SUMB[10][1] ), .B(n9), .Z(n31) );
  EOP U45 ( .A(\ab[1][6] ), .B(1'b0), .Z(n27) );
  EOP U46 ( .A(\ab[0][7] ), .B(n27), .Z(\SUMB[1][6] ) );
  ND2 U47 ( .A(\ab[0][7] ), .B(\ab[1][6] ), .Z(n28) );
  AN2P U48 ( .A(B[6]), .B(A[1]), .Z(\ab[1][6] ) );
  EOP U49 ( .A(n9), .B(1'b0), .Z(n30) );
  EOP U50 ( .A(\SUMB[10][1] ), .B(n30), .Z(PRODUCT[11]) );
  EOP U51 ( .A(\CARRYB[8][1] ), .B(1'b0), .Z(n32) );
  EO U52 ( .A(\SUMB[8][2] ), .B(n32), .Z(\SUMB[9][1] ) );
  ND2 U53 ( .A(\SUMB[8][2] ), .B(\CARRYB[8][1] ), .Z(n33) );
  EOP U54 ( .A(\CARRYB[9][1] ), .B(1'b0), .Z(n34) );
  EOP U55 ( .A(n34), .B(n6), .Z(\SUMB[10][1] ) );
  ND2 U56 ( .A(\SUMB[9][2] ), .B(\CARRYB[9][1] ), .Z(n35) );
  EO U57 ( .A(\CARRYB[10][3] ), .B(1'b0), .Z(n36) );
  EOP U58 ( .A(\SUMB[10][4] ), .B(n36), .Z(\SUMB[11][3] ) );
  ND2 U59 ( .A(\SUMB[1][5] ), .B(\CARRYB[1][4] ), .Z(n38) );
  ND2 U60 ( .A(\SUMB[1][5] ), .B(\ab[2][4] ), .Z(n39) );
  EOP U61 ( .A(\CARRYB[1][4] ), .B(\ab[2][4] ), .Z(n37) );
  ND2 U62 ( .A(\CARRYB[1][4] ), .B(\ab[2][4] ), .Z(n40) );
  EOP U63 ( .A(\SUMB[3][4] ), .B(\ab[4][3] ), .Z(n41) );
  EOP U64 ( .A(\CARRYB[3][3] ), .B(n41), .Z(\SUMB[4][3] ) );
  EO3 U65 ( .A(n17), .B(1'b0), .C(n10), .Z(PRODUCT[12]) );
  EOP U66 ( .A(\CARRYB[10][1] ), .B(1'b0), .Z(n43) );
  EOP U67 ( .A(n43), .B(\SUMB[10][2] ), .Z(\SUMB[11][1] ) );
  ND2 U68 ( .A(\SUMB[10][2] ), .B(n26), .Z(n44) );
  ND2 U69 ( .A(\CARRYB[3][3] ), .B(\SUMB[3][4] ), .Z(n45) );
  ND2 U70 ( .A(\CARRYB[3][3] ), .B(\ab[4][3] ), .Z(n46) );
  ND2 U71 ( .A(\SUMB[3][4] ), .B(\ab[4][3] ), .Z(n47) );
  EO3 U72 ( .A(\SUMB[12][1] ), .B(1'b0), .C(n29), .Z(PRODUCT[13]) );
  ND2 U73 ( .A(\CARRYB[12][0] ), .B(\SUMB[12][1] ), .Z(n48) );
  EOP U74 ( .A(\SUMB[1][3] ), .B(\ab[2][2] ), .Z(n49) );
  EOP U75 ( .A(\CARRYB[1][2] ), .B(n49), .Z(\SUMB[2][2] ) );
  ND2 U76 ( .A(\CARRYB[1][2] ), .B(\SUMB[1][3] ), .Z(n50) );
  ND2 U77 ( .A(\CARRYB[1][2] ), .B(\ab[2][2] ), .Z(n51) );
  ND2 U78 ( .A(\SUMB[1][3] ), .B(\ab[2][2] ), .Z(n52) );
  ND3P U79 ( .A(n50), .B(n51), .C(n52), .Z(\CARRYB[2][2] ) );
  AN2 U80 ( .A(B[5]), .B(n54), .Z(\ab[0][5] ) );
  B2IP U81 ( .A(A[0]), .Z2(n54) );
  AN2P U82 ( .A(B[4]), .B(n54), .Z(\ab[0][4] ) );
  AN2P U83 ( .A(B[3]), .B(n54), .Z(\ab[0][3] ) );
  AN2P U84 ( .A(B[3]), .B(A[1]), .Z(\ab[1][3] ) );
  AN2 U85 ( .A(B[1]), .B(n54), .Z(\ab[0][1] ) );
  AN2 U86 ( .A(A[1]), .B(B[8]), .Z(\ab[1][8] ) );
  AN2 U87 ( .A(B[0]), .B(A[1]), .Z(\ab[1][0] ) );
  AN2 U88 ( .A(B[1]), .B(A[1]), .Z(\ab[1][1] ) );
  AN2 U89 ( .A(B[2]), .B(A[1]), .Z(\ab[1][2] ) );
  AN2 U90 ( .A(B[4]), .B(A[1]), .Z(\ab[1][4] ) );
  AN2 U91 ( .A(B[5]), .B(A[1]), .Z(\ab[1][5] ) );
  AN2P U92 ( .A(n54), .B(B[8]), .Z(\ab[0][8] ) );
  AN2P U93 ( .A(B[7]), .B(n54), .Z(\ab[0][7] ) );
  AN2P U94 ( .A(B[6]), .B(n54), .Z(\ab[0][6] ) );
  AN2P U95 ( .A(A[2]), .B(B[7]), .Z(\ab[2][7] ) );
  AN2P U96 ( .A(A[2]), .B(B[6]), .Z(\ab[2][6] ) );
  AN2P U97 ( .A(A[2]), .B(B[5]), .Z(\ab[2][5] ) );
  AN2P U98 ( .A(A[2]), .B(B[4]), .Z(\ab[2][4] ) );
  AN2P U99 ( .A(A[2]), .B(B[3]), .Z(\ab[2][3] ) );
  AN2P U100 ( .A(A[2]), .B(B[2]), .Z(\ab[2][2] ) );
  AN2P U101 ( .A(A[2]), .B(B[1]), .Z(\ab[2][1] ) );
  AN2P U102 ( .A(A[2]), .B(B[0]), .Z(\ab[2][0] ) );
  AN2P U103 ( .A(n55), .B(B[7]), .Z(\ab[3][7] ) );
  AN2P U104 ( .A(A[2]), .B(B[8]), .Z(\ab[2][8] ) );
  AN2P U105 ( .A(n55), .B(B[6]), .Z(\ab[3][6] ) );
  AN2P U106 ( .A(n55), .B(B[5]), .Z(\ab[3][5] ) );
  AN2P U107 ( .A(n55), .B(B[4]), .Z(\ab[3][4] ) );
  AN2P U108 ( .A(n55), .B(B[3]), .Z(\ab[3][3] ) );
  AN2P U109 ( .A(n55), .B(B[2]), .Z(\ab[3][2] ) );
  AN2P U110 ( .A(n55), .B(B[1]), .Z(\ab[3][1] ) );
  AN2P U111 ( .A(n55), .B(B[0]), .Z(\ab[3][0] ) );
  AN2P U112 ( .A(n56), .B(B[7]), .Z(\ab[4][7] ) );
  AN2P U113 ( .A(n55), .B(B[8]), .Z(\ab[3][8] ) );
  AN2P U114 ( .A(n56), .B(B[6]), .Z(\ab[4][6] ) );
  AN2P U115 ( .A(n56), .B(B[5]), .Z(\ab[4][5] ) );
  AN2P U116 ( .A(n56), .B(B[4]), .Z(\ab[4][4] ) );
  AN2P U117 ( .A(n56), .B(B[3]), .Z(\ab[4][3] ) );
  AN2P U118 ( .A(n56), .B(B[2]), .Z(\ab[4][2] ) );
  AN2P U119 ( .A(n56), .B(B[1]), .Z(\ab[4][1] ) );
  AN2P U120 ( .A(n56), .B(B[0]), .Z(\ab[4][0] ) );
  AN2P U121 ( .A(n57), .B(B[7]), .Z(\ab[5][7] ) );
  AN2P U122 ( .A(n56), .B(B[8]), .Z(\ab[4][8] ) );
  AN2P U123 ( .A(n57), .B(B[6]), .Z(\ab[5][6] ) );
  AN2P U124 ( .A(n57), .B(B[5]), .Z(\ab[5][5] ) );
  AN2P U125 ( .A(n57), .B(B[4]), .Z(\ab[5][4] ) );
  AN2P U126 ( .A(n57), .B(B[3]), .Z(\ab[5][3] ) );
  AN2P U127 ( .A(n57), .B(B[2]), .Z(\ab[5][2] ) );
  AN2P U128 ( .A(n57), .B(B[1]), .Z(\ab[5][1] ) );
  AN2P U129 ( .A(n57), .B(B[0]), .Z(\ab[5][0] ) );
  AN2P U130 ( .A(A[6]), .B(B[7]), .Z(\ab[6][7] ) );
  AN2P U131 ( .A(n57), .B(B[8]), .Z(\ab[5][8] ) );
  AN2P U132 ( .A(A[6]), .B(B[6]), .Z(\ab[6][6] ) );
  AN2P U133 ( .A(A[6]), .B(B[5]), .Z(\ab[6][5] ) );
  AN2P U134 ( .A(A[6]), .B(B[4]), .Z(\ab[6][4] ) );
  AN2P U135 ( .A(A[6]), .B(B[3]), .Z(\ab[6][3] ) );
  AN2P U136 ( .A(A[6]), .B(B[2]), .Z(\ab[6][2] ) );
  AN2P U137 ( .A(A[6]), .B(B[1]), .Z(\ab[6][1] ) );
  AN2P U138 ( .A(A[6]), .B(B[0]), .Z(\ab[6][0] ) );
  AN2P U139 ( .A(A[6]), .B(B[8]), .Z(\ab[6][8] ) );
  AN2P U140 ( .A(B[0]), .B(n54), .Z(PRODUCT[0]) );
endmodule


module compute_xy_center_DW02_mult_1 ( A, B, TC, PRODUCT );
  input [14:0] A;
  input [14:0] B;
  output [29:0] PRODUCT;
  input TC;
  wire   \ab[8][5] , \ab[8][4] , \ab[8][3] , \ab[8][2] , \ab[8][1] ,
         \ab[8][0] , \ab[7][5] , \ab[7][4] , \ab[7][3] , \ab[7][2] ,
         \ab[7][1] , \ab[7][0] , \ab[6][5] , \ab[6][4] , \ab[6][3] ,
         \ab[6][2] , \ab[6][1] , \ab[6][0] , \ab[5][5] , \ab[5][4] ,
         \ab[5][3] , \ab[5][2] , \ab[5][1] , \ab[5][0] , \ab[4][5] ,
         \ab[4][4] , \ab[4][3] , \ab[4][2] , \ab[4][1] , \ab[4][0] ,
         \ab[3][5] , \ab[3][4] , \ab[3][3] , \ab[3][2] , \ab[3][1] ,
         \ab[3][0] , \ab[2][5] , \ab[2][4] , \ab[2][3] , \ab[2][2] ,
         \ab[2][1] , \ab[2][0] , \ab[1][6] , \ab[1][5] , \ab[1][4] ,
         \ab[1][3] , \ab[1][2] , \ab[1][1] , \ab[1][0] , \ab[0][6] ,
         \ab[0][5] , \ab[0][4] , \ab[0][3] , \ab[0][2] , \ab[0][1] ,
         \CARRYB[13][0] , \CARRYB[12][1] , \CARRYB[12][0] , \CARRYB[11][2] ,
         \CARRYB[11][1] , \CARRYB[11][0] , \CARRYB[10][3] , \CARRYB[10][2] ,
         \CARRYB[10][1] , \CARRYB[10][0] , \CARRYB[9][4] , \CARRYB[9][3] ,
         \CARRYB[9][2] , \CARRYB[9][1] , \CARRYB[9][0] , \CARRYB[8][5] ,
         \CARRYB[8][4] , \CARRYB[8][3] , \CARRYB[8][2] , \CARRYB[8][1] ,
         \CARRYB[8][0] , \CARRYB[7][5] , \CARRYB[7][4] , \CARRYB[7][3] ,
         \CARRYB[7][2] , \CARRYB[7][1] , \CARRYB[7][0] , \CARRYB[6][5] ,
         \CARRYB[6][4] , \CARRYB[6][3] , \CARRYB[6][2] , \CARRYB[6][1] ,
         \CARRYB[6][0] , \CARRYB[5][5] , \CARRYB[5][4] , \CARRYB[5][3] ,
         \CARRYB[5][2] , \CARRYB[5][1] , \CARRYB[5][0] , \CARRYB[4][5] ,
         \CARRYB[4][4] , \CARRYB[4][3] , \CARRYB[4][2] , \CARRYB[4][1] ,
         \CARRYB[4][0] , \CARRYB[3][5] , \CARRYB[3][4] , \CARRYB[3][3] ,
         \CARRYB[3][2] , \CARRYB[3][1] , \CARRYB[3][0] , \CARRYB[2][5] ,
         \CARRYB[2][4] , \CARRYB[2][3] , \CARRYB[2][2] , \CARRYB[2][1] ,
         \CARRYB[2][0] , \CARRYB[1][5] , \CARRYB[1][4] , \CARRYB[1][3] ,
         \CARRYB[1][2] , \CARRYB[1][1] , \CARRYB[1][0] , \SUMB[13][1] ,
         \SUMB[12][2] , \SUMB[12][1] , \SUMB[11][3] , \SUMB[11][2] ,
         \SUMB[11][1] , \SUMB[10][4] , \SUMB[10][3] , \SUMB[10][2] ,
         \SUMB[10][1] , \SUMB[9][5] , \SUMB[9][4] , \SUMB[9][3] , \SUMB[9][2] ,
         \SUMB[9][1] , \SUMB[8][6] , \SUMB[8][5] , \SUMB[8][4] , \SUMB[8][3] ,
         \SUMB[8][2] , \SUMB[8][1] , \SUMB[7][6] , \SUMB[7][5] , \SUMB[7][4] ,
         \SUMB[7][3] , \SUMB[7][2] , \SUMB[7][1] , \SUMB[6][6] , \SUMB[6][5] ,
         \SUMB[6][4] , \SUMB[6][3] , \SUMB[6][2] , \SUMB[6][1] , \SUMB[5][6] ,
         \SUMB[5][5] , \SUMB[5][4] , \SUMB[5][3] , \SUMB[5][2] , \SUMB[5][1] ,
         \SUMB[4][6] , \SUMB[4][5] , \SUMB[4][4] , \SUMB[4][3] , \SUMB[4][2] ,
         \SUMB[4][1] , \SUMB[3][6] , \SUMB[3][5] , \SUMB[3][4] , \SUMB[3][3] ,
         \SUMB[3][2] , \SUMB[3][1] , \SUMB[2][6] , \SUMB[2][5] , \SUMB[2][4] ,
         \SUMB[2][3] , \SUMB[2][2] , \SUMB[2][1] , \SUMB[1][6] , \SUMB[1][5] ,
         \SUMB[1][4] , \SUMB[1][3] , \SUMB[1][2] , \SUMB[1][1] , n5, n6, n7,
         n8, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22,
         n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36,
         n37, n38, n39, n40, n41, n42, n43, n45, n46, n47, n48, n49, n50;

  FA1AP S2_12_2 ( .A(\CARRYB[11][2] ), .B(1'b0), .CI(\SUMB[11][3] ), .S(
        \SUMB[12][2] ) );
  FA1AP S2_11_2 ( .A(1'b0), .B(\CARRYB[10][2] ), .CI(\SUMB[10][3] ), .CO(
        \CARRYB[11][2] ), .S(\SUMB[11][2] ) );
  FA1AP S2_10_3 ( .A(1'b0), .B(\CARRYB[9][3] ), .CI(\SUMB[9][4] ), .CO(
        \CARRYB[10][3] ), .S(\SUMB[10][3] ) );
  FA1AP S2_9_3 ( .A(1'b0), .B(\CARRYB[8][3] ), .CI(\SUMB[8][4] ), .CO(
        \CARRYB[9][3] ), .S(\SUMB[9][3] ) );
  FA1AP S2_9_4 ( .A(1'b0), .B(\CARRYB[8][4] ), .CI(\SUMB[8][5] ), .CO(
        \CARRYB[9][4] ), .S(\SUMB[9][4] ) );
  FA1AP S2_8_5 ( .A(\ab[8][5] ), .B(\CARRYB[7][5] ), .CI(\SUMB[7][6] ), .CO(
        \CARRYB[8][5] ), .S(\SUMB[8][5] ) );
  FA1AP S2_7_4 ( .A(\ab[7][4] ), .B(\CARRYB[6][4] ), .CI(\SUMB[6][5] ), .CO(
        \CARRYB[7][4] ), .S(\SUMB[7][4] ) );
  FA1AP S2_6_5 ( .A(\ab[6][5] ), .B(\CARRYB[5][5] ), .CI(\SUMB[5][6] ), .CO(
        \CARRYB[6][5] ), .S(\SUMB[6][5] ) );
  FA1AP S2_5_4 ( .A(\ab[5][4] ), .B(\CARRYB[4][4] ), .CI(\SUMB[4][5] ), .CO(
        \CARRYB[5][4] ), .S(\SUMB[5][4] ) );
  FA1P S1_3_0 ( .A(\ab[3][0] ), .B(\CARRYB[2][0] ), .CI(\SUMB[2][1] ), .CO(
        \CARRYB[3][0] ), .S(PRODUCT[3]) );
  FA1P S1_10_0 ( .A(1'b0), .B(\CARRYB[9][0] ), .CI(\SUMB[9][1] ), .CO(
        \CARRYB[10][0] ), .S(PRODUCT[10]) );
  FA1P S1_6_0 ( .A(\ab[6][0] ), .B(\CARRYB[5][0] ), .CI(\SUMB[5][1] ), .CO(
        \CARRYB[6][0] ), .S(PRODUCT[6]) );
  FA1P S2_4_2 ( .A(\ab[4][2] ), .B(\CARRYB[3][2] ), .CI(\SUMB[3][3] ), .CO(
        \CARRYB[4][2] ), .S(\SUMB[4][2] ) );
  FA1P S2_4_1 ( .A(\ab[4][1] ), .B(\CARRYB[3][1] ), .CI(\SUMB[3][2] ), .CO(
        \CARRYB[4][1] ), .S(\SUMB[4][1] ) );
  FA1P S1_4_0 ( .A(\ab[4][0] ), .B(\CARRYB[3][0] ), .CI(\SUMB[3][1] ), .CO(
        \CARRYB[4][0] ), .S(PRODUCT[4]) );
  FA1P S1_5_0 ( .A(\ab[5][0] ), .B(\CARRYB[4][0] ), .CI(\SUMB[4][1] ), .CO(
        \CARRYB[5][0] ), .S(PRODUCT[5]) );
  FA1AP S2_10_2 ( .A(\CARRYB[9][2] ), .B(1'b0), .CI(\SUMB[9][3] ), .CO(
        \CARRYB[10][2] ), .S(\SUMB[10][2] ) );
  FA1AP S2_12_1 ( .A(\CARRYB[11][1] ), .B(1'b0), .CI(\SUMB[11][2] ), .CO(
        \CARRYB[12][1] ), .S(\SUMB[12][1] ) );
  FA1 S2_6_4 ( .A(\ab[6][4] ), .B(\CARRYB[5][4] ), .CI(\SUMB[5][5] ), .CO(
        \CARRYB[6][4] ), .S(\SUMB[6][4] ) );
  FA1 S0_1 ( .A(\ab[0][2] ), .B(\ab[1][1] ), .CI(1'b0), .CO(\CARRYB[1][1] ), 
        .S(\SUMB[1][1] ) );
  FA1A S2_2_1 ( .A(\ab[2][1] ), .B(\CARRYB[1][1] ), .CI(\SUMB[1][2] ), .CO(
        \CARRYB[2][1] ), .S(\SUMB[2][1] ) );
  FA1P S1_8_0 ( .A(\ab[8][0] ), .B(\CARRYB[7][0] ), .CI(\SUMB[7][1] ), .CO(
        \CARRYB[8][0] ), .S(PRODUCT[8]) );
  FA1P S1_9_0 ( .A(1'b0), .B(\CARRYB[8][0] ), .CI(\SUMB[8][1] ), .CO(
        \CARRYB[9][0] ), .S(PRODUCT[9]) );
  EO3 S2_9_5 ( .A(1'b0), .B(\CARRYB[8][5] ), .C(\SUMB[8][6] ), .Z(\SUMB[9][5] ) );
  FA1A S2_10_4 ( .A(1'b0), .B(\CARRYB[9][4] ), .CI(\SUMB[9][5] ), .S(
        \SUMB[10][4] ) );
  FA1A S0_0 ( .A(\ab[0][1] ), .B(\ab[1][0] ), .CI(1'b0), .CO(\CARRYB[1][0] ), 
        .S(PRODUCT[1]) );
  FA1A S1_2_0 ( .A(\ab[2][0] ), .B(\CARRYB[1][0] ), .CI(\SUMB[1][1] ), .CO(
        \CARRYB[2][0] ), .S(PRODUCT[2]) );
  FA1A S2_4_5 ( .A(\ab[4][5] ), .B(\CARRYB[3][5] ), .CI(\SUMB[3][6] ), .CO(
        \CARRYB[4][5] ), .S(\SUMB[4][5] ) );
  EO3 S0_6 ( .A(1'b0), .B(\ab[1][6] ), .C(1'b0), .Z(\SUMB[1][6] ) );
  FA1A S2_2_2 ( .A(\ab[2][2] ), .B(\CARRYB[1][2] ), .CI(\SUMB[1][3] ), .CO(
        \CARRYB[2][2] ), .S(\SUMB[2][2] ) );
  FA1A S0_2 ( .A(\ab[0][3] ), .B(\ab[1][2] ), .CI(1'b0), .CO(\CARRYB[1][2] ), 
        .S(\SUMB[1][2] ) );
  FA1A S2_5_5 ( .A(\ab[5][5] ), .B(\CARRYB[4][5] ), .CI(\SUMB[4][6] ), .CO(
        \CARRYB[5][5] ), .S(\SUMB[5][5] ) );
  FA1AP S4_0 ( .A(\CARRYB[13][0] ), .B(1'b0), .CI(\SUMB[13][1] ), .S(
        PRODUCT[14]) );
  FA1P S2_2_5 ( .A(\ab[2][5] ), .B(\CARRYB[1][5] ), .CI(\SUMB[1][6] ), .CO(
        \CARRYB[2][5] ), .S(\SUMB[2][5] ) );
  FA1AP S2_2_3 ( .A(\ab[2][3] ), .B(\CARRYB[1][3] ), .CI(\SUMB[1][4] ), .CO(
        \CARRYB[2][3] ), .S(\SUMB[2][3] ) );
  FA1AP S0_5 ( .A(\ab[0][6] ), .B(1'b0), .CI(\ab[1][5] ), .CO(\CARRYB[1][5] ), 
        .S(\SUMB[1][5] ) );
  FA1P S2_3_5 ( .A(\ab[3][5] ), .B(\CARRYB[2][5] ), .CI(\SUMB[2][6] ), .CO(
        \CARRYB[3][5] ), .S(\SUMB[3][5] ) );
  FA1P S2_8_3 ( .A(\CARRYB[7][3] ), .B(\ab[8][3] ), .CI(\SUMB[7][4] ), .CO(
        \CARRYB[8][3] ), .S(\SUMB[8][3] ) );
  FA1A S2_11_3 ( .A(1'b0), .B(\CARRYB[10][3] ), .CI(\SUMB[10][4] ), .S(
        \SUMB[11][3] ) );
  FA1P S2_5_1 ( .A(\ab[5][1] ), .B(\CARRYB[4][1] ), .CI(\SUMB[4][2] ), .CO(
        \CARRYB[5][1] ), .S(\SUMB[5][1] ) );
  FA1P S0_4 ( .A(\ab[0][5] ), .B(\ab[1][4] ), .CI(1'b0), .CO(\CARRYB[1][4] ), 
        .S(\SUMB[1][4] ) );
  FA1 S2_8_1 ( .A(\ab[8][1] ), .B(\CARRYB[7][1] ), .CI(\SUMB[7][2] ), .CO(
        \CARRYB[8][1] ), .S(\SUMB[8][1] ) );
  FA1AP S2_7_3 ( .A(\ab[7][3] ), .B(\CARRYB[6][3] ), .CI(\SUMB[6][4] ), .CO(
        \CARRYB[7][3] ), .S(\SUMB[7][3] ) );
  FA1P S2_5_3 ( .A(\ab[5][3] ), .B(\CARRYB[4][3] ), .CI(\SUMB[4][4] ), .CO(
        \CARRYB[5][3] ), .S(\SUMB[5][3] ) );
  FA1P S2_3_4 ( .A(\CARRYB[2][4] ), .B(\ab[3][4] ), .CI(\SUMB[2][5] ), .CO(
        \CARRYB[3][4] ), .S(\SUMB[3][4] ) );
  FA1 S2_2_4 ( .A(\ab[2][4] ), .B(\CARRYB[1][4] ), .CI(\SUMB[1][5] ), .CO(
        \CARRYB[2][4] ), .S(\SUMB[2][4] ) );
  FA1 S2_6_1 ( .A(\ab[6][1] ), .B(\CARRYB[5][1] ), .CI(\SUMB[5][2] ), .CO(
        \CARRYB[6][1] ), .S(\SUMB[6][1] ) );
  FA1 S2_9_1 ( .A(\SUMB[8][2] ), .B(\CARRYB[8][1] ), .CI(1'b0), .CO(
        \CARRYB[9][1] ), .S(\SUMB[9][1] ) );
  FA1P S1_7_0 ( .A(\ab[7][0] ), .B(\CARRYB[6][0] ), .CI(\SUMB[6][1] ), .CO(
        \CARRYB[7][0] ), .S(PRODUCT[7]) );
  FA1 S2_7_2 ( .A(\ab[7][2] ), .B(\CARRYB[6][2] ), .CI(\SUMB[6][3] ), .CO(
        \CARRYB[7][2] ), .S(\SUMB[7][2] ) );
  FA1 S2_9_2 ( .A(\CARRYB[8][2] ), .B(1'b0), .CI(\SUMB[8][3] ), .CO(
        \CARRYB[9][2] ), .S(\SUMB[9][2] ) );
  FA1P S2_5_2 ( .A(\CARRYB[4][2] ), .B(\ab[5][2] ), .CI(\SUMB[4][3] ), .CO(
        \CARRYB[5][2] ), .S(\SUMB[5][2] ) );
  FA1P S2_4_4 ( .A(\CARRYB[3][4] ), .B(\ab[4][4] ), .CI(\SUMB[3][5] ), .CO(
        \CARRYB[4][4] ), .S(\SUMB[4][4] ) );
  FA1A S2_3_1 ( .A(\ab[3][1] ), .B(\CARRYB[2][1] ), .CI(\SUMB[2][2] ), .CO(
        \CARRYB[3][1] ), .S(\SUMB[3][1] ) );
  FA1A S2_8_4 ( .A(\ab[8][4] ), .B(\CARRYB[7][4] ), .CI(\SUMB[7][5] ), .CO(
        \CARRYB[8][4] ), .S(\SUMB[8][4] ) );
  FA1A S2_7_5 ( .A(\ab[7][5] ), .B(\CARRYB[6][5] ), .CI(\SUMB[6][6] ), .CO(
        \CARRYB[7][5] ), .S(\SUMB[7][5] ) );
  IVAP U2 ( .A(n30), .Z(\CARRYB[13][0] ) );
  IVAP U3 ( .A(n38), .Z(\CARRYB[12][0] ) );
  IVAP U4 ( .A(n35), .Z(\CARRYB[11][1] ) );
  IVAP U5 ( .A(n39), .Z(\CARRYB[10][1] ) );
  IVAP U6 ( .A(n36), .Z(\CARRYB[11][0] ) );
  IVAP U7 ( .A(n25), .Z(\CARRYB[1][3] ) );
  IVA U8 ( .A(n46), .Z(n45) );
  IVDA U9 ( .A(A[7]), .Y(n5), .Z(n6) );
  IVDA U10 ( .A(A[8]), .Y(n7), .Z(n8) );
  B2I U11 ( .A(B[3]), .Z2(n49) );
  AN2P U12 ( .A(B[5]), .B(A[1]), .Z(\ab[1][5] ) );
  EOP U13 ( .A(\SUMB[7][3] ), .B(n26), .Z(\SUMB[8][2] ) );
  EO3P U14 ( .A(\CARRYB[5][3] ), .B(\ab[6][3] ), .C(\SUMB[5][4] ), .Z(
        \SUMB[6][3] ) );
  IV U15 ( .A(B[6]), .Z(n50) );
  AN2 U16 ( .A(B[5]), .B(A[0]), .Z(\ab[0][5] ) );
  ND3P U17 ( .A(n22), .B(n23), .C(n24), .Z(\CARRYB[6][2] ) );
  EOP U18 ( .A(\CARRYB[12][1] ), .B(1'b0), .Z(n10) );
  EOP U19 ( .A(\SUMB[12][2] ), .B(n10), .Z(\SUMB[13][1] ) );
  EOP U20 ( .A(\CARRYB[2][3] ), .B(\ab[3][3] ), .Z(n11) );
  EOP U21 ( .A(\SUMB[2][4] ), .B(n11), .Z(\SUMB[3][3] ) );
  ND2 U22 ( .A(\SUMB[2][4] ), .B(\CARRYB[2][3] ), .Z(n12) );
  ND2 U23 ( .A(\SUMB[2][4] ), .B(\ab[3][3] ), .Z(n13) );
  ND2 U24 ( .A(\CARRYB[2][3] ), .B(\ab[3][3] ), .Z(n14) );
  ND3P U25 ( .A(n12), .B(n13), .C(n14), .Z(\CARRYB[3][3] ) );
  EO U26 ( .A(\CARRYB[3][3] ), .B(\ab[4][3] ), .Z(n15) );
  EOP U27 ( .A(\SUMB[3][4] ), .B(n15), .Z(\SUMB[4][3] ) );
  ND2 U28 ( .A(\SUMB[3][4] ), .B(\CARRYB[3][3] ), .Z(n16) );
  ND2 U29 ( .A(\SUMB[3][4] ), .B(\ab[4][3] ), .Z(n17) );
  ND2 U30 ( .A(\CARRYB[3][3] ), .B(\ab[4][3] ), .Z(n18) );
  ND3P U31 ( .A(n16), .B(n17), .C(n18), .Z(\CARRYB[4][3] ) );
  ND2 U32 ( .A(\SUMB[11][1] ), .B(\CARRYB[11][0] ), .Z(n38) );
  ND3P U33 ( .A(n27), .B(n28), .C(n29), .Z(\CARRYB[7][1] ) );
  ND2 U34 ( .A(\CARRYB[5][3] ), .B(\SUMB[5][4] ), .Z(n19) );
  ND2 U35 ( .A(\CARRYB[5][3] ), .B(\ab[6][3] ), .Z(n20) );
  ND2 U36 ( .A(\SUMB[5][4] ), .B(\ab[6][3] ), .Z(n21) );
  ND3 U37 ( .A(n19), .B(n20), .C(n21), .Z(\CARRYB[6][3] ) );
  EOP U38 ( .A(\SUMB[11][1] ), .B(1'b0), .Z(n37) );
  ND3 U39 ( .A(n31), .B(n32), .C(n33), .Z(\CARRYB[8][2] ) );
  EOP U40 ( .A(n37), .B(\CARRYB[11][0] ), .Z(PRODUCT[12]) );
  EO3P U41 ( .A(\CARRYB[5][2] ), .B(\ab[6][2] ), .C(\SUMB[5][3] ), .Z(
        \SUMB[6][2] ) );
  ND2 U42 ( .A(\CARRYB[5][2] ), .B(\SUMB[5][3] ), .Z(n22) );
  ND2 U43 ( .A(\CARRYB[5][2] ), .B(\ab[6][2] ), .Z(n23) );
  ND2 U44 ( .A(\SUMB[5][3] ), .B(\ab[6][2] ), .Z(n24) );
  EO3 U45 ( .A(\ab[0][4] ), .B(1'b0), .C(\ab[1][3] ), .Z(\SUMB[1][3] ) );
  ND2 U46 ( .A(\ab[0][4] ), .B(\ab[1][3] ), .Z(n25) );
  AN2P U47 ( .A(B[4]), .B(A[0]), .Z(\ab[0][4] ) );
  EOP U48 ( .A(\CARRYB[7][2] ), .B(\ab[8][2] ), .Z(n26) );
  AN2 U49 ( .A(A[2]), .B(n45), .Z(\ab[2][5] ) );
  EO3P U50 ( .A(\CARRYB[6][1] ), .B(\ab[7][1] ), .C(\SUMB[6][2] ), .Z(
        \SUMB[7][1] ) );
  ND2 U51 ( .A(\CARRYB[6][1] ), .B(\SUMB[6][2] ), .Z(n27) );
  ND2 U52 ( .A(\CARRYB[6][1] ), .B(\ab[7][1] ), .Z(n28) );
  ND2 U53 ( .A(\SUMB[6][2] ), .B(\ab[7][1] ), .Z(n29) );
  EO3P U54 ( .A(\CARRYB[12][0] ), .B(1'b0), .C(\SUMB[12][1] ), .Z(PRODUCT[13])
         );
  ND2 U55 ( .A(\CARRYB[12][0] ), .B(\SUMB[12][1] ), .Z(n30) );
  ND2 U56 ( .A(\CARRYB[7][2] ), .B(\SUMB[7][3] ), .Z(n31) );
  ND2 U57 ( .A(\CARRYB[7][2] ), .B(\ab[8][2] ), .Z(n32) );
  ND2 U58 ( .A(\SUMB[7][3] ), .B(\ab[8][2] ), .Z(n33) );
  EO3P U59 ( .A(\CARRYB[9][1] ), .B(1'b0), .C(\SUMB[9][2] ), .Z(\SUMB[10][1] )
         );
  EOP U60 ( .A(\CARRYB[10][1] ), .B(1'b0), .Z(n34) );
  EOP U61 ( .A(\SUMB[10][2] ), .B(n34), .Z(\SUMB[11][1] ) );
  ND2 U62 ( .A(\SUMB[10][2] ), .B(\CARRYB[10][1] ), .Z(n35) );
  EO3P U63 ( .A(\CARRYB[10][0] ), .B(1'b0), .C(\SUMB[10][1] ), .Z(PRODUCT[11])
         );
  ND2P U64 ( .A(\CARRYB[10][0] ), .B(\SUMB[10][1] ), .Z(n36) );
  ND2 U65 ( .A(\CARRYB[9][1] ), .B(\SUMB[9][2] ), .Z(n39) );
  EOP U66 ( .A(\SUMB[2][3] ), .B(\ab[3][2] ), .Z(n40) );
  EOP U67 ( .A(\CARRYB[2][2] ), .B(n40), .Z(\SUMB[3][2] ) );
  ND2 U68 ( .A(\CARRYB[2][2] ), .B(\SUMB[2][3] ), .Z(n41) );
  ND2 U69 ( .A(\CARRYB[2][2] ), .B(\ab[3][2] ), .Z(n42) );
  ND2 U70 ( .A(\SUMB[2][3] ), .B(\ab[3][2] ), .Z(n43) );
  ND3P U71 ( .A(n41), .B(n42), .C(n43), .Z(\CARRYB[3][2] ) );
  IVDA U72 ( .A(B[0]), .Z(n48) );
  NR2 U73 ( .A(n50), .B(n5), .Z(\SUMB[7][6] ) );
  NR2 U74 ( .A(n50), .B(n7), .Z(\SUMB[8][6] ) );
  IVDA U75 ( .A(B[5]), .Y(n46), .Z(n47) );
  AN2P U76 ( .A(B[4]), .B(A[1]), .Z(\ab[1][4] ) );
  AN2P U77 ( .A(n49), .B(A[0]), .Z(\ab[0][3] ) );
  AN2 U78 ( .A(B[2]), .B(A[0]), .Z(\ab[0][2] ) );
  AN2 U79 ( .A(B[2]), .B(A[1]), .Z(\ab[1][2] ) );
  AN2 U80 ( .A(B[1]), .B(A[0]), .Z(\ab[0][1] ) );
  AN2P U81 ( .A(A[1]), .B(B[6]), .Z(\ab[1][6] ) );
  AN2P U82 ( .A(A[0]), .B(B[6]), .Z(\ab[0][6] ) );
  AN2P U83 ( .A(n49), .B(A[1]), .Z(\ab[1][3] ) );
  AN2P U84 ( .A(B[1]), .B(A[1]), .Z(\ab[1][1] ) );
  AN2P U85 ( .A(n48), .B(A[1]), .Z(\ab[1][0] ) );
  AN2P U86 ( .A(A[2]), .B(B[6]), .Z(\SUMB[2][6] ) );
  AN2P U87 ( .A(A[2]), .B(B[4]), .Z(\ab[2][4] ) );
  AN2P U88 ( .A(A[2]), .B(n49), .Z(\ab[2][3] ) );
  AN2P U89 ( .A(A[2]), .B(B[2]), .Z(\ab[2][2] ) );
  AN2P U90 ( .A(A[2]), .B(B[1]), .Z(\ab[2][1] ) );
  AN2P U91 ( .A(A[2]), .B(n48), .Z(\ab[2][0] ) );
  AN2P U92 ( .A(A[3]), .B(B[6]), .Z(\SUMB[3][6] ) );
  AN2P U93 ( .A(A[3]), .B(n45), .Z(\ab[3][5] ) );
  AN2P U94 ( .A(A[3]), .B(B[4]), .Z(\ab[3][4] ) );
  AN2P U95 ( .A(A[3]), .B(n49), .Z(\ab[3][3] ) );
  AN2P U96 ( .A(A[3]), .B(B[2]), .Z(\ab[3][2] ) );
  AN2P U97 ( .A(A[3]), .B(B[1]), .Z(\ab[3][1] ) );
  AN2P U98 ( .A(A[3]), .B(n48), .Z(\ab[3][0] ) );
  AN2P U99 ( .A(A[4]), .B(B[6]), .Z(\SUMB[4][6] ) );
  AN2P U100 ( .A(A[4]), .B(n47), .Z(\ab[4][5] ) );
  AN2P U101 ( .A(A[4]), .B(B[4]), .Z(\ab[4][4] ) );
  AN2P U102 ( .A(A[4]), .B(n49), .Z(\ab[4][3] ) );
  AN2P U103 ( .A(A[4]), .B(B[2]), .Z(\ab[4][2] ) );
  AN2P U104 ( .A(A[4]), .B(B[1]), .Z(\ab[4][1] ) );
  AN2P U105 ( .A(A[4]), .B(n48), .Z(\ab[4][0] ) );
  AN2P U106 ( .A(A[5]), .B(B[6]), .Z(\SUMB[5][6] ) );
  AN2P U107 ( .A(A[5]), .B(n47), .Z(\ab[5][5] ) );
  AN2P U108 ( .A(A[5]), .B(B[4]), .Z(\ab[5][4] ) );
  AN2P U109 ( .A(A[5]), .B(n49), .Z(\ab[5][3] ) );
  AN2P U110 ( .A(A[5]), .B(B[2]), .Z(\ab[5][2] ) );
  AN2P U111 ( .A(A[5]), .B(B[1]), .Z(\ab[5][1] ) );
  AN2P U112 ( .A(A[5]), .B(n48), .Z(\ab[5][0] ) );
  AN2P U113 ( .A(A[6]), .B(B[6]), .Z(\SUMB[6][6] ) );
  AN2P U114 ( .A(A[6]), .B(n47), .Z(\ab[6][5] ) );
  AN2P U115 ( .A(A[6]), .B(B[4]), .Z(\ab[6][4] ) );
  AN2P U116 ( .A(A[6]), .B(n49), .Z(\ab[6][3] ) );
  AN2P U117 ( .A(A[6]), .B(B[2]), .Z(\ab[6][2] ) );
  AN2P U118 ( .A(A[6]), .B(B[1]), .Z(\ab[6][1] ) );
  AN2P U119 ( .A(A[6]), .B(n48), .Z(\ab[6][0] ) );
  AN2P U120 ( .A(n6), .B(n47), .Z(\ab[7][5] ) );
  AN2P U121 ( .A(n6), .B(B[4]), .Z(\ab[7][4] ) );
  AN2P U122 ( .A(n6), .B(n49), .Z(\ab[7][3] ) );
  AN2P U123 ( .A(n6), .B(B[2]), .Z(\ab[7][2] ) );
  AN2P U124 ( .A(n6), .B(B[1]), .Z(\ab[7][1] ) );
  AN2P U125 ( .A(n6), .B(n48), .Z(\ab[7][0] ) );
  AN2P U126 ( .A(n8), .B(n47), .Z(\ab[8][5] ) );
  AN2P U127 ( .A(n8), .B(B[4]), .Z(\ab[8][4] ) );
  AN2P U128 ( .A(n8), .B(n49), .Z(\ab[8][3] ) );
  AN2P U129 ( .A(n8), .B(B[2]), .Z(\ab[8][2] ) );
  AN2P U130 ( .A(n8), .B(B[1]), .Z(\ab[8][1] ) );
  AN2P U131 ( .A(n8), .B(n48), .Z(\ab[8][0] ) );
  AN2P U132 ( .A(n48), .B(A[0]), .Z(PRODUCT[0]) );
endmodule


module compute_xy_center_DW02_mult_0 ( A, B, TC, PRODUCT );
  input [14:0] A;
  input [14:0] B;
  output [29:0] PRODUCT;
  input TC;
  wire   \ab[8][5] , \ab[8][4] , \ab[8][3] , \ab[8][2] , \ab[8][1] ,
         \ab[8][0] , \ab[7][5] , \ab[7][4] , \ab[7][3] , \ab[7][2] ,
         \ab[7][1] , \ab[7][0] , \ab[6][5] , \ab[6][4] , \ab[6][3] ,
         \ab[6][2] , \ab[6][1] , \ab[6][0] , \ab[5][5] , \ab[5][4] ,
         \ab[5][3] , \ab[5][2] , \ab[5][1] , \ab[5][0] , \ab[4][5] ,
         \ab[4][4] , \ab[4][3] , \ab[4][2] , \ab[4][1] , \ab[4][0] ,
         \ab[3][5] , \ab[3][4] , \ab[3][3] , \ab[3][2] , \ab[3][1] ,
         \ab[3][0] , \ab[2][5] , \ab[2][4] , \ab[2][3] , \ab[2][2] ,
         \ab[2][1] , \ab[2][0] , \ab[1][6] , \ab[1][5] , \ab[1][3] ,
         \ab[1][2] , \ab[1][1] , \ab[1][0] , \ab[0][6] , \ab[0][5] ,
         \ab[0][4] , \ab[0][3] , \ab[0][2] , \ab[0][1] , \CARRYB[13][0] ,
         \CARRYB[12][1] , \CARRYB[12][0] , \CARRYB[11][2] , \CARRYB[11][1] ,
         \CARRYB[11][0] , \CARRYB[10][3] , \CARRYB[10][2] , \CARRYB[10][1] ,
         \CARRYB[10][0] , \CARRYB[9][4] , \CARRYB[9][3] , \CARRYB[9][2] ,
         \CARRYB[9][1] , \CARRYB[9][0] , \CARRYB[8][5] , \CARRYB[8][4] ,
         \CARRYB[8][3] , \CARRYB[8][2] , \CARRYB[8][1] , \CARRYB[8][0] ,
         \CARRYB[7][5] , \CARRYB[7][4] , \CARRYB[7][3] , \CARRYB[7][2] ,
         \CARRYB[7][1] , \CARRYB[7][0] , \CARRYB[6][5] , \CARRYB[6][4] ,
         \CARRYB[6][3] , \CARRYB[6][2] , \CARRYB[6][1] , \CARRYB[6][0] ,
         \CARRYB[5][5] , \CARRYB[5][4] , \CARRYB[5][3] , \CARRYB[5][2] ,
         \CARRYB[5][1] , \CARRYB[5][0] , \CARRYB[4][5] , \CARRYB[4][4] ,
         \CARRYB[4][3] , \CARRYB[4][2] , \CARRYB[4][1] , \CARRYB[4][0] ,
         \CARRYB[3][5] , \CARRYB[3][4] , \CARRYB[3][3] , \CARRYB[3][2] ,
         \CARRYB[3][1] , \CARRYB[3][0] , \CARRYB[2][5] , \CARRYB[2][4] ,
         \CARRYB[2][3] , \CARRYB[2][2] , \CARRYB[2][1] , \CARRYB[2][0] ,
         \CARRYB[1][5] , \CARRYB[1][4] , \CARRYB[1][3] , \CARRYB[1][2] ,
         \CARRYB[1][1] , \CARRYB[1][0] , \SUMB[13][1] , \SUMB[12][2] ,
         \SUMB[12][1] , \SUMB[11][3] , \SUMB[11][2] , \SUMB[11][1] ,
         \SUMB[10][4] , \SUMB[10][3] , \SUMB[10][2] , \SUMB[10][1] ,
         \SUMB[9][5] , \SUMB[9][4] , \SUMB[9][3] , \SUMB[9][2] , \SUMB[9][1] ,
         \SUMB[8][6] , \SUMB[8][5] , \SUMB[8][4] , \SUMB[8][3] , \SUMB[8][2] ,
         \SUMB[8][1] , \SUMB[7][6] , \SUMB[7][5] , \SUMB[7][4] , \SUMB[7][3] ,
         \SUMB[7][2] , \SUMB[7][1] , \SUMB[6][6] , \SUMB[6][5] , \SUMB[6][4] ,
         \SUMB[6][2] , \SUMB[6][1] , \SUMB[5][6] , \SUMB[5][5] , \SUMB[5][4] ,
         \SUMB[5][3] , \SUMB[5][2] , \SUMB[5][1] , \SUMB[4][6] , \SUMB[4][5] ,
         \SUMB[4][4] , \SUMB[4][3] , \SUMB[4][2] , \SUMB[4][1] , \SUMB[3][6] ,
         \SUMB[3][5] , \SUMB[3][4] , \SUMB[3][3] , \SUMB[3][2] , \SUMB[3][1] ,
         \SUMB[2][6] , \SUMB[2][5] , \SUMB[2][4] , \SUMB[2][3] , \SUMB[2][2] ,
         \SUMB[2][1] , \SUMB[1][6] , \SUMB[1][5] , \SUMB[1][4] , \SUMB[1][3] ,
         \SUMB[1][2] , \SUMB[1][1] , n6, n7, n8, n10, n11, n13, n14, n16, n17,
         n18, n19, n20, n21, n22, n24, n25, n26, n27, n28, n29, n32, n33, n34,
         n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48,
         n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62,
         n63, n64, n65, n66, n67, n68, n70, n71;

  FA1AP S2_7_5 ( .A(\ab[7][5] ), .B(\CARRYB[6][5] ), .CI(\SUMB[6][6] ), .CO(
        \CARRYB[7][5] ), .S(\SUMB[7][5] ) );
  FA1AP S2_2_4 ( .A(\CARRYB[1][4] ), .B(\ab[2][4] ), .CI(\SUMB[1][5] ), .CO(
        \CARRYB[2][4] ), .S(\SUMB[2][4] ) );
  FA1P S1_3_0 ( .A(\ab[3][0] ), .B(\CARRYB[2][0] ), .CI(\SUMB[2][1] ), .CO(
        \CARRYB[3][0] ), .S(PRODUCT[3]) );
  FA1P S1_4_0 ( .A(\ab[4][0] ), .B(\CARRYB[3][0] ), .CI(\SUMB[3][1] ), .CO(
        \CARRYB[4][0] ), .S(PRODUCT[4]) );
  FA1P S2_3_1 ( .A(\ab[3][1] ), .B(\CARRYB[2][1] ), .CI(\SUMB[2][2] ), .CO(
        \CARRYB[3][1] ), .S(\SUMB[3][1] ) );
  FA1A S2_11_1 ( .A(\CARRYB[10][1] ), .B(1'b0), .CI(\SUMB[10][2] ), .CO(
        \CARRYB[11][1] ), .S(\SUMB[11][1] ) );
  FA1A S2_9_2 ( .A(\CARRYB[8][2] ), .B(1'b0), .CI(\SUMB[8][3] ), .CO(
        \CARRYB[9][2] ), .S(\SUMB[9][2] ) );
  FA1P S1_11_0 ( .A(1'b0), .B(\CARRYB[10][0] ), .CI(\SUMB[10][1] ), .CO(
        \CARRYB[11][0] ), .S(PRODUCT[11]) );
  FA1P S1_12_0 ( .A(1'b0), .B(\CARRYB[11][0] ), .CI(\SUMB[11][1] ), .CO(
        \CARRYB[12][0] ), .S(PRODUCT[12]) );
  FA1AP S2_12_1 ( .A(\CARRYB[11][1] ), .B(1'b0), .CI(\SUMB[11][2] ), .CO(
        \CARRYB[12][1] ), .S(\SUMB[12][1] ) );
  FA1A S2_10_1 ( .A(\CARRYB[9][1] ), .B(1'b0), .CI(\SUMB[9][2] ), .CO(
        \CARRYB[10][1] ), .S(\SUMB[10][1] ) );
  FA1A S2_7_2 ( .A(\CARRYB[6][2] ), .B(\ab[7][2] ), .CI(n19), .CO(
        \CARRYB[7][2] ), .S(\SUMB[7][2] ) );
  FA1A S2_6_2 ( .A(\CARRYB[5][2] ), .B(\ab[6][2] ), .CI(\SUMB[5][3] ), .CO(
        \CARRYB[6][2] ), .S(\SUMB[6][2] ) );
  FA1 S0_0 ( .A(\ab[0][1] ), .B(\ab[1][0] ), .CI(1'b0), .CO(\CARRYB[1][0] ), 
        .S(PRODUCT[1]) );
  FA1P S1_2_0 ( .A(\ab[2][0] ), .B(\CARRYB[1][0] ), .CI(\SUMB[1][1] ), .CO(
        \CARRYB[2][0] ), .S(PRODUCT[2]) );
  FA1A S2_8_2 ( .A(\CARRYB[7][2] ), .B(\ab[8][2] ), .CI(\SUMB[7][3] ), .CO(
        \CARRYB[8][2] ), .S(\SUMB[8][2] ) );
  FA1 S2_9_3 ( .A(1'b0), .B(\CARRYB[8][3] ), .CI(\SUMB[8][4] ), .CO(
        \CARRYB[9][3] ), .S(\SUMB[9][3] ) );
  FA1A S2_10_2 ( .A(\CARRYB[9][2] ), .B(1'b0), .CI(\SUMB[9][3] ), .CO(
        \CARRYB[10][2] ), .S(\SUMB[10][2] ) );
  EO3 S2_9_5 ( .A(1'b0), .B(\CARRYB[8][5] ), .C(\SUMB[8][6] ), .Z(\SUMB[9][5] ) );
  FA1A S2_10_4 ( .A(1'b0), .B(\CARRYB[9][4] ), .CI(\SUMB[9][5] ), .S(
        \SUMB[10][4] ) );
  FA1A S2_9_4 ( .A(1'b0), .B(\CARRYB[8][4] ), .CI(\SUMB[8][5] ), .CO(
        \CARRYB[9][4] ), .S(\SUMB[9][4] ) );
  FA1A S2_8_5 ( .A(\ab[8][5] ), .B(\CARRYB[7][5] ), .CI(\SUMB[7][6] ), .CO(
        \CARRYB[8][5] ), .S(\SUMB[8][5] ) );
  FA1A S2_5_2 ( .A(\CARRYB[4][2] ), .B(\ab[5][2] ), .CI(\SUMB[4][3] ), .CO(
        \CARRYB[5][2] ), .S(\SUMB[5][2] ) );
  FA1A S2_4_3 ( .A(\CARRYB[3][3] ), .B(\ab[4][3] ), .CI(\SUMB[3][4] ), .CO(
        \CARRYB[4][3] ), .S(\SUMB[4][3] ) );
  FA1 S0_5 ( .A(1'b0), .B(\ab[1][5] ), .CI(\ab[0][6] ), .CO(\CARRYB[1][5] ), 
        .S(\SUMB[1][5] ) );
  FA1AP S2_2_2 ( .A(\ab[2][2] ), .B(\CARRYB[1][2] ), .CI(\SUMB[1][3] ), .CO(
        \CARRYB[2][2] ), .S(\SUMB[2][2] ) );
  FA1A S2_5_3 ( .A(\CARRYB[4][3] ), .B(\ab[5][3] ), .CI(\SUMB[4][4] ), .CO(
        \CARRYB[5][3] ), .S(\SUMB[5][3] ) );
  FA1P S1_8_0 ( .A(\ab[8][0] ), .B(\CARRYB[7][0] ), .CI(\SUMB[7][1] ), .CO(
        \CARRYB[8][0] ), .S(PRODUCT[8]) );
  FA1 S1_9_0 ( .A(1'b0), .B(\CARRYB[8][0] ), .CI(\SUMB[8][1] ), .CO(
        \CARRYB[9][0] ), .S(PRODUCT[9]) );
  FA1AP S2_10_3 ( .A(1'b0), .B(\CARRYB[9][3] ), .CI(\SUMB[9][4] ), .CO(
        \CARRYB[10][3] ), .S(\SUMB[10][3] ) );
  FA1AP S0_4 ( .A(n36), .B(1'b0), .CI(\ab[0][5] ), .CO(\CARRYB[1][4] ), .S(
        \SUMB[1][4] ) );
  FA1A S2_3_3 ( .A(\CARRYB[2][3] ), .B(\ab[3][3] ), .CI(\SUMB[2][4] ), .CO(
        \CARRYB[3][3] ), .S(\SUMB[3][3] ) );
  FA1P S2_7_1 ( .A(\ab[7][1] ), .B(\CARRYB[6][1] ), .CI(\SUMB[6][2] ), .CO(
        \CARRYB[7][1] ), .S(\SUMB[7][1] ) );
  FA1P S2_3_5 ( .A(\CARRYB[2][5] ), .B(\ab[3][5] ), .CI(\SUMB[2][6] ), .CO(
        \CARRYB[3][5] ), .S(\SUMB[3][5] ) );
  FA1P S2_8_4 ( .A(\ab[8][4] ), .B(\CARRYB[7][4] ), .CI(\SUMB[7][5] ), .CO(
        \CARRYB[8][4] ), .S(\SUMB[8][4] ) );
  FA1P S2_6_5 ( .A(\ab[6][5] ), .B(\CARRYB[5][5] ), .CI(\SUMB[5][6] ), .CO(
        \CARRYB[6][5] ), .S(\SUMB[6][5] ) );
  FA1P S2_7_4 ( .A(\ab[7][4] ), .B(\CARRYB[6][4] ), .CI(\SUMB[6][5] ), .CO(
        \CARRYB[7][4] ), .S(\SUMB[7][4] ) );
  FA1AP S2_8_3 ( .A(\ab[8][3] ), .B(\CARRYB[7][3] ), .CI(\SUMB[7][4] ), .CO(
        \CARRYB[8][3] ), .S(\SUMB[8][3] ) );
  FA1 S1_6_0 ( .A(\ab[6][0] ), .B(\CARRYB[5][0] ), .CI(\SUMB[5][1] ), .CO(
        \CARRYB[6][0] ), .S(PRODUCT[6]) );
  FA1P S1_7_0 ( .A(\ab[7][0] ), .B(\CARRYB[6][0] ), .CI(\SUMB[6][1] ), .CO(
        \CARRYB[7][0] ), .S(PRODUCT[7]) );
  FA1 S2_6_4 ( .A(\CARRYB[5][4] ), .B(\ab[6][4] ), .CI(\SUMB[5][5] ), .CO(
        \CARRYB[6][4] ), .S(\SUMB[6][4] ) );
  FA1P S2_4_5 ( .A(\ab[4][5] ), .B(\CARRYB[3][5] ), .CI(\SUMB[3][6] ), .CO(
        \CARRYB[4][5] ), .S(\SUMB[4][5] ) );
  FA1P S2_5_5 ( .A(\ab[5][5] ), .B(\CARRYB[4][5] ), .CI(\SUMB[4][6] ), .CO(
        \CARRYB[5][5] ), .S(\SUMB[5][5] ) );
  EO3P S0_6 ( .A(1'b0), .B(\ab[1][6] ), .C(1'b0), .Z(\SUMB[1][6] ) );
  FA1A S2_6_3 ( .A(\CARRYB[5][3] ), .B(\ab[6][3] ), .CI(\SUMB[5][4] ), .CO(
        \CARRYB[6][3] ) );
  FA1P S2_5_1 ( .A(\ab[5][1] ), .B(\CARRYB[4][1] ), .CI(\SUMB[4][2] ), .CO(
        \CARRYB[5][1] ), .S(\SUMB[5][1] ) );
  FA1 S2_13_1 ( .A(\CARRYB[12][1] ), .B(1'b0), .CI(\SUMB[12][2] ), .S(
        \SUMB[13][1] ) );
  FA1AP S2_2_5 ( .A(\CARRYB[1][5] ), .B(\ab[2][5] ), .CI(\SUMB[1][6] ), .CO(
        \CARRYB[2][5] ), .S(\SUMB[2][5] ) );
  FA1A S2_4_4 ( .A(\ab[4][4] ), .B(\CARRYB[3][4] ), .CI(\SUMB[3][5] ), .CO(
        \CARRYB[4][4] ), .S(\SUMB[4][4] ) );
  FA1AP S1_5_0 ( .A(\ab[5][0] ), .B(\CARRYB[4][0] ), .CI(\SUMB[4][1] ), .CO(
        \CARRYB[5][0] ), .S(PRODUCT[5]) );
  FA1A S2_4_1 ( .A(\ab[4][1] ), .B(\CARRYB[3][1] ), .CI(\SUMB[3][2] ), .CO(
        \CARRYB[4][1] ), .S(\SUMB[4][1] ) );
  FA1 S0_2 ( .A(\ab[0][3] ), .B(\ab[1][2] ), .CI(1'b0), .CO(\CARRYB[1][2] ), 
        .S(\SUMB[1][2] ) );
  FA1 S2_3_4 ( .A(\CARRYB[2][4] ), .B(\ab[3][4] ), .CI(\SUMB[2][5] ), .CO(
        \CARRYB[3][4] ), .S(\SUMB[3][4] ) );
  FA1 S0_1 ( .A(\ab[0][2] ), .B(\ab[1][1] ), .CI(1'b0), .CO(\CARRYB[1][1] ), 
        .S(\SUMB[1][1] ) );
  FA1AP S2_2_1 ( .A(\ab[2][1] ), .B(\CARRYB[1][1] ), .CI(\SUMB[1][2] ), .CO(
        \CARRYB[2][1] ), .S(\SUMB[2][1] ) );
  FA1AP S1_10_0 ( .A(1'b0), .B(\CARRYB[9][0] ), .CI(\SUMB[9][1] ), .CO(
        \CARRYB[10][0] ), .S(PRODUCT[10]) );
  FA1P S2_8_1 ( .A(\ab[8][1] ), .B(\CARRYB[7][1] ), .CI(\SUMB[7][2] ), .CO(
        \CARRYB[8][1] ), .S(\SUMB[8][1] ) );
  FA1P S2_6_1 ( .A(\ab[6][1] ), .B(\CARRYB[5][1] ), .CI(\SUMB[5][2] ), .CO(
        \CARRYB[6][1] ), .S(\SUMB[6][1] ) );
  EO3 S2_11_3 ( .A(1'b0), .B(\CARRYB[10][3] ), .C(\SUMB[10][4] ), .Z(
        \SUMB[11][3] ) );
  IVAP U2 ( .A(n55), .Z(\CARRYB[9][1] ) );
  IVAP U3 ( .A(n46), .Z(\CARRYB[11][2] ) );
  IVAP U4 ( .A(n42), .Z(\CARRYB[1][3] ) );
  ND3 U5 ( .A(n42), .B(1'b1), .C(1'b1), .Z(n37) );
  IVAP U6 ( .A(n56), .Z(\CARRYB[13][0] ) );
  IVDA U8 ( .A(\SUMB[1][4] ), .Y(n6), .Z(n7) );
  IVA U9 ( .A(A[0]), .Z(n60) );
  EOP U10 ( .A(n16), .B(n8), .Z(PRODUCT[13]) );
  ENP U11 ( .A(\CARRYB[12][0] ), .B(1'b0), .Z(n8) );
  EOP U12 ( .A(\SUMB[8][2] ), .B(1'b0), .Z(n54) );
  B2I U13 ( .A(B[5]), .Z2(n66) );
  AO3 U14 ( .A(n17), .B(n18), .C(n40), .D(n41), .Z(n10) );
  AO3 U15 ( .A(n17), .B(n18), .C(n40), .D(n41), .Z(n11) );
  AO3 U16 ( .A(n17), .B(n18), .C(n40), .D(n41), .Z(\CARRYB[3][2] ) );
  EOP U17 ( .A(n11), .B(n50), .Z(\SUMB[4][2] ) );
  B3I U18 ( .A(B[3]), .Z1(n61), .Z2(n64) );
  AN2P U19 ( .A(A[2]), .B(B[1]), .Z(\ab[2][1] ) );
  B2I U20 ( .A(B[0]), .Z2(n63) );
  AO3 U21 ( .A(n13), .B(n14), .C(n49), .D(n48), .Z(\CARRYB[2][3] ) );
  IVDA U22 ( .A(\ab[2][3] ), .Y(n13) );
  IV U23 ( .A(\SUMB[1][4] ), .Z(n14) );
  EO3P U24 ( .A(1'b0), .B(\CARRYB[11][2] ), .C(\SUMB[11][3] ), .Z(
        \SUMB[12][2] ) );
  IV U25 ( .A(\SUMB[12][1] ), .Z(n16) );
  IVP U26 ( .A(n21), .Z(n17) );
  IVA U27 ( .A(\CARRYB[2][2] ), .Z(n18) );
  NR2P U28 ( .A(n59), .B(n60), .Z(\ab[0][4] ) );
  IVAP U29 ( .A(n61), .Z(n62) );
  FA1AP U30 ( .A(\CARRYB[5][3] ), .B(\ab[6][3] ), .CI(\SUMB[5][4] ), .CO(n20), 
        .S(n19) );
  EO3P U31 ( .A(\SUMB[4][5] ), .B(\ab[5][4] ), .C(\CARRYB[4][4] ), .Z(
        \SUMB[5][4] ) );
  EO U32 ( .A(n21), .B(n39), .Z(\SUMB[3][2] ) );
  ND2 U33 ( .A(n26), .B(n27), .Z(n21) );
  ND2 U34 ( .A(n26), .B(n27), .Z(\SUMB[2][3] ) );
  ND2P U35 ( .A(n7), .B(n25), .Z(n26) );
  AN2P U36 ( .A(B[5]), .B(A[1]), .Z(\ab[1][5] ) );
  EO U37 ( .A(\SUMB[3][3] ), .B(\ab[4][2] ), .Z(n50) );
  B2I U38 ( .A(B[6]), .Z2(n22) );
  IVDA U39 ( .A(B[4]), .Y(n59), .Z(n65) );
  EOP U40 ( .A(\CARRYB[13][0] ), .B(1'b0), .Z(n24) );
  EOP U41 ( .A(\SUMB[13][1] ), .B(n24), .Z(PRODUCT[14]) );
  ND2 U42 ( .A(n6), .B(n47), .Z(n27) );
  IV U43 ( .A(n47), .Z(n25) );
  NR2P U44 ( .A(n59), .B(n60), .Z(n28) );
  IVDA U45 ( .A(\CARRYB[10][2] ), .Z(n29) );
  ND2P U46 ( .A(\CARRYB[2][2] ), .B(\ab[3][2] ), .Z(n41) );
  B2I U47 ( .A(B[2]), .Z2(n57) );
  AN2P U48 ( .A(n64), .B(A[1]), .Z(n32) );
  ND2 U49 ( .A(\SUMB[4][5] ), .B(\CARRYB[4][4] ), .Z(n33) );
  ND2 U50 ( .A(\SUMB[4][5] ), .B(\ab[5][4] ), .Z(n34) );
  ND2 U51 ( .A(\CARRYB[4][4] ), .B(\ab[5][4] ), .Z(n35) );
  ND3P U52 ( .A(n33), .B(n34), .C(n35), .Z(\CARRYB[5][4] ) );
  AN2 U53 ( .A(B[4]), .B(A[1]), .Z(n36) );
  AN2 U54 ( .A(B[5]), .B(n58), .Z(\ab[0][5] ) );
  FA1A U55 ( .A(\CARRYB[5][4] ), .B(\ab[6][4] ), .CI(\SUMB[5][5] ), .S(n38) );
  EOP U56 ( .A(\CARRYB[2][2] ), .B(\ab[3][2] ), .Z(n39) );
  ND2P U57 ( .A(\SUMB[2][3] ), .B(\ab[3][2] ), .Z(n40) );
  EO3 U58 ( .A(n32), .B(1'b0), .C(n28), .Z(\SUMB[1][3] ) );
  ND2 U59 ( .A(\ab[1][3] ), .B(\ab[0][4] ), .Z(n42) );
  EO3 U60 ( .A(\CARRYB[6][3] ), .B(\ab[7][3] ), .C(\SUMB[6][4] ), .Z(
        \SUMB[7][3] ) );
  ND2 U61 ( .A(n20), .B(n38), .Z(n43) );
  ND2 U62 ( .A(n20), .B(\ab[7][3] ), .Z(n44) );
  ND2 U63 ( .A(n38), .B(\ab[7][3] ), .Z(n45) );
  ND3 U64 ( .A(n43), .B(n44), .C(n45), .Z(\CARRYB[7][3] ) );
  EO3 U65 ( .A(\CARRYB[10][2] ), .B(1'b0), .C(\SUMB[10][3] ), .Z(\SUMB[11][2] ) );
  ND2 U66 ( .A(n29), .B(\SUMB[10][3] ), .Z(n46) );
  EOP U67 ( .A(\CARRYB[1][3] ), .B(\ab[2][3] ), .Z(n47) );
  ND2 U68 ( .A(\SUMB[1][4] ), .B(n37), .Z(n48) );
  ND2 U69 ( .A(n37), .B(\ab[2][3] ), .Z(n49) );
  ND3 U70 ( .A(n51), .B(n52), .C(n53), .Z(\CARRYB[4][2] ) );
  ND2 U71 ( .A(n10), .B(\SUMB[3][3] ), .Z(n51) );
  ND2 U72 ( .A(\CARRYB[3][2] ), .B(\ab[4][2] ), .Z(n52) );
  ND2 U73 ( .A(\SUMB[3][3] ), .B(\ab[4][2] ), .Z(n53) );
  EOP U74 ( .A(\CARRYB[8][1] ), .B(n54), .Z(\SUMB[9][1] ) );
  ND2 U75 ( .A(\CARRYB[8][1] ), .B(\SUMB[8][2] ), .Z(n55) );
  ND2 U76 ( .A(\SUMB[12][1] ), .B(\CARRYB[12][0] ), .Z(n56) );
  IV U77 ( .A(n60), .Z(n58) );
  IVP U78 ( .A(n71), .Z(n67) );
  IVP U79 ( .A(A[7]), .Z(n71) );
  IVP U80 ( .A(n70), .Z(n68) );
  IVP U81 ( .A(A[8]), .Z(n70) );
  AN2 U82 ( .A(A[5]), .B(n62), .Z(\ab[5][3] ) );
  AN2 U83 ( .A(B[2]), .B(A[0]), .Z(\ab[0][2] ) );
  NR2P U84 ( .A(n61), .B(n60), .Z(\ab[0][3] ) );
  AN2P U85 ( .A(A[3]), .B(n65), .Z(\ab[3][4] ) );
  AN2P U86 ( .A(A[2]), .B(n65), .Z(\ab[2][4] ) );
  AN2 U87 ( .A(A[4]), .B(n65), .Z(\ab[4][4] ) );
  AN2P U88 ( .A(B[1]), .B(A[1]), .Z(\ab[1][1] ) );
  AN2 U89 ( .A(A[5]), .B(B[1]), .Z(\ab[5][1] ) );
  AN2 U90 ( .A(B[1]), .B(A[0]), .Z(\ab[0][1] ) );
  AN2P U91 ( .A(A[1]), .B(B[6]), .Z(\ab[1][6] ) );
  AN2P U92 ( .A(A[0]), .B(B[6]), .Z(\ab[0][6] ) );
  AN2P U93 ( .A(n64), .B(A[1]), .Z(\ab[1][3] ) );
  AN2P U94 ( .A(B[2]), .B(A[1]), .Z(\ab[1][2] ) );
  AN2P U95 ( .A(n63), .B(A[1]), .Z(\ab[1][0] ) );
  AN2P U96 ( .A(A[2]), .B(n22), .Z(\SUMB[2][6] ) );
  AN2P U97 ( .A(A[2]), .B(n66), .Z(\ab[2][5] ) );
  AN2P U98 ( .A(A[2]), .B(n62), .Z(\ab[2][3] ) );
  AN2P U99 ( .A(A[2]), .B(B[2]), .Z(\ab[2][2] ) );
  AN2P U100 ( .A(A[2]), .B(n63), .Z(\ab[2][0] ) );
  AN2P U101 ( .A(A[3]), .B(n22), .Z(\SUMB[3][6] ) );
  AN2P U102 ( .A(A[3]), .B(n66), .Z(\ab[3][5] ) );
  AN2P U103 ( .A(A[3]), .B(n62), .Z(\ab[3][3] ) );
  AN2P U104 ( .A(A[3]), .B(n57), .Z(\ab[3][2] ) );
  AN2P U105 ( .A(A[3]), .B(B[1]), .Z(\ab[3][1] ) );
  AN2P U106 ( .A(A[3]), .B(n63), .Z(\ab[3][0] ) );
  AN2P U107 ( .A(A[4]), .B(n22), .Z(\SUMB[4][6] ) );
  AN2P U108 ( .A(A[4]), .B(n66), .Z(\ab[4][5] ) );
  AN2P U109 ( .A(A[4]), .B(n62), .Z(\ab[4][3] ) );
  AN2P U110 ( .A(A[4]), .B(n57), .Z(\ab[4][2] ) );
  AN2P U111 ( .A(A[4]), .B(B[1]), .Z(\ab[4][1] ) );
  AN2P U112 ( .A(A[4]), .B(n63), .Z(\ab[4][0] ) );
  AN2P U113 ( .A(A[5]), .B(n22), .Z(\SUMB[5][6] ) );
  AN2P U114 ( .A(A[5]), .B(n66), .Z(\ab[5][5] ) );
  AN2P U115 ( .A(A[5]), .B(n65), .Z(\ab[5][4] ) );
  AN2P U116 ( .A(A[5]), .B(n57), .Z(\ab[5][2] ) );
  AN2P U117 ( .A(A[5]), .B(n63), .Z(\ab[5][0] ) );
  AN2P U118 ( .A(A[6]), .B(n22), .Z(\SUMB[6][6] ) );
  AN2P U119 ( .A(A[6]), .B(n66), .Z(\ab[6][5] ) );
  AN2P U120 ( .A(A[6]), .B(n65), .Z(\ab[6][4] ) );
  AN2P U121 ( .A(A[6]), .B(n62), .Z(\ab[6][3] ) );
  AN2P U122 ( .A(A[6]), .B(n57), .Z(\ab[6][2] ) );
  AN2P U123 ( .A(A[6]), .B(B[1]), .Z(\ab[6][1] ) );
  AN2P U124 ( .A(A[6]), .B(n63), .Z(\ab[6][0] ) );
  AN2P U125 ( .A(n67), .B(n22), .Z(\SUMB[7][6] ) );
  AN2P U126 ( .A(n67), .B(n66), .Z(\ab[7][5] ) );
  AN2P U127 ( .A(n67), .B(n65), .Z(\ab[7][4] ) );
  AN2P U128 ( .A(n67), .B(n62), .Z(\ab[7][3] ) );
  AN2P U129 ( .A(n67), .B(n57), .Z(\ab[7][2] ) );
  AN2P U130 ( .A(n67), .B(B[1]), .Z(\ab[7][1] ) );
  AN2P U131 ( .A(n67), .B(n63), .Z(\ab[7][0] ) );
  AN2P U132 ( .A(n68), .B(n22), .Z(\SUMB[8][6] ) );
  AN2P U133 ( .A(n68), .B(n66), .Z(\ab[8][5] ) );
  AN2P U134 ( .A(n68), .B(n65), .Z(\ab[8][4] ) );
  AN2P U135 ( .A(n68), .B(n62), .Z(\ab[8][3] ) );
  AN2P U136 ( .A(n68), .B(n57), .Z(\ab[8][2] ) );
  AN2P U137 ( .A(n68), .B(B[1]), .Z(\ab[8][1] ) );
  AN2P U138 ( .A(n68), .B(n63), .Z(\ab[8][0] ) );
  AN2P U139 ( .A(n63), .B(A[0]), .Z(PRODUCT[0]) );
endmodule


module read_mem_DW01_inc_2 ( A, SUM );
  input [31:0] A;
  output [31:0] SUM;

  wire   [31:2] carry;

  HA1 U1_1_30 ( .A(A[30]), .B(carry[30]), .CO(carry[31]), .S(SUM[30]) );
  HA1 U1_1_1 ( .A(A[1]), .B(A[0]), .CO(carry[2]), .S(SUM[1]) );
  HA1 U1_1_2 ( .A(A[2]), .B(carry[2]), .CO(carry[3]), .S(SUM[2]) );
  HA1 U1_1_3 ( .A(A[3]), .B(carry[3]), .CO(carry[4]), .S(SUM[3]) );
  HA1 U1_1_28 ( .A(A[28]), .B(carry[28]), .CO(carry[29]), .S(SUM[28]) );
  HA1 U1_1_27 ( .A(A[27]), .B(carry[27]), .CO(carry[28]), .S(SUM[27]) );
  HA1 U1_1_26 ( .A(A[26]), .B(carry[26]), .CO(carry[27]), .S(SUM[26]) );
  HA1 U1_1_25 ( .A(A[25]), .B(carry[25]), .CO(carry[26]), .S(SUM[25]) );
  HA1 U1_1_24 ( .A(A[24]), .B(carry[24]), .CO(carry[25]), .S(SUM[24]) );
  HA1 U1_1_23 ( .A(A[23]), .B(carry[23]), .CO(carry[24]), .S(SUM[23]) );
  HA1 U1_1_22 ( .A(A[22]), .B(carry[22]), .CO(carry[23]), .S(SUM[22]) );
  HA1 U1_1_21 ( .A(A[21]), .B(carry[21]), .CO(carry[22]), .S(SUM[21]) );
  HA1 U1_1_20 ( .A(A[20]), .B(carry[20]), .CO(carry[21]), .S(SUM[20]) );
  HA1 U1_1_19 ( .A(A[19]), .B(carry[19]), .CO(carry[20]), .S(SUM[19]) );
  HA1 U1_1_18 ( .A(A[18]), .B(carry[18]), .CO(carry[19]), .S(SUM[18]) );
  HA1 U1_1_17 ( .A(A[17]), .B(carry[17]), .CO(carry[18]), .S(SUM[17]) );
  HA1 U1_1_16 ( .A(A[16]), .B(carry[16]), .CO(carry[17]), .S(SUM[16]) );
  HA1 U1_1_15 ( .A(A[15]), .B(carry[15]), .CO(carry[16]), .S(SUM[15]) );
  HA1 U1_1_14 ( .A(A[14]), .B(carry[14]), .CO(carry[15]), .S(SUM[14]) );
  HA1 U1_1_13 ( .A(A[13]), .B(carry[13]), .CO(carry[14]), .S(SUM[13]) );
  HA1 U1_1_12 ( .A(A[12]), .B(carry[12]), .CO(carry[13]), .S(SUM[12]) );
  HA1 U1_1_11 ( .A(A[11]), .B(carry[11]), .CO(carry[12]), .S(SUM[11]) );
  HA1 U1_1_10 ( .A(A[10]), .B(carry[10]), .CO(carry[11]), .S(SUM[10]) );
  HA1 U1_1_9 ( .A(A[9]), .B(carry[9]), .CO(carry[10]), .S(SUM[9]) );
  HA1 U1_1_8 ( .A(A[8]), .B(carry[8]), .CO(carry[9]), .S(SUM[8]) );
  HA1 U1_1_7 ( .A(A[7]), .B(carry[7]), .CO(carry[8]), .S(SUM[7]) );
  HA1 U1_1_6 ( .A(A[6]), .B(carry[6]), .CO(carry[7]), .S(SUM[6]) );
  HA1 U1_1_5 ( .A(A[5]), .B(carry[5]), .CO(carry[6]), .S(SUM[5]) );
  HA1 U1_1_4 ( .A(A[4]), .B(carry[4]), .CO(carry[5]), .S(SUM[4]) );
  HA1 U1_1_29 ( .A(A[29]), .B(carry[29]), .CO(carry[30]), .S(SUM[29]) );
  IVDA U1 ( .A(A[0]), .Y(SUM[0]) );
  EO U2 ( .A(carry[31]), .B(A[31]), .Z(SUM[31]) );
endmodule


module read_mem_DW01_inc_1 ( A, SUM );
  input [8:0] A;
  output [8:0] SUM;

  wire   [8:2] carry;

  HA1 U1_1_7 ( .A(A[7]), .B(carry[7]), .CO(carry[8]), .S(SUM[7]) );
  HA1 U1_1_6 ( .A(A[6]), .B(carry[6]), .CO(carry[7]), .S(SUM[6]) );
  HA1 U1_1_5 ( .A(A[5]), .B(carry[5]), .CO(carry[6]), .S(SUM[5]) );
  HA1 U1_1_4 ( .A(A[4]), .B(carry[4]), .CO(carry[5]), .S(SUM[4]) );
  HA1 U1_1_3 ( .A(A[3]), .B(carry[3]), .CO(carry[4]), .S(SUM[3]) );
  HA1 U1_1_2 ( .A(A[2]), .B(carry[2]), .CO(carry[3]), .S(SUM[2]) );
  HA1 U1_1_1 ( .A(A[1]), .B(A[0]), .CO(carry[2]), .S(SUM[1]) );
  IVP U1 ( .A(A[0]), .Z(SUM[0]) );
  EO U2 ( .A(carry[8]), .B(A[8]), .Z(SUM[8]) );
endmodule


module read_mem_DW01_inc_0 ( A, SUM );
  input [8:0] A;
  output [8:0] SUM;

  wire   [8:2] carry;

  HA1 U1_1_7 ( .A(A[7]), .B(carry[7]), .CO(carry[8]), .S(SUM[7]) );
  HA1 U1_1_6 ( .A(A[6]), .B(carry[6]), .CO(carry[7]), .S(SUM[6]) );
  HA1 U1_1_5 ( .A(A[5]), .B(carry[5]), .CO(carry[6]), .S(SUM[5]) );
  HA1 U1_1_4 ( .A(A[4]), .B(carry[4]), .CO(carry[5]), .S(SUM[4]) );
  HA1 U1_1_3 ( .A(A[3]), .B(carry[3]), .CO(carry[4]), .S(SUM[3]) );
  HA1 U1_1_2 ( .A(A[2]), .B(carry[2]), .CO(carry[3]), .S(SUM[2]) );
  HA1 U1_1_1 ( .A(A[1]), .B(A[0]), .CO(carry[2]), .S(SUM[1]) );
  IVP U1 ( .A(A[0]), .Z(SUM[0]) );
  EO U2 ( .A(carry[8]), .B(A[8]), .Z(SUM[8]) );
endmodule


module write_mem_scratch_20000_DW01_inc_0 ( A, SUM );
  input [31:0] A;
  output [31:0] SUM;

  wire   [31:2] carry;

  HA1 U1_1_30 ( .A(A[30]), .B(carry[30]), .CO(carry[31]), .S(SUM[30]) );
  HA1 U1_1_1 ( .A(A[1]), .B(A[0]), .CO(carry[2]), .S(SUM[1]) );
  HA1 U1_1_2 ( .A(A[2]), .B(carry[2]), .CO(carry[3]), .S(SUM[2]) );
  HA1 U1_1_3 ( .A(A[3]), .B(carry[3]), .CO(carry[4]), .S(SUM[3]) );
  HA1 U1_1_29 ( .A(A[29]), .B(carry[29]), .CO(carry[30]), .S(SUM[29]) );
  HA1 U1_1_28 ( .A(A[28]), .B(carry[28]), .CO(carry[29]), .S(SUM[28]) );
  HA1 U1_1_27 ( .A(A[27]), .B(carry[27]), .CO(carry[28]), .S(SUM[27]) );
  HA1 U1_1_26 ( .A(A[26]), .B(carry[26]), .CO(carry[27]), .S(SUM[26]) );
  HA1 U1_1_25 ( .A(A[25]), .B(carry[25]), .CO(carry[26]), .S(SUM[25]) );
  HA1 U1_1_24 ( .A(A[24]), .B(carry[24]), .CO(carry[25]), .S(SUM[24]) );
  HA1 U1_1_23 ( .A(A[23]), .B(carry[23]), .CO(carry[24]), .S(SUM[23]) );
  HA1 U1_1_22 ( .A(A[22]), .B(carry[22]), .CO(carry[23]), .S(SUM[22]) );
  HA1 U1_1_21 ( .A(A[21]), .B(carry[21]), .CO(carry[22]), .S(SUM[21]) );
  HA1 U1_1_20 ( .A(A[20]), .B(carry[20]), .CO(carry[21]), .S(SUM[20]) );
  HA1 U1_1_19 ( .A(A[19]), .B(carry[19]), .CO(carry[20]), .S(SUM[19]) );
  HA1 U1_1_18 ( .A(A[18]), .B(carry[18]), .CO(carry[19]), .S(SUM[18]) );
  HA1 U1_1_17 ( .A(A[17]), .B(carry[17]), .CO(carry[18]), .S(SUM[17]) );
  HA1 U1_1_16 ( .A(A[16]), .B(carry[16]), .CO(carry[17]), .S(SUM[16]) );
  HA1 U1_1_15 ( .A(A[15]), .B(carry[15]), .CO(carry[16]), .S(SUM[15]) );
  HA1 U1_1_14 ( .A(A[14]), .B(carry[14]), .CO(carry[15]), .S(SUM[14]) );
  HA1 U1_1_13 ( .A(A[13]), .B(carry[13]), .CO(carry[14]), .S(SUM[13]) );
  HA1 U1_1_12 ( .A(A[12]), .B(carry[12]), .CO(carry[13]), .S(SUM[12]) );
  HA1 U1_1_11 ( .A(A[11]), .B(carry[11]), .CO(carry[12]), .S(SUM[11]) );
  HA1 U1_1_10 ( .A(A[10]), .B(carry[10]), .CO(carry[11]), .S(SUM[10]) );
  HA1 U1_1_9 ( .A(A[9]), .B(carry[9]), .CO(carry[10]), .S(SUM[9]) );
  HA1 U1_1_8 ( .A(A[8]), .B(carry[8]), .CO(carry[9]), .S(SUM[8]) );
  HA1 U1_1_7 ( .A(A[7]), .B(carry[7]), .CO(carry[8]), .S(SUM[7]) );
  HA1 U1_1_6 ( .A(A[6]), .B(carry[6]), .CO(carry[7]), .S(SUM[6]) );
  HA1 U1_1_5 ( .A(A[5]), .B(carry[5]), .CO(carry[6]), .S(SUM[5]) );
  HA1 U1_1_4 ( .A(A[4]), .B(carry[4]), .CO(carry[5]), .S(SUM[4]) );
  IVDA U1 ( .A(A[0]), .Y(SUM[0]) );
  EO U2 ( .A(carry[31]), .B(A[31]), .Z(SUM[31]) );
endmodule


module read_mem_20000_DW01_inc_2 ( A, SUM );
  input [31:0] A;
  output [31:0] SUM;

  wire   [31:2] carry;

  HA1 U1_1_30 ( .A(A[30]), .B(carry[30]), .CO(carry[31]), .S(SUM[30]) );
  HA1 U1_1_1 ( .A(A[1]), .B(A[0]), .CO(carry[2]), .S(SUM[1]) );
  HA1 U1_1_2 ( .A(A[2]), .B(carry[2]), .CO(carry[3]), .S(SUM[2]) );
  HA1 U1_1_3 ( .A(A[3]), .B(carry[3]), .CO(carry[4]), .S(SUM[3]) );
  HA1 U1_1_29 ( .A(A[29]), .B(carry[29]), .CO(carry[30]), .S(SUM[29]) );
  HA1 U1_1_28 ( .A(A[28]), .B(carry[28]), .CO(carry[29]), .S(SUM[28]) );
  HA1 U1_1_27 ( .A(A[27]), .B(carry[27]), .CO(carry[28]), .S(SUM[27]) );
  HA1 U1_1_26 ( .A(A[26]), .B(carry[26]), .CO(carry[27]), .S(SUM[26]) );
  HA1 U1_1_25 ( .A(A[25]), .B(carry[25]), .CO(carry[26]), .S(SUM[25]) );
  HA1 U1_1_24 ( .A(A[24]), .B(carry[24]), .CO(carry[25]), .S(SUM[24]) );
  HA1 U1_1_23 ( .A(A[23]), .B(carry[23]), .CO(carry[24]), .S(SUM[23]) );
  HA1 U1_1_22 ( .A(A[22]), .B(carry[22]), .CO(carry[23]), .S(SUM[22]) );
  HA1 U1_1_21 ( .A(A[21]), .B(carry[21]), .CO(carry[22]), .S(SUM[21]) );
  HA1 U1_1_20 ( .A(A[20]), .B(carry[20]), .CO(carry[21]), .S(SUM[20]) );
  HA1 U1_1_19 ( .A(A[19]), .B(carry[19]), .CO(carry[20]), .S(SUM[19]) );
  HA1 U1_1_18 ( .A(A[18]), .B(carry[18]), .CO(carry[19]), .S(SUM[18]) );
  HA1 U1_1_17 ( .A(A[17]), .B(carry[17]), .CO(carry[18]), .S(SUM[17]) );
  HA1 U1_1_16 ( .A(A[16]), .B(carry[16]), .CO(carry[17]), .S(SUM[16]) );
  HA1 U1_1_15 ( .A(A[15]), .B(carry[15]), .CO(carry[16]), .S(SUM[15]) );
  HA1 U1_1_14 ( .A(A[14]), .B(carry[14]), .CO(carry[15]), .S(SUM[14]) );
  HA1 U1_1_13 ( .A(A[13]), .B(carry[13]), .CO(carry[14]), .S(SUM[13]) );
  HA1 U1_1_12 ( .A(A[12]), .B(carry[12]), .CO(carry[13]), .S(SUM[12]) );
  HA1 U1_1_11 ( .A(A[11]), .B(carry[11]), .CO(carry[12]), .S(SUM[11]) );
  HA1 U1_1_10 ( .A(A[10]), .B(carry[10]), .CO(carry[11]), .S(SUM[10]) );
  HA1 U1_1_9 ( .A(A[9]), .B(carry[9]), .CO(carry[10]), .S(SUM[9]) );
  HA1 U1_1_8 ( .A(A[8]), .B(carry[8]), .CO(carry[9]), .S(SUM[8]) );
  HA1 U1_1_7 ( .A(A[7]), .B(carry[7]), .CO(carry[8]), .S(SUM[7]) );
  HA1 U1_1_6 ( .A(A[6]), .B(carry[6]), .CO(carry[7]), .S(SUM[6]) );
  HA1 U1_1_5 ( .A(A[5]), .B(carry[5]), .CO(carry[6]), .S(SUM[5]) );
  HA1 U1_1_4 ( .A(A[4]), .B(carry[4]), .CO(carry[5]), .S(SUM[4]) );
  IVDA U1 ( .A(A[0]), .Y(SUM[0]) );
  EO U2 ( .A(carry[31]), .B(A[31]), .Z(SUM[31]) );
endmodule


module read_mem_20000_DW01_inc_1 ( A, SUM );
  input [8:0] A;
  output [8:0] SUM;

  wire   [8:2] carry;

  HA1 U1_1_7 ( .A(A[7]), .B(carry[7]), .CO(carry[8]), .S(SUM[7]) );
  HA1 U1_1_6 ( .A(A[6]), .B(carry[6]), .CO(carry[7]), .S(SUM[6]) );
  HA1 U1_1_5 ( .A(A[5]), .B(carry[5]), .CO(carry[6]), .S(SUM[5]) );
  HA1 U1_1_4 ( .A(A[4]), .B(carry[4]), .CO(carry[5]), .S(SUM[4]) );
  HA1 U1_1_3 ( .A(A[3]), .B(carry[3]), .CO(carry[4]), .S(SUM[3]) );
  HA1 U1_1_2 ( .A(A[2]), .B(carry[2]), .CO(carry[3]), .S(SUM[2]) );
  HA1 U1_1_1 ( .A(A[1]), .B(A[0]), .CO(carry[2]), .S(SUM[1]) );
  IVP U1 ( .A(A[0]), .Z(SUM[0]) );
  EO U2 ( .A(carry[8]), .B(A[8]), .Z(SUM[8]) );
endmodule


module read_mem_20000_DW01_inc_0 ( A, SUM );
  input [8:0] A;
  output [8:0] SUM;

  wire   [8:2] carry;

  HA1 U1_1_7 ( .A(A[7]), .B(carry[7]), .CO(carry[8]), .S(SUM[7]) );
  HA1 U1_1_6 ( .A(A[6]), .B(carry[6]), .CO(carry[7]), .S(SUM[6]) );
  HA1 U1_1_5 ( .A(A[5]), .B(carry[5]), .CO(carry[6]), .S(SUM[5]) );
  HA1 U1_1_4 ( .A(A[4]), .B(carry[4]), .CO(carry[5]), .S(SUM[4]) );
  HA1 U1_1_3 ( .A(A[3]), .B(carry[3]), .CO(carry[4]), .S(SUM[3]) );
  HA1 U1_1_2 ( .A(A[2]), .B(carry[2]), .CO(carry[3]), .S(SUM[2]) );
  HA1 U1_1_1 ( .A(A[1]), .B(A[0]), .CO(carry[2]), .S(SUM[1]) );
  IVP U1 ( .A(A[0]), .Z(SUM[0]) );
  EO U2 ( .A(carry[8]), .B(A[8]), .Z(SUM[8]) );
endmodule


module compute_xy_center_DW01_inc_0 ( A, SUM );
  input [31:0] A;
  output [31:0] SUM;

  wire   [31:2] carry;

  HA1 U1_1_30 ( .A(A[30]), .B(carry[30]), .CO(carry[31]), .S(SUM[30]) );
  HA1 U1_1_1 ( .A(A[1]), .B(A[0]), .CO(carry[2]), .S(SUM[1]) );
  HA1 U1_1_2 ( .A(A[2]), .B(carry[2]), .CO(carry[3]), .S(SUM[2]) );
  HA1 U1_1_3 ( .A(A[3]), .B(carry[3]), .CO(carry[4]), .S(SUM[3]) );
  HA1 U1_1_29 ( .A(A[29]), .B(carry[29]), .CO(carry[30]), .S(SUM[29]) );
  HA1 U1_1_28 ( .A(A[28]), .B(carry[28]), .CO(carry[29]), .S(SUM[28]) );
  HA1 U1_1_27 ( .A(A[27]), .B(carry[27]), .CO(carry[28]), .S(SUM[27]) );
  HA1 U1_1_26 ( .A(A[26]), .B(carry[26]), .CO(carry[27]), .S(SUM[26]) );
  HA1 U1_1_25 ( .A(A[25]), .B(carry[25]), .CO(carry[26]), .S(SUM[25]) );
  HA1 U1_1_24 ( .A(A[24]), .B(carry[24]), .CO(carry[25]), .S(SUM[24]) );
  HA1 U1_1_23 ( .A(A[23]), .B(carry[23]), .CO(carry[24]), .S(SUM[23]) );
  HA1 U1_1_22 ( .A(A[22]), .B(carry[22]), .CO(carry[23]), .S(SUM[22]) );
  HA1 U1_1_21 ( .A(A[21]), .B(carry[21]), .CO(carry[22]), .S(SUM[21]) );
  HA1 U1_1_20 ( .A(A[20]), .B(carry[20]), .CO(carry[21]), .S(SUM[20]) );
  HA1 U1_1_19 ( .A(A[19]), .B(carry[19]), .CO(carry[20]), .S(SUM[19]) );
  HA1 U1_1_18 ( .A(A[18]), .B(carry[18]), .CO(carry[19]), .S(SUM[18]) );
  HA1 U1_1_17 ( .A(A[17]), .B(carry[17]), .CO(carry[18]), .S(SUM[17]) );
  HA1 U1_1_16 ( .A(A[16]), .B(carry[16]), .CO(carry[17]), .S(SUM[16]) );
  HA1 U1_1_15 ( .A(A[15]), .B(carry[15]), .CO(carry[16]), .S(SUM[15]) );
  HA1 U1_1_14 ( .A(A[14]), .B(carry[14]), .CO(carry[15]), .S(SUM[14]) );
  HA1 U1_1_13 ( .A(A[13]), .B(carry[13]), .CO(carry[14]), .S(SUM[13]) );
  HA1 U1_1_12 ( .A(A[12]), .B(carry[12]), .CO(carry[13]), .S(SUM[12]) );
  HA1 U1_1_11 ( .A(A[11]), .B(carry[11]), .CO(carry[12]), .S(SUM[11]) );
  HA1 U1_1_10 ( .A(A[10]), .B(carry[10]), .CO(carry[11]), .S(SUM[10]) );
  HA1 U1_1_9 ( .A(A[9]), .B(carry[9]), .CO(carry[10]), .S(SUM[9]) );
  HA1 U1_1_8 ( .A(A[8]), .B(carry[8]), .CO(carry[9]), .S(SUM[8]) );
  HA1 U1_1_7 ( .A(A[7]), .B(carry[7]), .CO(carry[8]), .S(SUM[7]) );
  HA1 U1_1_6 ( .A(A[6]), .B(carry[6]), .CO(carry[7]), .S(SUM[6]) );
  HA1 U1_1_5 ( .A(A[5]), .B(carry[5]), .CO(carry[6]), .S(SUM[5]) );
  HA1 U1_1_4 ( .A(A[4]), .B(carry[4]), .CO(carry[5]), .S(SUM[4]) );
  IVDA U1 ( .A(A[0]), .Y(SUM[0]) );
  EO U2 ( .A(carry[31]), .B(A[31]), .Z(SUM[31]) );
endmodule


module ycompute ( angle, offset );
  input [7:0] angle;
  output [15:0] offset;
  wire   n11, n12, n13, n14, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25,
         n26, n27, n1, n2, n3, n4, n5, n6, n7, n8, n9, n10;
  assign offset[0] = offset[15];
  assign offset[7] = offset[15];
  assign offset[8] = offset[15];
  assign offset[9] = offset[15];
  assign offset[10] = offset[15];
  assign offset[11] = offset[15];
  assign offset[12] = offset[15];
  assign offset[13] = offset[15];
  assign offset[14] = offset[15];

  OR3 U34 ( .A(angle[3]), .B(n2), .C(n10), .Z(n18) );
  OR2P U2 ( .A(angle[7]), .B(angle[0]), .Z(n1) );
  IVP U3 ( .A(offset[5]), .Z(n4) );
  IVP U4 ( .A(offset[3]), .Z(n5) );
  AO7 U5 ( .A(n6), .B(n1), .C(n5), .Z(offset[5]) );
  NR2 U6 ( .A(n14), .B(n1), .Z(offset[3]) );
  AO7 U7 ( .A(n12), .B(n1), .C(n4), .Z(offset[1]) );
  AO7 U8 ( .A(n1), .B(n13), .C(n4), .Z(offset[2]) );
  AN3 U9 ( .A(n13), .B(n6), .C(n12), .Z(n11) );
  AO6 U10 ( .A(n14), .B(n11), .C(n1), .Z(offset[15]) );
  IVP U11 ( .A(n22), .Z(n8) );
  AO7 U12 ( .A(n12), .B(n1), .C(n5), .Z(offset[4]) );
  NR2 U13 ( .A(n11), .B(n1), .Z(offset[6]) );
  AO7 U14 ( .A(angle[1]), .B(n21), .C(n26), .Z(n25) );
  ND4 U15 ( .A(n2), .B(angle[1]), .C(n27), .D(n10), .Z(n26) );
  ND4 U16 ( .A(angle[3]), .B(n10), .C(n3), .D(n7), .Z(n21) );
  IVP U17 ( .A(angle[2]), .Z(n10) );
  EO1 U18 ( .A(n16), .B(n17), .C(n18), .D(n19), .Z(n12) );
  AN3 U19 ( .A(n2), .B(n7), .C(angle[2]), .Z(n16) );
  IVP U20 ( .A(n20), .Z(n6) );
  AO7 U21 ( .A(n21), .B(n22), .C(n23), .Z(n20) );
  AO3 U22 ( .A(n24), .B(n8), .C(angle[3]), .D(n16), .Z(n13) );
  NR2 U23 ( .A(angle[1]), .B(n9), .Z(n24) );
  ND2 U24 ( .A(angle[1]), .B(n9), .Z(n22) );
  ND2 U25 ( .A(angle[5]), .B(n25), .Z(n14) );
  IVP U26 ( .A(angle[5]), .Z(n9) );
  NR3 U27 ( .A(angle[5]), .B(angle[3]), .C(angle[1]), .Z(n17) );
  ND2 U28 ( .A(angle[6]), .B(n8), .Z(n19) );
  ND4 U29 ( .A(n17), .B(angle[6]), .C(n2), .D(n10), .Z(n23) );
  NR2 U30 ( .A(angle[6]), .B(angle[3]), .Z(n27) );
  IVP U31 ( .A(angle[6]), .Z(n7) );
  IVA U32 ( .A(n3), .Z(n2) );
  IVA U33 ( .A(angle[4]), .Z(n3) );
endmodule


module xcompute ( angle, offset );
  input [7:0] angle;
  output [15:0] offset;
  wire   n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24,
         n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n1, n2, n3, n4,
         n5, n8, n9, n10, n36;
  assign offset[15] = 1'b0;
  assign offset[14] = 1'b0;
  assign offset[13] = 1'b0;
  assign offset[12] = 1'b0;
  assign offset[11] = 1'b0;
  assign offset[10] = 1'b0;
  assign offset[9] = 1'b0;

  AO1P U2 ( .A(n11), .B(n12), .C(n13), .D(n8), .Z(offset[8]) );
  AO1P U24 ( .A(n34), .B(n14), .C(n24), .D(n18), .Z(n35) );
  OR3 U41 ( .A(angle[7]), .B(angle[5]), .C(angle[0]), .Z(n13) );
  OR3 U42 ( .A(n1), .B(angle[2]), .C(n32), .Z(n28) );
  AO7 U3 ( .A(n5), .B(n25), .C(n27), .Z(offset[1]) );
  NR2 U4 ( .A(n5), .B(n35), .Z(offset[0]) );
  ND2 U5 ( .A(n34), .B(n15), .Z(n25) );
  NR3 U6 ( .A(n10), .B(n3), .C(n32), .Z(n18) );
  AO7 U7 ( .A(n30), .B(n18), .C(n19), .Z(n27) );
  AN3 U8 ( .A(n3), .B(n10), .C(n33), .Z(n30) );
  NR2 U9 ( .A(n29), .B(n5), .Z(offset[2]) );
  NR3 U10 ( .A(n17), .B(n24), .C(n30), .Z(n29) );
  IVP U11 ( .A(n19), .Z(n5) );
  ND2 U12 ( .A(n28), .B(n26), .Z(n17) );
  AO7 U13 ( .A(n5), .B(n28), .C(n27), .Z(offset[3]) );
  NR2 U14 ( .A(n9), .B(n36), .Z(n14) );
  AO7 U15 ( .A(n5), .B(n26), .C(n27), .Z(offset[4]) );
  IVP U16 ( .A(n22), .Z(offset[5]) );
  AO7 U17 ( .A(n23), .B(n24), .C(n19), .Z(n22) );
  ND2 U18 ( .A(n25), .B(n26), .Z(n23) );
  IVP U19 ( .A(n16), .Z(offset[7]) );
  AO7 U20 ( .A(n17), .B(n18), .C(n19), .Z(n16) );
  IVP U21 ( .A(n2), .Z(n1) );
  AN3 U22 ( .A(angle[2]), .B(n1), .C(n33), .Z(n24) );
  IVP U23 ( .A(angle[3]), .Z(n9) );
  IVP U25 ( .A(angle[1]), .Z(n36) );
  NR2 U26 ( .A(angle[0]), .B(angle[7]), .Z(n19) );
  ND4 U27 ( .A(n9), .B(n8), .C(n10), .D(n31), .Z(n26) );
  IVP U28 ( .A(angle[2]), .Z(n10) );
  NR2 U29 ( .A(angle[1]), .B(angle[3]), .Z(n15) );
  ND2 U30 ( .A(n4), .B(n20), .Z(offset[6]) );
  ND4 U31 ( .A(n19), .B(n1), .C(n15), .D(n21), .Z(n20) );
  IVP U32 ( .A(offset[0]), .Z(n4) );
  AO3 U33 ( .A(n14), .B(n15), .C(n10), .D(n1), .Z(n11) );
  ND4 U34 ( .A(angle[2]), .B(angle[1]), .C(n9), .D(n3), .Z(n12) );
  AN3 U35 ( .A(n1), .B(angle[1]), .C(angle[5]), .Z(n31) );
  NR4 U36 ( .A(n8), .B(n3), .C(angle[2]), .D(angle[5]), .Z(n34) );
  ND4 U37 ( .A(angle[5]), .B(angle[3]), .C(n36), .D(n8), .Z(n32) );
  NR3 U38 ( .A(angle[5]), .B(angle[6]), .C(n10), .Z(n21) );
  NR4 U39 ( .A(n9), .B(n36), .C(angle[5]), .D(angle[6]), .Z(n33) );
  IVP U40 ( .A(angle[6]), .Z(n8) );
  IVA U43 ( .A(angle[4]), .Z(n2) );
  IVA U44 ( .A(n1), .Z(n3) );
endmodule


module cosine ( angle, cos_val );
  input [7:0] angle;
  output [15:0] cos_val;
  wire   n1, n2, n3, n4, n5, n6, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n28, n29, n30, n31,
         n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45,
         n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59,
         n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73,
         n74, n75, n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87,
         n88, n89, n90, n91, n92, n93, n94, n95, n96, n97;
  assign cos_val[15] = 1'b0;
  assign cos_val[14] = 1'b0;
  assign cos_val[13] = 1'b0;
  assign cos_val[12] = 1'b0;
  assign cos_val[11] = 1'b0;
  assign cos_val[10] = 1'b0;
  assign cos_val[9] = 1'b0;
  assign cos_val[8] = 1'b0;
  assign cos_val[7] = 1'b0;

  B4IP U3 ( .A(n51), .Z(cos_val[1]) );
  ND3 U4 ( .A(angle[5]), .B(n30), .C(n32), .Z(n79) );
  IVAP U5 ( .A(angle[1]), .Z(n8) );
  IVAP U6 ( .A(angle[1]), .Z(n17) );
  IVP U7 ( .A(n90), .Z(n74) );
  IVAP U8 ( .A(angle[5]), .Z(n9) );
  NR2P U9 ( .A(n1), .B(n2), .Z(n56) );
  AN2 U10 ( .A(n64), .B(n63), .Z(n1) );
  AN2 U11 ( .A(n48), .B(n46), .Z(n2) );
  NR2 U12 ( .A(n89), .B(n88), .Z(n3) );
  IVAP U13 ( .A(angle[3]), .Z(n4) );
  OR2P U14 ( .A(n73), .B(n25), .Z(n52) );
  ND3P U15 ( .A(angle[1]), .B(n13), .C(n12), .Z(n25) );
  NR2 U16 ( .A(n62), .B(n61), .Z(n70) );
  ND6P U17 ( .A(angle[2]), .B(n14), .C(angle[4]), .D(n18), .E(n9), .F(n8), .Z(
        n5) );
  IVDA U18 ( .A(n93), .Z(n6) );
  ND8 U19 ( .A(n30), .B(n32), .C(n16), .D(n53), .E(n68), .F(n67), .G(angle[5]), 
        .H(angle[3]), .Z(n57) );
  B4IP U20 ( .A(angle[0]), .Z(n68) );
  B4IP U21 ( .A(angle[7]), .Z(n67) );
  AN3 U22 ( .A(n41), .B(n55), .C(n85), .Z(n78) );
  ND6P U23 ( .A(angle[4]), .B(angle[6]), .C(n33), .D(n8), .E(n9), .F(n76), .Z(
        n10) );
  IVAP U24 ( .A(n10), .Z(n61) );
  IVAP U25 ( .A(angle[2]), .Z(n33) );
  NR3P U26 ( .A(n22), .B(n40), .C(n24), .Z(n72) );
  B4IP U27 ( .A(angle[6]), .Z(n22) );
  ND6P U28 ( .A(n37), .B(n82), .C(angle[3]), .D(angle[4]), .E(angle[2]), .F(
        angle[1]), .Z(n84) );
  ND6P U29 ( .A(angle[2]), .B(n82), .C(angle[4]), .D(n4), .E(n36), .F(n83), 
        .Z(n11) );
  ND6 U30 ( .A(angle[2]), .B(n14), .C(angle[4]), .D(n18), .E(n36), .F(n83), 
        .Z(n85) );
  NR3 U31 ( .A(angle[2]), .B(angle[6]), .C(angle[3]), .Z(n35) );
  B4I U32 ( .A(angle[6]), .Z(n54) );
  IVAP U33 ( .A(angle[6]), .Z(n53) );
  ND4P U34 ( .A(n45), .B(n95), .C(n94), .D(n6), .Z(n97) );
  IVAP U35 ( .A(angle[6]), .Z(n12) );
  IVAP U36 ( .A(angle[5]), .Z(n13) );
  IVAP U37 ( .A(angle[6]), .Z(n14) );
  OR2 U38 ( .A(n79), .B(n80), .Z(n15) );
  IVAP U39 ( .A(angle[1]), .Z(n16) );
  IVAP U40 ( .A(angle[3]), .Z(n18) );
  IVAP U41 ( .A(angle[5]), .Z(n36) );
  B4IP U42 ( .A(angle[1]), .Z(n19) );
  IVAP U43 ( .A(angle[3]), .Z(n76) );
  NR3P U44 ( .A(angle[5]), .B(angle[4]), .C(angle[6]), .Z(n47) );
  NR3P U45 ( .A(n20), .B(n19), .C(angle[2]), .Z(n49) );
  B4IP U46 ( .A(angle[3]), .Z(n20) );
  B4IP U47 ( .A(angle[1]), .Z(n21) );
  B4IP U48 ( .A(angle[1]), .Z(n23) );
  B4IP U49 ( .A(angle[2]), .Z(n24) );
  AN3 U50 ( .A(n91), .B(n44), .C(n50), .Z(n26) );
  ND3 U51 ( .A(angle[4]), .B(angle[5]), .C(angle[2]), .Z(n86) );
  B2I U52 ( .A(n96), .Z2(n45) );
  ND3P U53 ( .A(angle[3]), .B(n16), .C(n53), .Z(n80) );
  AN3 U54 ( .A(angle[3]), .B(angle[1]), .C(n92), .Z(n28) );
  B4IP U55 ( .A(angle[4]), .Z(n29) );
  IVAP U56 ( .A(angle[4]), .Z(n30) );
  B4IP U57 ( .A(angle[5]), .Z(n31) );
  B4IP U58 ( .A(angle[6]), .Z(n50) );
  IVAP U59 ( .A(angle[6]), .Z(n82) );
  IVAP U60 ( .A(angle[2]), .Z(n32) );
  ND2P U61 ( .A(n49), .B(n47), .Z(n34) );
  ND2 U62 ( .A(n28), .B(n26), .Z(n55) );
  ND3P U63 ( .A(angle[3]), .B(angle[4]), .C(angle[2]), .Z(n73) );
  IVAP U64 ( .A(angle[5]), .Z(n37) );
  NR3 U65 ( .A(n42), .B(n21), .C(n29), .Z(n38) );
  IVAP U66 ( .A(angle[1]), .Z(n83) );
  NR3P U67 ( .A(angle[5]), .B(angle[4]), .C(angle[6]), .Z(n46) );
  NR3P U68 ( .A(n39), .B(n43), .C(angle[2]), .Z(n48) );
  B4IP U69 ( .A(angle[3]), .Z(n39) );
  B4IP U70 ( .A(angle[1]), .Z(n40) );
  ND2 U71 ( .A(n38), .B(n35), .Z(n41) );
  NR3P U72 ( .A(n31), .B(n23), .C(n58), .Z(n64) );
  B4IP U73 ( .A(angle[5]), .Z(n42) );
  B4IP U74 ( .A(angle[1]), .Z(n43) );
  B4IP U75 ( .A(angle[4]), .Z(n44) );
  NR3P U76 ( .A(angle[3]), .B(angle[4]), .C(angle[5]), .Z(n71) );
  IV U77 ( .A(n81), .Z(cos_val[2]) );
  NR2P U78 ( .A(angle[1]), .B(angle[5]), .Z(n59) );
  ND2P U79 ( .A(n75), .B(n74), .Z(n51) );
  AN2 U80 ( .A(n26), .B(n28), .Z(n66) );
  AO4 U81 ( .A(n25), .B(n73), .C(n80), .D(n79), .Z(n62) );
  NR2P U82 ( .A(n66), .B(n65), .Z(n69) );
  ND6 U83 ( .A(angle[6]), .B(angle[4]), .C(n32), .D(angle[1]), .E(n9), .F(
        angle[3]), .Z(n95) );
  AO3P U84 ( .A(n87), .B(n86), .C(n5), .D(n52), .Z(n88) );
  ND3 U85 ( .A(n17), .B(n54), .C(angle[3]), .Z(n87) );
  AO6P U86 ( .A(n45), .B(n78), .C(n90), .Z(cos_val[3]) );
  ND4P U87 ( .A(n34), .B(n84), .C(n93), .D(n11), .Z(n77) );
  IV U88 ( .A(n41), .Z(n65) );
  B4IP U89 ( .A(angle[2]), .Z(n92) );
  B4IP U90 ( .A(angle[5]), .Z(n91) );
  ND2P U91 ( .A(n57), .B(n81), .Z(cos_val[4]) );
  ND2P U92 ( .A(n15), .B(n56), .Z(n89) );
  ND4 U93 ( .A(n93), .B(n96), .C(n52), .D(n34), .Z(n75) );
  AN2 U94 ( .A(angle[4]), .B(angle[6]), .Z(n60) );
  B4IP U95 ( .A(angle[4]), .Z(n58) );
  ND4P U96 ( .A(n60), .B(n33), .C(n59), .D(n76), .Z(n96) );
  NR3P U97 ( .A(angle[2]), .B(angle[6]), .C(angle[3]), .Z(n63) );
  ND2P U98 ( .A(n68), .B(n67), .Z(n90) );
  AO6P U99 ( .A(n70), .B(n69), .C(n90), .Z(cos_val[0]) );
  ND2P U100 ( .A(n72), .B(n71), .Z(n93) );
  ND2P U101 ( .A(n74), .B(n77), .Z(n81) );
  NR2P U102 ( .A(n89), .B(n88), .Z(n94) );
  NR2P U103 ( .A(n3), .B(n90), .Z(cos_val[5]) );
  ND2P U104 ( .A(n74), .B(n97), .Z(cos_val[6]) );
endmodule


module sine ( angle, sin_val );
  input [7:0] angle;
  output [15:0] sin_val;
  wire   n1, n2, n3, n4, n5, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44,
         n45, n46, n47, n48, n49;
  assign sin_val[15] = 1'b0;
  assign sin_val[14] = 1'b0;
  assign sin_val[13] = 1'b0;
  assign sin_val[12] = 1'b0;
  assign sin_val[11] = 1'b0;
  assign sin_val[10] = 1'b0;
  assign sin_val[9] = 1'b0;
  assign sin_val[8] = 1'b0;
  assign sin_val[7] = 1'b0;

  IVP U2 ( .A(n46), .Z(n47) );
  AN3 U3 ( .A(angle[3]), .B(angle[1]), .C(n39), .Z(n1) );
  ND6 U4 ( .A(angle[4]), .B(n22), .C(angle[6]), .D(n39), .E(n49), .F(n28), .Z(
        n11) );
  ND4P U5 ( .A(angle[5]), .B(n22), .C(angle[3]), .D(n14), .Z(n31) );
  ND6 U6 ( .A(n39), .B(angle[4]), .C(n22), .D(n28), .E(angle[2]), .F(n40), .Z(
        n29) );
  B4IP U7 ( .A(angle[1]), .Z(n22) );
  NR8P U8 ( .A(n2), .B(n3), .C(n4), .D(n5), .E(angle[2]), .F(angle[0]), .G(
        angle[7]), .H(angle[5]), .Z(sin_val[6]) );
  B4IP U9 ( .A(angle[6]), .Z(n2) );
  B4IP U10 ( .A(angle[4]), .Z(n3) );
  B4IP U11 ( .A(angle[3]), .Z(n4) );
  B4IP U12 ( .A(angle[1]), .Z(n5) );
  NR2P U13 ( .A(n41), .B(n12), .Z(n42) );
  OR2 U14 ( .A(n20), .B(n21), .Z(n44) );
  AN2P U15 ( .A(n48), .B(n47), .Z(sin_val[5]) );
  OR2P U16 ( .A(n20), .B(n21), .Z(n7) );
  ND6P U17 ( .A(angle[4]), .B(n22), .C(angle[6]), .D(n39), .E(n49), .F(n28), 
        .Z(n8) );
  B4IP U18 ( .A(angle[3]), .Z(n28) );
  ND4P U19 ( .A(n45), .B(n7), .C(n43), .D(n42), .Z(n48) );
  ND2P U20 ( .A(n9), .B(n10), .Z(n46) );
  B4IP U21 ( .A(angle[0]), .Z(n9) );
  B4IP U22 ( .A(angle[7]), .Z(n10) );
  AO6 U23 ( .A(n19), .B(n18), .C(n46), .Z(sin_val[0]) );
  ND4P U24 ( .A(n29), .B(n44), .C(n11), .D(n43), .Z(n30) );
  NR3P U25 ( .A(angle[2]), .B(angle[6]), .C(angle[4]), .Z(n14) );
  B4IP U26 ( .A(angle[6]), .Z(n40) );
  ND3P U27 ( .A(angle[1]), .B(n13), .C(n39), .Z(n24) );
  B4IP U28 ( .A(angle[5]), .Z(n39) );
  B4IP U29 ( .A(angle[2]), .Z(n49) );
  IVA U30 ( .A(n38), .Z(sin_val[2]) );
  NR2P U31 ( .A(n35), .B(n36), .Z(n12) );
  ND3 U32 ( .A(n40), .B(n49), .C(n28), .Z(n36) );
  NR2 U33 ( .A(n24), .B(n23), .Z(n16) );
  NR2 U34 ( .A(n21), .B(n20), .Z(n15) );
  ND4 U35 ( .A(angle[2]), .B(n40), .C(n1), .D(angle[4]), .Z(n45) );
  NR2 U36 ( .A(n36), .B(n35), .Z(n17) );
  IV U37 ( .A(n32), .Z(n33) );
  ND4 U38 ( .A(n7), .B(n11), .C(n29), .D(n32), .Z(n27) );
  B4IP U39 ( .A(angle[4]), .Z(n13) );
  AN2P U40 ( .A(n31), .B(n8), .Z(n19) );
  ND3P U41 ( .A(angle[4]), .B(angle[1]), .C(angle[5]), .Z(n35) );
  ND3P U42 ( .A(n40), .B(n49), .C(angle[3]), .Z(n23) );
  ND3P U43 ( .A(n22), .B(n40), .C(angle[2]), .Z(n21) );
  ND3P U44 ( .A(angle[4]), .B(angle[5]), .C(angle[3]), .Z(n20) );
  NR3P U45 ( .A(n17), .B(n16), .C(n15), .Z(n18) );
  IVA U46 ( .A(n23), .Z(n26) );
  IVA U47 ( .A(n24), .Z(n25) );
  ND2P U48 ( .A(n26), .B(n25), .Z(n32) );
  AN2P U49 ( .A(n27), .B(n47), .Z(sin_val[1]) );
  ND6P U50 ( .A(angle[2]), .B(angle[1]), .C(n13), .D(n39), .E(angle[6]), .F(
        n28), .Z(n43) );
  ND2P U51 ( .A(n47), .B(n30), .Z(n38) );
  ND2P U52 ( .A(n8), .B(n31), .Z(n41) );
  NR2P U53 ( .A(n41), .B(n33), .Z(n34) );
  AO6P U54 ( .A(n43), .B(n34), .C(n46), .Z(sin_val[3]) );
  ND2 U55 ( .A(n47), .B(n12), .Z(n37) );
  ND2P U56 ( .A(n38), .B(n37), .Z(sin_val[4]) );
endmodule


module write_mem ( clk, data, valid, WRITE, addr, datain );
  input [63:0] data;
  output [31:0] addr;
  output [31:0] datain;
  input clk, valid;
  output WRITE;
  wire   N3, N9, N16, N46, N47, N48, N49, N50, N51, N52, N53, N54, N55, N56,
         N57, N58, N59, N60, N61, N62, N63, N64, N44, N43, N42, N41, N40, N39,
         N38, N37, N36, N35, N34, \add_111/carry[17] , \add_111/carry[16] ,
         \add_111/carry[15] , \add_111/carry[14] , \add_111/carry[13] ,
         \add_111/carry[12] , \add_111/carry[11] , \add_111/carry[10] ,
         \add_111/carry[9] , \add_111/carry[8] , \add_111/carry[7] ,
         \mult_111/A2[14] , \mult_111/CARRYB[2][6] , \mult_111/CARRYB[3][6] ,
         \mult_111/CARRYB[4][6] , \mult_111/CARRYB[5][6] ,
         \mult_111/CARRYB[6][6] , \mult_111/CARRYB[7][6] ,
         \mult_111/CARRYB[8][6] , n1, n74, n75, n76;
  wire   [8:0] x;
  wire   [8:0] y;
  assign addr[31] = 1'b0;
  assign addr[30] = 1'b0;
  assign addr[29] = 1'b0;
  assign addr[28] = 1'b0;
  assign addr[27] = 1'b0;
  assign addr[26] = 1'b0;
  assign addr[25] = 1'b0;
  assign addr[24] = 1'b0;
  assign addr[23] = 1'b0;
  assign addr[22] = 1'b0;
  assign addr[21] = 1'b0;
  assign addr[20] = 1'b0;
  assign addr[19] = 1'b0;

  FDS2 \y_reg[8]  ( .CR(data[40]), .D(n75), .CP(clk), .Q(y[8]) );
  FDS2 \x_reg[8]  ( .CR(data[49]), .D(n74), .CP(clk), .Q(x[8]) );
  FDS2 \y_reg[7]  ( .CR(data[39]), .D(n75), .CP(clk), .Q(y[7]) );
  FDS2 \x_reg[7]  ( .CR(data[48]), .D(n74), .CP(clk), .Q(x[7]) );
  FDS2 \x_reg[6]  ( .CR(data[47]), .D(n74), .CP(clk), .Q(x[6]) );
  FDS2 \y_reg[6]  ( .CR(data[38]), .D(n75), .CP(clk), .Q(y[6]) );
  FDS2 \y_reg[5]  ( .CR(data[37]), .D(n75), .CP(clk), .Q(y[5]) );
  FDS2 \y_reg[4]  ( .CR(data[36]), .D(n75), .CP(clk), .Q(y[4]) );
  FDS2 \y_reg[1]  ( .CR(data[33]), .D(n75), .CP(clk), .Q(N35) );
  FDS2 \y_reg[3]  ( .CR(data[35]), .D(n75), .CP(clk), .Q(y[3]) );
  FDS2 \y_reg[2]  ( .CR(data[34]), .D(n75), .CP(clk), .Q(y[2]) );
  FDS2 \y_reg[0]  ( .CR(data[32]), .D(n75), .CP(clk), .Q(N34) );
  FA1A \add_111/U1_7  ( .A(x[7]), .B(N35), .CI(\add_111/carry[7] ), .CO(
        \add_111/carry[8] ), .S(N53) );
  FA1A \add_111/U1_8  ( .A(x[8]), .B(N36), .CI(\add_111/carry[8] ), .CO(
        \add_111/carry[9] ), .S(N54) );
  FA1A \mult_111/S2_7_6  ( .A(y[7]), .B(\mult_111/CARRYB[6][6] ), .CI(y[5]), 
        .CO(\mult_111/CARRYB[7][6] ), .S(N41) );
  FA1A \mult_111/S2_6_6  ( .A(y[6]), .B(\mult_111/CARRYB[5][6] ), .CI(y[4]), 
        .CO(\mult_111/CARRYB[6][6] ), .S(N40) );
  FA1A \mult_111/S2_5_6  ( .A(y[5]), .B(\mult_111/CARRYB[4][6] ), .CI(y[3]), 
        .CO(\mult_111/CARRYB[5][6] ), .S(N39) );
  FA1A \mult_111/S2_4_6  ( .A(y[4]), .B(\mult_111/CARRYB[3][6] ), .CI(y[2]), 
        .CO(\mult_111/CARRYB[4][6] ), .S(N38) );
  FA1A \mult_111/S2_3_6  ( .A(y[3]), .B(\mult_111/CARRYB[2][6] ), .CI(N35), 
        .CO(\mult_111/CARRYB[3][6] ), .S(N37) );
  FA1A \mult_111/S4_6  ( .A(y[8]), .B(\mult_111/CARRYB[7][6] ), .CI(y[6]), 
        .CO(\mult_111/CARRYB[8][6] ), .S(N42) );
  FD1 \addr_reg[8]  ( .D(N54), .CP(clk), .Q(addr[8]) );
  FD1 \addr_reg[7]  ( .D(N53), .CP(clk), .Q(addr[7]) );
  FD1 \addr_reg[6]  ( .D(N52), .CP(clk), .Q(addr[6]) );
  FDS2 \x_reg[5]  ( .CR(data[46]), .D(n74), .CP(clk), .Q(N51) );
  FDS2 \x_reg[4]  ( .CR(data[45]), .D(n74), .CP(clk), .Q(N50) );
  FD1 \x_reg[3]  ( .D(N9), .CP(clk), .Q(N49) );
  FDS2 \x_reg[2]  ( .CR(data[43]), .D(n74), .CP(clk), .Q(N48) );
  FDS2 \x_reg[1]  ( .CR(data[42]), .D(n74), .CP(clk), .Q(N47) );
  FDS2 \x_reg[0]  ( .CR(data[41]), .D(n74), .CP(clk), .Q(N46) );
  FD1 \addr_reg[18]  ( .D(N64), .CP(clk), .Q(addr[18]) );
  FD1 \addr_reg[17]  ( .D(N63), .CP(clk), .Q(addr[17]) );
  FD1 \addr_reg[16]  ( .D(N62), .CP(clk), .Q(addr[16]) );
  FD1 \addr_reg[15]  ( .D(N61), .CP(clk), .Q(addr[15]) );
  FD1 \addr_reg[14]  ( .D(N60), .CP(clk), .Q(addr[14]) );
  FD1 \addr_reg[13]  ( .D(N59), .CP(clk), .Q(addr[13]) );
  FD1 \addr_reg[12]  ( .D(N58), .CP(clk), .Q(addr[12]) );
  FD1 \addr_reg[11]  ( .D(N57), .CP(clk), .Q(addr[11]) );
  FD1 \addr_reg[10]  ( .D(N56), .CP(clk), .Q(addr[10]) );
  FD1 \addr_reg[9]  ( .D(N55), .CP(clk), .Q(addr[9]) );
  FD1 WRITE_reg ( .D(valid), .CP(clk), .Q(WRITE) );
  FD1 \addr_reg[5]  ( .D(N51), .CP(clk), .Q(addr[5]) );
  FD1 \addr_reg[4]  ( .D(N50), .CP(clk), .Q(addr[4]) );
  FD1 \addr_reg[3]  ( .D(N49), .CP(clk), .Q(addr[3]) );
  FD1 \addr_reg[2]  ( .D(N48), .CP(clk), .Q(addr[2]) );
  FD1 \addr_reg[1]  ( .D(N47), .CP(clk), .Q(addr[1]) );
  FD1 \addr_reg[0]  ( .D(N46), .CP(clk), .Q(addr[0]) );
  FD1 \datain_reg[31]  ( .D(data[31]), .CP(clk), .Q(datain[31]) );
  FD1 \datain_reg[30]  ( .D(data[30]), .CP(clk), .Q(datain[30]) );
  FD1 \datain_reg[29]  ( .D(data[29]), .CP(clk), .Q(datain[29]) );
  FD1 \datain_reg[28]  ( .D(data[28]), .CP(clk), .Q(datain[28]) );
  FD1 \datain_reg[27]  ( .D(data[27]), .CP(clk), .Q(datain[27]) );
  FD1 \datain_reg[26]  ( .D(data[26]), .CP(clk), .Q(datain[26]) );
  FD1 \datain_reg[25]  ( .D(data[25]), .CP(clk), .Q(datain[25]) );
  FD1 \datain_reg[24]  ( .D(data[24]), .CP(clk), .Q(datain[24]) );
  FD1 \datain_reg[23]  ( .D(data[23]), .CP(clk), .Q(datain[23]) );
  FD1 \datain_reg[22]  ( .D(data[22]), .CP(clk), .Q(datain[22]) );
  FD1 \datain_reg[21]  ( .D(data[21]), .CP(clk), .Q(datain[21]) );
  FD1 \datain_reg[20]  ( .D(data[20]), .CP(clk), .Q(datain[20]) );
  FD1 \datain_reg[19]  ( .D(data[19]), .CP(clk), .Q(datain[19]) );
  FD1 \datain_reg[18]  ( .D(data[18]), .CP(clk), .Q(datain[18]) );
  FD1 \datain_reg[17]  ( .D(data[17]), .CP(clk), .Q(datain[17]) );
  FD1 \datain_reg[16]  ( .D(data[16]), .CP(clk), .Q(datain[16]) );
  FD1 \datain_reg[15]  ( .D(data[15]), .CP(clk), .Q(datain[15]) );
  FD1 \datain_reg[14]  ( .D(data[14]), .CP(clk), .Q(datain[14]) );
  FD1 \datain_reg[13]  ( .D(data[13]), .CP(clk), .Q(datain[13]) );
  FD1 \datain_reg[12]  ( .D(data[12]), .CP(clk), .Q(datain[12]) );
  FD1 \datain_reg[11]  ( .D(data[11]), .CP(clk), .Q(datain[11]) );
  FD1 \datain_reg[10]  ( .D(data[10]), .CP(clk), .Q(datain[10]) );
  FD1 \datain_reg[9]  ( .D(data[9]), .CP(clk), .Q(datain[9]) );
  FD1 \datain_reg[8]  ( .D(data[8]), .CP(clk), .Q(datain[8]) );
  FD1 \datain_reg[7]  ( .D(data[7]), .CP(clk), .Q(datain[7]) );
  FD1 \datain_reg[6]  ( .D(data[6]), .CP(clk), .Q(datain[6]) );
  FD1 \datain_reg[5]  ( .D(data[5]), .CP(clk), .Q(datain[5]) );
  FD1 \datain_reg[4]  ( .D(data[4]), .CP(clk), .Q(datain[4]) );
  FD1 \datain_reg[3]  ( .D(data[3]), .CP(clk), .Q(datain[3]) );
  FD1 \datain_reg[2]  ( .D(data[2]), .CP(clk), .Q(datain[2]) );
  FD1 \datain_reg[1]  ( .D(data[1]), .CP(clk), .Q(datain[1]) );
  FD1 \datain_reg[0]  ( .D(data[0]), .CP(clk), .Q(datain[0]) );
  AN2P U4 ( .A(\mult_111/A2[14] ), .B(y[8]), .Z(n1) );
  IVDA U5 ( .A(N3), .Z(n74) );
  AO7 U6 ( .A(data[48]), .B(data[47]), .C(data[49]), .Z(N3) );
  IVDA U7 ( .A(N16), .Z(n75) );
  AO7 U8 ( .A(data[39]), .B(data[38]), .C(data[40]), .Z(N16) );
  AN2 U9 ( .A(n1), .B(\add_111/carry[17] ), .Z(N64) );
  EO U10 ( .A(\add_111/carry[17] ), .B(n1), .Z(N63) );
  AN2 U11 ( .A(N44), .B(\add_111/carry[16] ), .Z(\add_111/carry[17] ) );
  EO U24 ( .A(\add_111/carry[16] ), .B(N44), .Z(N62) );
  AN2 U25 ( .A(N43), .B(\add_111/carry[15] ), .Z(\add_111/carry[16] ) );
  EO U26 ( .A(\add_111/carry[15] ), .B(N43), .Z(N61) );
  AN2 U27 ( .A(N42), .B(\add_111/carry[14] ), .Z(\add_111/carry[15] ) );
  EO U28 ( .A(\add_111/carry[14] ), .B(N42), .Z(N60) );
  AN2 U29 ( .A(N41), .B(\add_111/carry[13] ), .Z(\add_111/carry[14] ) );
  EO U30 ( .A(\add_111/carry[13] ), .B(N41), .Z(N59) );
  AN2 U31 ( .A(N40), .B(\add_111/carry[12] ), .Z(\add_111/carry[13] ) );
  EO U32 ( .A(\add_111/carry[12] ), .B(N40), .Z(N58) );
  AN2 U33 ( .A(N39), .B(\add_111/carry[11] ), .Z(\add_111/carry[12] ) );
  EO U34 ( .A(\add_111/carry[11] ), .B(N39), .Z(N57) );
  AN2 U35 ( .A(N38), .B(\add_111/carry[10] ), .Z(\add_111/carry[11] ) );
  EO U36 ( .A(\add_111/carry[10] ), .B(N38), .Z(N56) );
  AN2 U37 ( .A(N37), .B(\add_111/carry[9] ), .Z(\add_111/carry[10] ) );
  EO U38 ( .A(\add_111/carry[9] ), .B(N37), .Z(N55) );
  AN2 U39 ( .A(N34), .B(x[6]), .Z(\add_111/carry[7] ) );
  EO U40 ( .A(x[6]), .B(N34), .Z(N52) );
  AN2 U41 ( .A(\mult_111/CARRYB[8][6] ), .B(y[7]), .Z(\mult_111/A2[14] ) );
  EO U42 ( .A(y[7]), .B(\mult_111/CARRYB[8][6] ), .Z(N43) );
  AN2 U43 ( .A(N34), .B(y[2]), .Z(\mult_111/CARRYB[2][6] ) );
  EO U44 ( .A(y[2]), .B(N34), .Z(N36) );
  NR2 U45 ( .A(n1), .B(n76), .Z(N44) );
  NR2 U46 ( .A(\mult_111/A2[14] ), .B(y[8]), .Z(n76) );
  AN2P U47 ( .A(data[44]), .B(n74), .Z(N9) );
endmodule


module compute_xy_center ( clk, valid_n, data, angle, data_valid, data2, 
        data_valid2, pixel_count );
  input [63:0] data;
  input [7:0] angle;
  output [63:0] data2;
  output [31:0] pixel_count;
  input clk, valid_n, data_valid;
  output data_valid2;
  wire   N5, N6, N7, N8, N9, N10, N11, N12, N13, N14, N15, N16, N17, N18, N19,
         N20, N21, N22, N23, N24, N25, N26, N27, N28, N29, N30, N31, N32, N33,
         N34, N35, N36, N141, N142, N143, N144, N145, N146, N147, N148, N149,
         N189, N190, N191, N192, N193, N194, N195, N196, N197, n159, n160,
         n161, n163, n164, n165, n166, n167, n168, n169, n170, n171, n172,
         n173, n174, n175, n176, n177, n178, n179, n180, n181, n182, n183,
         n184, n185, n186, n187, n188, n189, n190, n191, n192, n193, n194,
         n195, n196, n197, n198, n199, n200, n201, n202, n203, n204, n205,
         N188, N187, N186, N185, N184, N183, N182, N181, N180, N179, N178,
         N177, N176, N175, N174, N173, N172, N171, N170, N169, N168, N167,
         N166, N165, N164, N163, N162, N161, N160, N159, N158, N157, N156,
         N155, N154, N153, N152, N151, N150, N140, N139, N138, N137, N136,
         N135, N134, N133, N132, N131, N130, N129, N128, N127, N126, N125,
         N124, N123, N122, N121, N120, N119, N118, N117, N116, N115, N114,
         N113, N112, N111, N110, N109, N108, N107, N106, N105, N104, N103,
         N102, n3, n4, n5, n25, n113, n114, n115, n116, n117, n118, n135, n136,
         n137, n138, n139, n140, n141, n142, n143, n144, n145, n146, n147,
         n148, n149, n150, n151, n152, n153, n154, n155, n156, n157, n158,
         n162, n206, n207, n208, n209, n210, n211, n212;
  wire   [14:0] x;
  wire   [15:0] cos_val;
  wire   [14:0] y;
  wire   [15:0] sin_val;
  wire   [15:0] xoffset;
  wire   [15:0] yoffset;
  wire   [8:0] xtemp;
  wire   [8:0] ytemp;
  wire   SYNOPSYS_UNCONNECTED__0, SYNOPSYS_UNCONNECTED__1, 
        SYNOPSYS_UNCONNECTED__2, SYNOPSYS_UNCONNECTED__3, 
        SYNOPSYS_UNCONNECTED__4, SYNOPSYS_UNCONNECTED__5, 
        SYNOPSYS_UNCONNECTED__6, SYNOPSYS_UNCONNECTED__7, 
        SYNOPSYS_UNCONNECTED__8, SYNOPSYS_UNCONNECTED__9, 
        SYNOPSYS_UNCONNECTED__10, SYNOPSYS_UNCONNECTED__11, 
        SYNOPSYS_UNCONNECTED__12, SYNOPSYS_UNCONNECTED__13, 
        SYNOPSYS_UNCONNECTED__14, SYNOPSYS_UNCONNECTED__15, 
        SYNOPSYS_UNCONNECTED__16, SYNOPSYS_UNCONNECTED__17, 
        SYNOPSYS_UNCONNECTED__18, SYNOPSYS_UNCONNECTED__19, 
        SYNOPSYS_UNCONNECTED__20, SYNOPSYS_UNCONNECTED__21, 
        SYNOPSYS_UNCONNECTED__22, SYNOPSYS_UNCONNECTED__23, 
        SYNOPSYS_UNCONNECTED__24, SYNOPSYS_UNCONNECTED__25, 
        SYNOPSYS_UNCONNECTED__26, SYNOPSYS_UNCONNECTED__27, 
        SYNOPSYS_UNCONNECTED__28, SYNOPSYS_UNCONNECTED__29, 
        SYNOPSYS_UNCONNECTED__30, SYNOPSYS_UNCONNECTED__31, 
        SYNOPSYS_UNCONNECTED__32, SYNOPSYS_UNCONNECTED__33, 
        SYNOPSYS_UNCONNECTED__34, SYNOPSYS_UNCONNECTED__35, 
        SYNOPSYS_UNCONNECTED__36, SYNOPSYS_UNCONNECTED__37, 
        SYNOPSYS_UNCONNECTED__38, SYNOPSYS_UNCONNECTED__39, 
        SYNOPSYS_UNCONNECTED__40, SYNOPSYS_UNCONNECTED__41, 
        SYNOPSYS_UNCONNECTED__42, SYNOPSYS_UNCONNECTED__43, 
        SYNOPSYS_UNCONNECTED__44, SYNOPSYS_UNCONNECTED__45, 
        SYNOPSYS_UNCONNECTED__46, SYNOPSYS_UNCONNECTED__47, 
        SYNOPSYS_UNCONNECTED__48, SYNOPSYS_UNCONNECTED__49, 
        SYNOPSYS_UNCONNECTED__50, SYNOPSYS_UNCONNECTED__51, 
        SYNOPSYS_UNCONNECTED__52, SYNOPSYS_UNCONNECTED__53, 
        SYNOPSYS_UNCONNECTED__54, SYNOPSYS_UNCONNECTED__55, 
        SYNOPSYS_UNCONNECTED__56, SYNOPSYS_UNCONNECTED__57, 
        SYNOPSYS_UNCONNECTED__58, SYNOPSYS_UNCONNECTED__59, 
        SYNOPSYS_UNCONNECTED__60, SYNOPSYS_UNCONNECTED__61, 
        SYNOPSYS_UNCONNECTED__62, SYNOPSYS_UNCONNECTED__63, 
        SYNOPSYS_UNCONNECTED__64, SYNOPSYS_UNCONNECTED__65, 
        SYNOPSYS_UNCONNECTED__66, SYNOPSYS_UNCONNECTED__67, 
        SYNOPSYS_UNCONNECTED__68, SYNOPSYS_UNCONNECTED__69, 
        SYNOPSYS_UNCONNECTED__70, SYNOPSYS_UNCONNECTED__71, 
        SYNOPSYS_UNCONNECTED__72, SYNOPSYS_UNCONNECTED__73, 
        SYNOPSYS_UNCONNECTED__74, SYNOPSYS_UNCONNECTED__75, 
        SYNOPSYS_UNCONNECTED__76, SYNOPSYS_UNCONNECTED__77, 
        SYNOPSYS_UNCONNECTED__78, SYNOPSYS_UNCONNECTED__79, 
        SYNOPSYS_UNCONNECTED__80, SYNOPSYS_UNCONNECTED__81, 
        SYNOPSYS_UNCONNECTED__82, SYNOPSYS_UNCONNECTED__83, 
        SYNOPSYS_UNCONNECTED__84, SYNOPSYS_UNCONNECTED__85, 
        SYNOPSYS_UNCONNECTED__86, SYNOPSYS_UNCONNECTED__87, 
        SYNOPSYS_UNCONNECTED__88, SYNOPSYS_UNCONNECTED__89, 
        SYNOPSYS_UNCONNECTED__90, SYNOPSYS_UNCONNECTED__91, 
        SYNOPSYS_UNCONNECTED__92, SYNOPSYS_UNCONNECTED__93, 
        SYNOPSYS_UNCONNECTED__94, SYNOPSYS_UNCONNECTED__95, 
        SYNOPSYS_UNCONNECTED__96, SYNOPSYS_UNCONNECTED__97, 
        SYNOPSYS_UNCONNECTED__98, SYNOPSYS_UNCONNECTED__99, 
        SYNOPSYS_UNCONNECTED__100, SYNOPSYS_UNCONNECTED__101, 
        SYNOPSYS_UNCONNECTED__102, SYNOPSYS_UNCONNECTED__103;
  assign data2[63] = 1'b0;
  assign data2[62] = 1'b0;
  assign data2[61] = 1'b0;
  assign data2[60] = 1'b0;
  assign data2[59] = 1'b0;
  assign data2[58] = 1'b0;
  assign data2[57] = 1'b0;
  assign data2[56] = 1'b0;
  assign data2[55] = 1'b0;
  assign data2[54] = 1'b0;
  assign data2[53] = 1'b0;
  assign data2[52] = 1'b0;
  assign data2[51] = 1'b0;
  assign data2[50] = 1'b0;

  AN4P U67 ( .A(n202), .B(n203), .C(n204), .D(n205), .Z(n195) );
  FD1P \x_reg[2]  ( .D(data[43]), .CP(clk), .Q(x[2]) );
  FD1P \x_reg[1]  ( .D(data[42]), .CP(clk), .Q(x[1]) );
  FD1P \x_reg[0]  ( .D(data[41]), .CP(clk), .Q(x[0]) );
  FD1P \pixel_count_reg[0]  ( .D(n135), .CP(clk), .Q(pixel_count[0]) );
  FD1P \pixel_count_reg[1]  ( .D(n212), .CP(clk), .Q(pixel_count[1]) );
  FD1P \y_reg[8]  ( .D(data[40]), .CP(clk), .Q(y[8]) );
  FD1P \y_reg[7]  ( .D(data[39]), .CP(clk), .Q(y[7]) );
  FD1P \y_reg[6]  ( .D(data[38]), .CP(clk), .Q(y[6]) );
  FD1P \y_reg[5]  ( .D(data[37]), .CP(clk), .Q(y[5]) );
  FD1P \y_reg[4]  ( .D(data[36]), .CP(clk), .Q(y[4]) );
  FD1P \y_reg[3]  ( .D(data[35]), .CP(clk), .Q(y[3]) );
  FD1P \y_reg[2]  ( .D(data[34]), .CP(clk), .Q(y[2]) );
  sine sine1 ( .angle(angle), .sin_val({SYNOPSYS_UNCONNECTED__0, 
        SYNOPSYS_UNCONNECTED__1, SYNOPSYS_UNCONNECTED__2, 
        SYNOPSYS_UNCONNECTED__3, SYNOPSYS_UNCONNECTED__4, 
        SYNOPSYS_UNCONNECTED__5, SYNOPSYS_UNCONNECTED__6, 
        SYNOPSYS_UNCONNECTED__7, SYNOPSYS_UNCONNECTED__8, sin_val[6:0]}) );
  cosine cosine1 ( .angle(angle), .cos_val({SYNOPSYS_UNCONNECTED__9, 
        SYNOPSYS_UNCONNECTED__10, SYNOPSYS_UNCONNECTED__11, 
        SYNOPSYS_UNCONNECTED__12, SYNOPSYS_UNCONNECTED__13, 
        SYNOPSYS_UNCONNECTED__14, SYNOPSYS_UNCONNECTED__15, 
        SYNOPSYS_UNCONNECTED__16, SYNOPSYS_UNCONNECTED__17, cos_val[6:0]}) );
  xcompute xcompute_inst ( .angle(angle), .offset({SYNOPSYS_UNCONNECTED__18, 
        SYNOPSYS_UNCONNECTED__19, SYNOPSYS_UNCONNECTED__20, 
        SYNOPSYS_UNCONNECTED__21, SYNOPSYS_UNCONNECTED__22, 
        SYNOPSYS_UNCONNECTED__23, SYNOPSYS_UNCONNECTED__24, xoffset[8:0]}) );
  ycompute ycompute_inst ( .angle(angle), .offset({SYNOPSYS_UNCONNECTED__25, 
        SYNOPSYS_UNCONNECTED__26, SYNOPSYS_UNCONNECTED__27, 
        SYNOPSYS_UNCONNECTED__28, SYNOPSYS_UNCONNECTED__29, 
        SYNOPSYS_UNCONNECTED__30, SYNOPSYS_UNCONNECTED__31, yoffset[8:0]}) );
  compute_xy_center_DW01_inc_0 add_148 ( .A(pixel_count), .SUM({N36, N35, N34, 
        N33, N32, N31, N30, N29, N28, N27, N26, N25, N24, N23, N22, N21, N20, 
        N19, N18, N17, N16, N15, N14, N13, N12, N11, N10, N9, N8, N7, N6, N5})
         );
  compute_xy_center_DW02_mult_3 mult_152 ( .A({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, x[8:0]}), .B({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        sin_val[6:5], n4, sin_val[3:0]}), .TC(1'b0), .PRODUCT({
        SYNOPSYS_UNCONNECTED__32, SYNOPSYS_UNCONNECTED__33, 
        SYNOPSYS_UNCONNECTED__34, SYNOPSYS_UNCONNECTED__35, 
        SYNOPSYS_UNCONNECTED__36, SYNOPSYS_UNCONNECTED__37, 
        SYNOPSYS_UNCONNECTED__38, SYNOPSYS_UNCONNECTED__39, 
        SYNOPSYS_UNCONNECTED__40, SYNOPSYS_UNCONNECTED__41, 
        SYNOPSYS_UNCONNECTED__42, SYNOPSYS_UNCONNECTED__43, 
        SYNOPSYS_UNCONNECTED__44, SYNOPSYS_UNCONNECTED__45, 
        SYNOPSYS_UNCONNECTED__46, N164, N163, N162, N161, N160, N159, N158, 
        N157, N156, N155, N154, N153, N152, N151, N150}) );
  compute_xy_center_DW02_mult_2 mult_152_2 ( .A({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, n113, cos_val[5:3], n114, cos_val[1:0]}), .B({1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, y[8:0]}), .TC(1'b0), .PRODUCT({
        SYNOPSYS_UNCONNECTED__47, SYNOPSYS_UNCONNECTED__48, 
        SYNOPSYS_UNCONNECTED__49, SYNOPSYS_UNCONNECTED__50, 
        SYNOPSYS_UNCONNECTED__51, SYNOPSYS_UNCONNECTED__52, 
        SYNOPSYS_UNCONNECTED__53, SYNOPSYS_UNCONNECTED__54, 
        SYNOPSYS_UNCONNECTED__55, SYNOPSYS_UNCONNECTED__56, 
        SYNOPSYS_UNCONNECTED__57, SYNOPSYS_UNCONNECTED__58, 
        SYNOPSYS_UNCONNECTED__59, SYNOPSYS_UNCONNECTED__60, 
        SYNOPSYS_UNCONNECTED__61, N179, N178, N177, N176, N175, N174, N173, 
        N172, N171, N170, N169, N168, N167, N166, N165}) );
  compute_xy_center_DW02_mult_1 mult_151_2 ( .A({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, y[8:0]}), .B({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        sin_val[6:5], n25, sin_val[3:0]}), .TC(1'b0), .PRODUCT({
        SYNOPSYS_UNCONNECTED__62, SYNOPSYS_UNCONNECTED__63, 
        SYNOPSYS_UNCONNECTED__64, SYNOPSYS_UNCONNECTED__65, 
        SYNOPSYS_UNCONNECTED__66, SYNOPSYS_UNCONNECTED__67, 
        SYNOPSYS_UNCONNECTED__68, SYNOPSYS_UNCONNECTED__69, 
        SYNOPSYS_UNCONNECTED__70, SYNOPSYS_UNCONNECTED__71, 
        SYNOPSYS_UNCONNECTED__72, SYNOPSYS_UNCONNECTED__73, 
        SYNOPSYS_UNCONNECTED__74, SYNOPSYS_UNCONNECTED__75, 
        SYNOPSYS_UNCONNECTED__76, N131, N130, N129, N128, N127, N126, N125, 
        N124, N123, N122, N121, N120, N119, N118, N117}) );
  compute_xy_center_DW02_mult_0 mult_151 ( .A({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, x[8:0]}), .B({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        cos_val[6:0]}), .TC(1'b0), .PRODUCT({SYNOPSYS_UNCONNECTED__77, 
        SYNOPSYS_UNCONNECTED__78, SYNOPSYS_UNCONNECTED__79, 
        SYNOPSYS_UNCONNECTED__80, SYNOPSYS_UNCONNECTED__81, 
        SYNOPSYS_UNCONNECTED__82, SYNOPSYS_UNCONNECTED__83, 
        SYNOPSYS_UNCONNECTED__84, SYNOPSYS_UNCONNECTED__85, 
        SYNOPSYS_UNCONNECTED__86, SYNOPSYS_UNCONNECTED__87, 
        SYNOPSYS_UNCONNECTED__88, SYNOPSYS_UNCONNECTED__89, 
        SYNOPSYS_UNCONNECTED__90, SYNOPSYS_UNCONNECTED__91, N116, N115, N114, 
        N113, N112, N111, N110, N109, N108, N107, N106, N105, N104, N103, N102}) );
  compute_xy_center_DW01_sub_1 sub_151 ( .A({N116, N115, N114, N113, N112, 
        N111, N110, N109, N108, N107, N106, N105, N104, N103, N102}), .B({N131, 
        N130, N129, N128, N127, N126, N125, N124, N123, N122, N121, N120, N119, 
        N118, N117}), .CI(1'b0), .DIFF({N140, N139, N138, N137, N136, N135, 
        N134, N133, N132, SYNOPSYS_UNCONNECTED__92, SYNOPSYS_UNCONNECTED__93, 
        SYNOPSYS_UNCONNECTED__94, SYNOPSYS_UNCONNECTED__95, 
        SYNOPSYS_UNCONNECTED__96, SYNOPSYS_UNCONNECTED__97}) );
  compute_xy_center_DW01_add_8 add_152 ( .A({N164, N163, N162, N161, N160, 
        N159, N158, N157, N156, N155, N154, N153, N152, N151, N150}), .B({N179, 
        N178, N177, N176, N175, N174, N173, N172, N171, N170, N169, N168, N167, 
        N166, N165}), .CI(1'b0), .SUM({N188, N187, N186, N185, N184, N183, 
        N182, N181, N180, SYNOPSYS_UNCONNECTED__98, SYNOPSYS_UNCONNECTED__99, 
        SYNOPSYS_UNCONNECTED__100, SYNOPSYS_UNCONNECTED__101, 
        SYNOPSYS_UNCONNECTED__102, SYNOPSYS_UNCONNECTED__103}) );
  compute_xy_center_DW01_add_7 add_152_2 ( .A({N188, N187, N186, N185, N184, 
        N183, N182, N181, N180}), .B(yoffset[8:0]), .CI(1'b0), .SUM({N197, 
        N196, N195, N194, N193, N192, N191, N190, N189}) );
  compute_xy_center_DW01_add_9 add_151 ( .A({N140, N139, N138, N137, N136, 
        N135, N134, N133, N132}), .B(xoffset[8:0]), .CI(1'b0), .SUM({N149, 
        N148, N147, N146, N145, N144, N143, N142, N141}) );
  FD1 \data2_reg[47]  ( .D(xtemp[6]), .CP(clk), .Q(data2[47]) );
  FD1 \data2_reg[44]  ( .D(xtemp[3]), .CP(clk), .Q(data2[44]) );
  FD1 \data2_reg[40]  ( .D(ytemp[8]), .CP(clk), .Q(data2[40]) );
  FD1 \data2_reg[39]  ( .D(ytemp[7]), .CP(clk), .Q(data2[39]) );
  FD1 \data2_reg[38]  ( .D(ytemp[6]), .CP(clk), .Q(data2[38]) );
  FD1 \data2_reg[49]  ( .D(xtemp[8]), .CP(clk), .Q(data2[49]) );
  FD1 \data2_reg[48]  ( .D(xtemp[7]), .CP(clk), .Q(data2[48]) );
  FD1 \pixel_count_reg[27]  ( .D(n140), .CP(clk), .Q(pixel_count[27]) );
  FD1 \pixel_count_reg[28]  ( .D(n139), .CP(clk), .Q(pixel_count[28]) );
  FD1 \pixel_count_reg[25]  ( .D(n142), .CP(clk), .Q(pixel_count[25]) );
  FD1 \pixel_count_reg[26]  ( .D(n141), .CP(clk), .Q(pixel_count[26]) );
  FD1 \pixel_count_reg[24]  ( .D(n143), .CP(clk), .Q(pixel_count[24]) );
  FD1 \pixel_count_reg[23]  ( .D(n144), .CP(clk), .Q(pixel_count[23]) );
  FD1 \pixel_count_reg[22]  ( .D(n145), .CP(clk), .Q(pixel_count[22]) );
  FD1 \pixel_count_reg[21]  ( .D(n146), .CP(clk), .Q(pixel_count[21]) );
  FD1 \pixel_count_reg[20]  ( .D(n147), .CP(clk), .Q(pixel_count[20]) );
  FD1 \pixel_count_reg[19]  ( .D(n148), .CP(clk), .Q(pixel_count[19]) );
  FD1 \pixel_count_reg[18]  ( .D(n149), .CP(clk), .Q(pixel_count[18]) );
  FD1 \pixel_count_reg[17]  ( .D(n150), .CP(clk), .Q(pixel_count[17]) );
  FD1 \pixel_count_reg[16]  ( .D(n151), .CP(clk), .Q(pixel_count[16]) );
  FD1 \pixel_count_reg[15]  ( .D(n152), .CP(clk), .Q(pixel_count[15]) );
  FD1 \pixel_count_reg[13]  ( .D(n154), .CP(clk), .Q(pixel_count[13]), .QN(
        n159) );
  FD1 \pixel_count_reg[14]  ( .D(n153), .CP(clk), .Q(pixel_count[14]) );
  FD1 \pixel_count_reg[12]  ( .D(n155), .CP(clk), .Q(pixel_count[12]), .QN(
        n160) );
  FD1 \x_reg[8]  ( .D(data[49]), .CP(clk), .Q(x[8]) );
  FD1 \pixel_count_reg[11]  ( .D(n156), .CP(clk), .Q(pixel_count[11]) );
  FD1 \pixel_count_reg[10]  ( .D(n157), .CP(clk), .Q(pixel_count[10]) );
  FD1 \x_reg[7]  ( .D(data[48]), .CP(clk), .Q(x[7]) );
  FD1 \pixel_count_reg[9]  ( .D(n158), .CP(clk), .Q(pixel_count[9]) );
  FD1 \pixel_count_reg[8]  ( .D(n162), .CP(clk), .Q(pixel_count[8]) );
  FD1 \x_reg[6]  ( .D(data[47]), .CP(clk), .Q(x[6]) );
  FD1 \pixel_count_reg[7]  ( .D(n206), .CP(clk), .Q(pixel_count[7]) );
  FD1 \pixel_count_reg[6]  ( .D(n207), .CP(clk), .Q(pixel_count[6]) );
  FD1 \x_reg[5]  ( .D(data[46]), .CP(clk), .Q(x[5]) );
  FD1 \pixel_count_reg[5]  ( .D(n208), .CP(clk), .Q(pixel_count[5]) );
  FD1 \ytemp_reg[0]  ( .D(N189), .CP(clk), .Q(ytemp[0]) );
  FD1 \xtemp_reg[0]  ( .D(N141), .CP(clk), .Q(xtemp[0]) );
  FD1 \ytemp_reg[3]  ( .D(N192), .CP(clk), .Q(ytemp[3]) );
  FD1 \ytemp_reg[2]  ( .D(N191), .CP(clk), .Q(ytemp[2]) );
  FD1 \ytemp_reg[1]  ( .D(N190), .CP(clk), .Q(ytemp[1]) );
  FD1 \xtemp_reg[3]  ( .D(N144), .CP(clk), .Q(xtemp[3]) );
  FD1 \xtemp_reg[2]  ( .D(N143), .CP(clk), .Q(xtemp[2]) );
  FD1 \xtemp_reg[1]  ( .D(N142), .CP(clk), .Q(xtemp[1]) );
  FD1 \data2_reg[31]  ( .D(data[31]), .CP(clk), .Q(data2[31]) );
  FD1 \data2_reg[30]  ( .D(data[30]), .CP(clk), .Q(data2[30]) );
  FD1 \data2_reg[29]  ( .D(data[29]), .CP(clk), .Q(data2[29]) );
  FD1 \data2_reg[28]  ( .D(data[28]), .CP(clk), .Q(data2[28]) );
  FD1 \data2_reg[27]  ( .D(data[27]), .CP(clk), .Q(data2[27]) );
  FD1 \data2_reg[26]  ( .D(data[26]), .CP(clk), .Q(data2[26]) );
  FD1 \data2_reg[25]  ( .D(data[25]), .CP(clk), .Q(data2[25]) );
  FD1 \data2_reg[24]  ( .D(data[24]), .CP(clk), .Q(data2[24]) );
  FD1 \data2_reg[23]  ( .D(data[23]), .CP(clk), .Q(data2[23]) );
  FD1 \data2_reg[22]  ( .D(data[22]), .CP(clk), .Q(data2[22]) );
  FD1 \data2_reg[21]  ( .D(data[21]), .CP(clk), .Q(data2[21]) );
  FD1 \data2_reg[20]  ( .D(data[20]), .CP(clk), .Q(data2[20]) );
  FD1 \data2_reg[19]  ( .D(data[19]), .CP(clk), .Q(data2[19]) );
  FD1 \data2_reg[18]  ( .D(data[18]), .CP(clk), .Q(data2[18]) );
  FD1 \data2_reg[17]  ( .D(data[17]), .CP(clk), .Q(data2[17]) );
  FD1 \data2_reg[16]  ( .D(data[16]), .CP(clk), .Q(data2[16]) );
  FD1 \data2_reg[15]  ( .D(data[15]), .CP(clk), .Q(data2[15]) );
  FD1 \data2_reg[14]  ( .D(data[14]), .CP(clk), .Q(data2[14]) );
  FD1 \data2_reg[13]  ( .D(data[13]), .CP(clk), .Q(data2[13]) );
  FD1 \data2_reg[12]  ( .D(data[12]), .CP(clk), .Q(data2[12]) );
  FD1 \data2_reg[11]  ( .D(data[11]), .CP(clk), .Q(data2[11]) );
  FD1 \data2_reg[10]  ( .D(data[10]), .CP(clk), .Q(data2[10]) );
  FD1 \data2_reg[9]  ( .D(data[9]), .CP(clk), .Q(data2[9]) );
  FD1 \data2_reg[8]  ( .D(data[8]), .CP(clk), .Q(data2[8]) );
  FD1 \data2_reg[7]  ( .D(data[7]), .CP(clk), .Q(data2[7]) );
  FD1 \data2_reg[6]  ( .D(data[6]), .CP(clk), .Q(data2[6]) );
  FD1 \data2_reg[5]  ( .D(data[5]), .CP(clk), .Q(data2[5]) );
  FD1 \data2_reg[4]  ( .D(data[4]), .CP(clk), .Q(data2[4]) );
  FD1 \data2_reg[3]  ( .D(data[3]), .CP(clk), .Q(data2[3]) );
  FD1 \data2_reg[2]  ( .D(data[2]), .CP(clk), .Q(data2[2]) );
  FD1 \data2_reg[1]  ( .D(data[1]), .CP(clk), .Q(data2[1]) );
  FD1 \data2_reg[0]  ( .D(data[0]), .CP(clk), .Q(data2[0]) );
  FD1 \data2_reg[37]  ( .D(ytemp[5]), .CP(clk), .Q(data2[37]) );
  FD1 \data2_reg[36]  ( .D(ytemp[4]), .CP(clk), .Q(data2[36]) );
  FD1 \data2_reg[35]  ( .D(ytemp[3]), .CP(clk), .Q(data2[35]) );
  FD1 \data2_reg[34]  ( .D(ytemp[2]), .CP(clk), .Q(data2[34]) );
  FD1 \data2_reg[33]  ( .D(ytemp[1]), .CP(clk), .Q(data2[33]) );
  FD1 \data2_reg[32]  ( .D(ytemp[0]), .CP(clk), .Q(data2[32]) );
  FD1 \data2_reg[46]  ( .D(xtemp[5]), .CP(clk), .Q(data2[46]) );
  FD1 \data2_reg[45]  ( .D(xtemp[4]), .CP(clk), .Q(data2[45]) );
  FD1 \data2_reg[43]  ( .D(xtemp[2]), .CP(clk), .Q(data2[43]) );
  FD1 \data2_reg[42]  ( .D(xtemp[1]), .CP(clk), .Q(data2[42]) );
  FD1 \data2_reg[41]  ( .D(xtemp[0]), .CP(clk), .Q(data2[41]) );
  FD1 data_valid2_reg ( .D(data_valid), .CP(clk), .Q(data_valid2) );
  FD1 \pixel_count_reg[3]  ( .D(n210), .CP(clk), .Q(pixel_count[3]) );
  FD1 \pixel_count_reg[29]  ( .D(n138), .CP(clk), .Q(pixel_count[29]) );
  FD1 \ytemp_reg[4]  ( .D(N193), .CP(clk), .Q(ytemp[4]) );
  FD1 \xtemp_reg[4]  ( .D(N145), .CP(clk), .Q(xtemp[4]) );
  FD1 \x_reg[3]  ( .D(data[44]), .CP(clk), .Q(x[3]) );
  FD1 \pixel_count_reg[2]  ( .D(n211), .CP(clk), .Q(pixel_count[2]) );
  FD1 \pixel_count_reg[30]  ( .D(n137), .CP(clk), .Q(pixel_count[30]) );
  FD1 \xtemp_reg[5]  ( .D(N146), .CP(clk), .Q(xtemp[5]) );
  FD1 \pixel_count_reg[31]  ( .D(n136), .CP(clk), .Q(pixel_count[31]) );
  FD1 \ytemp_reg[5]  ( .D(N194), .CP(clk), .Q(ytemp[5]) );
  FD1 \xtemp_reg[6]  ( .D(N147), .CP(clk), .Q(xtemp[6]) );
  FD1 \y_reg[1]  ( .D(data[33]), .CP(clk), .Q(y[1]) );
  FD1 \y_reg[0]  ( .D(data[32]), .CP(clk), .Q(y[0]) );
  FD1 \ytemp_reg[6]  ( .D(N195), .CP(clk), .Q(ytemp[6]) );
  FD1 \xtemp_reg[7]  ( .D(N148), .CP(clk), .Q(xtemp[7]) );
  FD1 \ytemp_reg[7]  ( .D(N196), .CP(clk), .Q(ytemp[7]) );
  FD1 \ytemp_reg[8]  ( .D(N197), .CP(clk), .Q(ytemp[8]) );
  FD1 \xtemp_reg[8]  ( .D(N149), .CP(clk), .Q(xtemp[8]) );
  FD1 \pixel_count_reg[4]  ( .D(n209), .CP(clk), .Q(pixel_count[4]) );
  FD1 \x_reg[4]  ( .D(data[45]), .CP(clk), .Q(x[4]) );
  AN2P U29 ( .A(valid_n), .B(n197), .Z(n3) );
  B3IP U30 ( .A(sin_val[4]), .Z1(n5), .Z2(n4) );
  B2I U31 ( .A(cos_val[2]), .Z2(n114) );
  B4IP U32 ( .A(n5), .Z(n25) );
  AO2P U33 ( .A(n3), .B(pixel_count[31]), .C(N36), .D(n115), .Z(n161) );
  IV U34 ( .A(n161), .Z(n136) );
  IV U35 ( .A(n165), .Z(n138) );
  IVDA U36 ( .A(n166), .Y(n139) );
  IVP U37 ( .A(n118), .Z(n115) );
  IVP U38 ( .A(n118), .Z(n116) );
  IVDA U39 ( .A(cos_val[6]), .Z(n113) );
  AO6 U40 ( .A(n195), .B(n196), .C(n197), .Z(n163) );
  NR4 U41 ( .A(n198), .B(n199), .C(n200), .D(n201), .Z(n196) );
  IVP U42 ( .A(n164), .Z(n137) );
  AO2 U43 ( .A(n3), .B(pixel_count[30]), .C(N35), .D(n115), .Z(n164) );
  AO2 U44 ( .A(n3), .B(pixel_count[29]), .C(N34), .D(n115), .Z(n165) );
  AO2 U45 ( .A(n3), .B(pixel_count[28]), .C(N33), .D(n115), .Z(n166) );
  IVP U46 ( .A(n167), .Z(n140) );
  AO2 U47 ( .A(n3), .B(pixel_count[27]), .C(N32), .D(n115), .Z(n167) );
  IVP U48 ( .A(n168), .Z(n141) );
  AO2 U49 ( .A(n3), .B(pixel_count[26]), .C(N31), .D(n115), .Z(n168) );
  IVP U50 ( .A(n169), .Z(n142) );
  AO2 U51 ( .A(n3), .B(pixel_count[25]), .C(N30), .D(n115), .Z(n169) );
  IVP U52 ( .A(n170), .Z(n143) );
  AO2 U53 ( .A(n3), .B(pixel_count[24]), .C(N29), .D(n115), .Z(n170) );
  IVP U54 ( .A(n171), .Z(n144) );
  AO2 U55 ( .A(n3), .B(pixel_count[23]), .C(N28), .D(n115), .Z(n171) );
  IVP U56 ( .A(n172), .Z(n145) );
  AO2 U57 ( .A(n3), .B(pixel_count[22]), .C(N27), .D(n115), .Z(n172) );
  IVP U58 ( .A(n173), .Z(n146) );
  AO2 U59 ( .A(n3), .B(pixel_count[21]), .C(N26), .D(n115), .Z(n173) );
  IVP U60 ( .A(n174), .Z(n147) );
  AO2 U61 ( .A(n3), .B(pixel_count[20]), .C(N25), .D(n115), .Z(n174) );
  IVP U62 ( .A(n175), .Z(n148) );
  AO2 U63 ( .A(n3), .B(pixel_count[19]), .C(N24), .D(n116), .Z(n175) );
  IVP U64 ( .A(n176), .Z(n149) );
  AO2 U65 ( .A(n3), .B(pixel_count[18]), .C(N23), .D(n116), .Z(n176) );
  IVP U66 ( .A(n177), .Z(n150) );
  AO2 U68 ( .A(n3), .B(pixel_count[17]), .C(N22), .D(n116), .Z(n177) );
  IVP U69 ( .A(n178), .Z(n151) );
  AO2 U70 ( .A(pixel_count[16]), .B(n3), .C(N21), .D(n116), .Z(n178) );
  IVP U71 ( .A(n179), .Z(n152) );
  AO2 U72 ( .A(pixel_count[15]), .B(n3), .C(N20), .D(n116), .Z(n179) );
  IVP U73 ( .A(n180), .Z(n153) );
  AO2 U74 ( .A(n3), .B(pixel_count[14]), .C(N19), .D(n116), .Z(n180) );
  IVP U75 ( .A(n181), .Z(n154) );
  AO2 U76 ( .A(n3), .B(pixel_count[13]), .C(N18), .D(n116), .Z(n181) );
  IVP U77 ( .A(n182), .Z(n155) );
  AO2 U78 ( .A(n3), .B(pixel_count[12]), .C(N17), .D(n116), .Z(n182) );
  IVP U79 ( .A(n183), .Z(n156) );
  AO2 U80 ( .A(pixel_count[11]), .B(n3), .C(N16), .D(n116), .Z(n183) );
  IVP U81 ( .A(n184), .Z(n157) );
  AO2 U82 ( .A(pixel_count[10]), .B(n3), .C(N15), .D(n116), .Z(n184) );
  IVP U83 ( .A(n185), .Z(n158) );
  AO2 U84 ( .A(pixel_count[9]), .B(n3), .C(N14), .D(n116), .Z(n185) );
  IVP U85 ( .A(n186), .Z(n162) );
  AO2 U86 ( .A(pixel_count[8]), .B(n3), .C(N13), .D(n116), .Z(n186) );
  IVP U87 ( .A(n187), .Z(n206) );
  AO2 U88 ( .A(pixel_count[7]), .B(n3), .C(N12), .D(n117), .Z(n187) );
  NR4 U89 ( .A(pixel_count[27]), .B(pixel_count[26]), .C(pixel_count[25]), .D(
        pixel_count[24]), .Z(n204) );
  NR4 U90 ( .A(pixel_count[23]), .B(pixel_count[22]), .C(pixel_count[21]), .D(
        pixel_count[20]), .Z(n203) );
  NR4 U91 ( .A(pixel_count[19]), .B(pixel_count[18]), .C(pixel_count[17]), .D(
        pixel_count[14]), .Z(n202) );
  NR4 U92 ( .A(pixel_count[31]), .B(pixel_count[30]), .C(pixel_count[29]), .D(
        pixel_count[28]), .Z(n205) );
  IVP U93 ( .A(n188), .Z(n207) );
  AO2 U94 ( .A(pixel_count[6]), .B(n3), .C(N11), .D(n117), .Z(n188) );
  IVP U95 ( .A(n189), .Z(n208) );
  AO2 U96 ( .A(pixel_count[5]), .B(n3), .C(N10), .D(n117), .Z(n189) );
  IVP U97 ( .A(n190), .Z(n209) );
  AO2 U98 ( .A(pixel_count[4]), .B(n3), .C(N9), .D(n117), .Z(n190) );
  IVP U99 ( .A(n191), .Z(n210) );
  AO2 U100 ( .A(pixel_count[3]), .B(n3), .C(N8), .D(n117), .Z(n191) );
  IVP U101 ( .A(n192), .Z(n211) );
  AO2 U102 ( .A(pixel_count[2]), .B(n3), .C(N7), .D(n117), .Z(n192) );
  IVP U103 ( .A(n193), .Z(n212) );
  AO2 U104 ( .A(pixel_count[1]), .B(n3), .C(N6), .D(n117), .Z(n193) );
  IVP U105 ( .A(n194), .Z(n135) );
  AO2 U106 ( .A(pixel_count[0]), .B(n3), .C(N5), .D(n117), .Z(n194) );
  ND4 U107 ( .A(n159), .B(n160), .C(pixel_count[0]), .D(pixel_count[10]), .Z(
        n201) );
  ND4 U108 ( .A(pixel_count[11]), .B(pixel_count[15]), .C(pixel_count[16]), 
        .D(pixel_count[1]), .Z(n200) );
  ND4 U109 ( .A(pixel_count[2]), .B(pixel_count[3]), .C(pixel_count[4]), .D(
        pixel_count[5]), .Z(n199) );
  ND4 U110 ( .A(pixel_count[6]), .B(pixel_count[7]), .C(pixel_count[8]), .D(
        pixel_count[9]), .Z(n198) );
  ND2 U111 ( .A(valid_n), .B(data_valid), .Z(n197) );
  IVA U112 ( .A(n118), .Z(n117) );
  IVA U113 ( .A(n163), .Z(n118) );
endmodule


module read_mem_20000 ( clk, rst_n, valid_n, READ, addr, datain, data, 
        data_valid );
  output [31:0] addr;
  input [31:0] datain;
  output [63:0] data;
  input clk, rst_n, valid_n;
  output READ, data_valid;
  wire   N32, N33, N34, N35, N36, N37, N38, N39, N40, N41, N42, N43, N44, N45,
         N46, N47, N48, N49, N50, N51, N52, N53, N54, N55, N56, N57, N58, N59,
         N60, N61, N62, N63, N64, N65, N66, N67, N68, N69, N70, N71, N72, N82,
         N83, N84, N85, N86, N87, N88, N89, N90, n268, n269, n270, n271, n272,
         n273, n274, n275, n276, n277, n278, n279, n280, n281, n282, n283,
         n285, n286, n287, n291, n293, n294, n295, n296, n297, n298, n299,
         n300, n301, n302, n303, n304, n305, n306, n307, n308, n309, n310,
         n311, n312, n313, n314, n315, n316, n318, n319, n352, n353, n354,
         n355, n357, n358, n360, n361, n362, n363, n364, n365, n366, n367,
         n368, n369, n370, n371, n374, n375, n376, n377, n378, n379, n380,
         n381, n382, n383, n384, n385, n386, n387, n388, n389, n390, n391,
         n392, n393, n394, n1, n3, n5, n7, n9, n11, n168, n220, n221, n222,
         n223, n224, n225, n226, n227, n228, n229, n230, n231, n232, n233,
         n234, n235, n236, n237, n238, n239, n240, n241, n242, n243, n244,
         n245, n246, n247, n248, n249, n250, n251, n252, n253, n254, n255,
         n256, n257, n258, n259, n260, n261, n262, n263, n264, n265, n266,
         n267, n284, n288, n289, n290, n292, n317, n320, n321, n322, n323,
         n324, n325, n326, n327, n328, n329, n330, n331, n332, n333, n334,
         n335, n336, n337, n338, n339, n340, n341, n342, n343, n344, n345,
         n346, n347, n348, n349, n350, n351, n356, n359, n372, n373, n395,
         n396, n397, n398, n399, n400, n401, n402, n403, n404, n405, n406,
         n407, n408, n409, n410, n411, n412, n413, n414, n415, n416, n417,
         n418, n419, n420, n421, n422, n423, n424, n425, n426, n427, n428,
         n429, n430, n431, n432, n433, n434, n435, n436, n437, n438, n439,
         n440, n441, n442, n443, n444, n445, n446;
  wire   [3:0] state;
  wire   [31:0] index;
  wire   [8:0] x;
  wire   [8:0] y;

  AN4P U111 ( .A(x[3]), .B(x[4]), .C(x[5]), .D(x[8]), .Z(n319) );
  AN4P U167 ( .A(n362), .B(n363), .C(n364), .D(n365), .Z(n361) );
  AO1P U178 ( .A(n446), .B(n356), .C(n293), .D(n371), .Z(n370) );
  OR2 U244 ( .A(n1), .B(addr[17]), .Z(n374) );
  OR3 U245 ( .A(state[1]), .B(state[2]), .C(n269), .Z(n291) );
  read_mem_20000_DW01_inc_0 add_309 ( .A(y), .SUM({N90, N89, N88, N87, N86, 
        N85, N84, N83, N82}) );
  read_mem_20000_DW01_inc_1 add_308 ( .A(x), .SUM({N72, N71, N70, N69, N68, 
        N67, N66, N65, N64}) );
  read_mem_20000_DW01_inc_2 add_307 ( .A(index), .SUM({N63, N62, N61, N60, N59, 
        N58, N57, N56, N55, N54, N53, N52, N51, N50, N49, N48, N47, N46, N45, 
        N44, N43, N42, N41, N40, N39, N38, N37, N36, N35, N34, N33, N32}) );
  FD1 \addr_reg[17]  ( .D(n374), .CP(clk), .Q(addr[17]) );
  FD2 READ_reg ( .D(n375), .CP(clk), .CD(n239), .Q(READ), .QN(n168) );
  FD1 \data_reg[63]  ( .D(n378), .CP(clk), .Q(data[63]), .QN(n270) );
  FD1 \data_reg[62]  ( .D(n379), .CP(clk), .Q(data[62]), .QN(n271) );
  FD1 \data_reg[61]  ( .D(n380), .CP(clk), .Q(data[61]), .QN(n272) );
  FD1 \data_reg[60]  ( .D(n381), .CP(clk), .Q(data[60]), .QN(n273) );
  FD1 \data_reg[59]  ( .D(n382), .CP(clk), .Q(data[59]), .QN(n274) );
  FD1 \data_reg[58]  ( .D(n383), .CP(clk), .Q(data[58]), .QN(n275) );
  FD1 \data_reg[57]  ( .D(n384), .CP(clk), .Q(data[57]), .QN(n276) );
  FD1 \data_reg[56]  ( .D(n385), .CP(clk), .Q(data[56]), .QN(n277) );
  FD1 \data_reg[55]  ( .D(n386), .CP(clk), .Q(data[55]), .QN(n278) );
  FD1 \data_reg[54]  ( .D(n387), .CP(clk), .Q(data[54]), .QN(n279) );
  FD1 \data_reg[53]  ( .D(n388), .CP(clk), .Q(data[53]), .QN(n280) );
  FD1 \data_reg[52]  ( .D(n389), .CP(clk), .Q(data[52]), .QN(n281) );
  FD1 \data_reg[51]  ( .D(n390), .CP(clk), .Q(data[51]), .QN(n282) );
  FD1 \data_reg[50]  ( .D(n391), .CP(clk), .Q(data[50]), .QN(n283) );
  FD1 \y_reg[8]  ( .D(n436), .CP(clk), .Q(y[8]) );
  FD1 \y_reg[7]  ( .D(n444), .CP(clk), .Q(y[7]) );
  FD1 \y_reg[6]  ( .D(n443), .CP(clk), .Q(y[6]) );
  FD1 \y_reg[5]  ( .D(n442), .CP(clk), .Q(y[5]) );
  FD2 data_valid_reg ( .D(n377), .CP(clk), .CD(n236), .Q(data_valid) );
  FD2 \index_reg[28]  ( .D(n421), .CP(clk), .CD(n238), .Q(index[28]) );
  FD1 \y_reg[4]  ( .D(n441), .CP(clk), .Q(y[4]) );
  FD2 \index_reg[27]  ( .D(n420), .CP(clk), .CD(n238), .Q(index[27]) );
  FD1 \y_reg[3]  ( .D(n440), .CP(clk), .Q(y[3]) );
  FD1 \x_reg[3]  ( .D(n432), .CP(clk), .Q(x[3]) );
  FD1 \x_reg[4]  ( .D(n431), .CP(clk), .Q(x[4]) );
  FD1 \x_reg[5]  ( .D(n430), .CP(clk), .Q(x[5]) );
  FD1 \x_reg[8]  ( .D(n427), .CP(clk), .Q(x[8]) );
  FD1 \x_reg[6]  ( .D(n429), .CP(clk), .Q(x[6]) );
  FD1 \x_reg[7]  ( .D(n428), .CP(clk), .Q(x[7]) );
  FD2 \index_reg[26]  ( .D(n419), .CP(clk), .CD(n238), .Q(index[26]) );
  FD1 \y_reg[2]  ( .D(n439), .CP(clk), .Q(y[2]) );
  FD1 \x_reg[2]  ( .D(n433), .CP(clk), .Q(x[2]) );
  FD2 \index_reg[25]  ( .D(n418), .CP(clk), .CD(n238), .Q(index[25]) );
  FD1 \y_reg[1]  ( .D(n438), .CP(clk), .Q(y[1]) );
  FD1 \x_reg[0]  ( .D(n376), .CP(clk), .Q(x[0]), .QN(n285) );
  FD1 \x_reg[1]  ( .D(n434), .CP(clk), .Q(x[1]) );
  FD2 \index_reg[24]  ( .D(n417), .CP(clk), .CD(n238), .Q(index[24]) );
  FD1 \y_reg[0]  ( .D(n437), .CP(clk), .Q(y[0]) );
  FD2 \index_reg[22]  ( .D(n415), .CP(clk), .CD(n238), .Q(index[22]) );
  FD2 \index_reg[23]  ( .D(n416), .CP(clk), .CD(n238), .Q(index[23]) );
  FD2 \state_reg[0]  ( .D(n394), .CP(clk), .CD(n236), .Q(state[0]), .QN(n269)
         );
  FD2 \index_reg[21]  ( .D(n414), .CP(clk), .CD(n238), .Q(index[21]) );
  FD2 \state_reg[1]  ( .D(n392), .CP(clk), .CD(n236), .Q(state[1]), .QN(n3) );
  FD2 \state_reg[2]  ( .D(n393), .CP(clk), .CD(n236), .Q(state[2]), .QN(n268)
         );
  FD2 \index_reg[20]  ( .D(n413), .CP(clk), .CD(n238), .Q(index[20]) );
  FD2 \index_reg[19]  ( .D(n412), .CP(clk), .CD(n237), .Q(index[19]) );
  FD2 \index_reg[18]  ( .D(n411), .CP(clk), .CD(n237), .Q(index[18]) );
  FD2 \index_reg[17]  ( .D(n410), .CP(clk), .CD(n237), .Q(index[17]) );
  FD2 \index_reg[16]  ( .D(n409), .CP(clk), .CD(n237), .Q(index[16]) );
  FD2 \index_reg[15]  ( .D(n408), .CP(clk), .CD(n237), .Q(index[15]) );
  FD2 \index_reg[14]  ( .D(n407), .CP(clk), .CD(n237), .Q(index[14]) );
  FD2 \index_reg[13]  ( .D(n406), .CP(clk), .CD(n237), .Q(index[13]), .QN(n286) );
  FD2 \index_reg[12]  ( .D(n405), .CP(clk), .CD(n237), .Q(index[12]), .QN(n287) );
  FD2 \index_reg[11]  ( .D(n404), .CP(clk), .CD(n237), .Q(index[11]) );
  FD2 \index_reg[10]  ( .D(n403), .CP(clk), .CD(n237), .Q(index[10]) );
  FD2 \index_reg[9]  ( .D(n402), .CP(clk), .CD(n237), .Q(index[9]) );
  FD2 \index_reg[8]  ( .D(n401), .CP(clk), .CD(n237), .Q(index[8]) );
  FD2 \index_reg[7]  ( .D(n400), .CP(clk), .CD(n236), .Q(index[7]) );
  FD2 \index_reg[6]  ( .D(n399), .CP(clk), .CD(n236), .Q(index[6]) );
  FD2 \index_reg[5]  ( .D(n398), .CP(clk), .CD(n236), .Q(index[5]) );
  FD2 \index_reg[4]  ( .D(n397), .CP(clk), .CD(n236), .Q(index[4]) );
  FD2 \index_reg[3]  ( .D(n396), .CP(clk), .CD(n236), .Q(index[3]) );
  FD2 \index_reg[2]  ( .D(n395), .CP(clk), .CD(n236), .Q(index[2]), .QN(n9) );
  FD2 \index_reg[1]  ( .D(n373), .CP(clk), .CD(n236), .Q(index[1]), .QN(n11)
         );
  FD2 \index_reg[0]  ( .D(n372), .CP(clk), .CD(n236), .Q(index[0]), .QN(n7) );
  FDS2L \addr_reg[0]  ( .CR(1'b1), .D(index[0]), .LD(n1), .CP(clk), .Q(addr[0]) );
  FDS2L \addr_reg[2]  ( .CR(1'b1), .D(index[2]), .LD(n1), .CP(clk), .Q(addr[2]) );
  FDS2L \addr_reg[3]  ( .CR(1'b1), .D(index[3]), .LD(n1), .CP(clk), .Q(addr[3]) );
  FDS2L \addr_reg[4]  ( .CR(1'b1), .D(index[4]), .LD(n1), .CP(clk), .Q(addr[4]) );
  FDS2L \addr_reg[5]  ( .CR(1'b1), .D(index[5]), .LD(n1), .CP(clk), .Q(addr[5]) );
  FDS2L \addr_reg[6]  ( .CR(1'b1), .D(index[6]), .LD(n1), .CP(clk), .Q(addr[6]) );
  FDS2L \addr_reg[7]  ( .CR(1'b1), .D(index[7]), .LD(n1), .CP(clk), .Q(addr[7]) );
  FDS2L \addr_reg[8]  ( .CR(1'b1), .D(index[8]), .LD(n1), .CP(clk), .Q(addr[8]) );
  FDS2L \addr_reg[9]  ( .CR(1'b1), .D(index[9]), .LD(n1), .CP(clk), .Q(addr[9]) );
  FDS2L \addr_reg[10]  ( .CR(1'b1), .D(index[10]), .LD(n1), .CP(clk), .Q(
        addr[10]) );
  FDS2L \addr_reg[11]  ( .CR(1'b1), .D(index[11]), .LD(n1), .CP(clk), .Q(
        addr[11]) );
  FDS2L \addr_reg[12]  ( .CR(1'b1), .D(index[12]), .LD(n1), .CP(clk), .Q(
        addr[12]) );
  FDS2L \addr_reg[13]  ( .CR(1'b1), .D(index[13]), .LD(n1), .CP(clk), .Q(
        addr[13]) );
  FDS2L \addr_reg[14]  ( .CR(1'b1), .D(index[14]), .LD(n1), .CP(clk), .Q(
        addr[14]) );
  FDS2L \addr_reg[15]  ( .CR(1'b1), .D(index[15]), .LD(n1), .CP(clk), .Q(
        addr[15]) );
  FDS2L \addr_reg[16]  ( .CR(1'b1), .D(index[16]), .LD(n1), .CP(clk), .Q(
        addr[16]) );
  FDS2L \addr_reg[18]  ( .CR(1'b1), .D(index[18]), .LD(n1), .CP(clk), .Q(
        addr[18]) );
  FDS2L \addr_reg[19]  ( .CR(1'b1), .D(index[19]), .LD(n1), .CP(clk), .Q(
        addr[19]) );
  FDS2L \addr_reg[20]  ( .CR(1'b1), .D(index[20]), .LD(n1), .CP(clk), .Q(
        addr[20]) );
  FDS2L \addr_reg[21]  ( .CR(1'b1), .D(index[21]), .LD(n1), .CP(clk), .Q(
        addr[21]) );
  FDS2L \addr_reg[22]  ( .CR(1'b1), .D(index[22]), .LD(n1), .CP(clk), .Q(
        addr[22]) );
  FDS2L \addr_reg[23]  ( .CR(1'b1), .D(index[23]), .LD(n1), .CP(clk), .Q(
        addr[23]) );
  FDS2L \addr_reg[24]  ( .CR(1'b1), .D(index[24]), .LD(n1), .CP(clk), .Q(
        addr[24]) );
  FDS2L \addr_reg[25]  ( .CR(1'b1), .D(index[25]), .LD(n1), .CP(clk), .Q(
        addr[25]) );
  FDS2L \addr_reg[26]  ( .CR(1'b1), .D(index[26]), .LD(n1), .CP(clk), .Q(
        addr[26]) );
  FDS2L \addr_reg[27]  ( .CR(1'b1), .D(index[27]), .LD(n1), .CP(clk), .Q(
        addr[27]) );
  FDS2L \addr_reg[28]  ( .CR(1'b1), .D(index[28]), .LD(n1), .CP(clk), .Q(
        addr[28]) );
  FDS2L \addr_reg[29]  ( .CR(1'b1), .D(index[29]), .LD(n1), .CP(clk), .Q(
        addr[29]) );
  FDS2L \addr_reg[30]  ( .CR(1'b1), .D(index[30]), .LD(n1), .CP(clk), .Q(
        addr[30]) );
  FDS2L \addr_reg[31]  ( .CR(1'b1), .D(index[31]), .LD(n1), .CP(clk), .Q(
        addr[31]) );
  FDS2L \addr_reg[1]  ( .CR(1'b1), .D(index[1]), .LD(n1), .CP(clk), .Q(addr[1]) );
  FDS2L \data_reg[49]  ( .CR(1'b1), .D(x[8]), .LD(n235), .CP(clk), .Q(data[49]) );
  FDS2L \data_reg[47]  ( .CR(1'b1), .D(x[6]), .LD(n235), .CP(clk), .Q(data[47]) );
  FDS2L \data_reg[45]  ( .CR(1'b1), .D(x[4]), .LD(n235), .CP(clk), .Q(data[45]) );
  FDS2L \data_reg[43]  ( .CR(1'b1), .D(x[2]), .LD(n235), .CP(clk), .Q(data[43]) );
  FDS2L \data_reg[41]  ( .CR(1'b1), .D(x[0]), .LD(n235), .CP(clk), .Q(data[41]) );
  FDS2L \data_reg[39]  ( .CR(1'b1), .D(y[7]), .LD(n235), .CP(clk), .Q(data[39]) );
  FDS2L \data_reg[37]  ( .CR(1'b1), .D(y[5]), .LD(n235), .CP(clk), .Q(data[37]) );
  FDS2L \data_reg[35]  ( .CR(1'b1), .D(y[3]), .LD(n233), .CP(clk), .Q(data[35]) );
  FDS2L \data_reg[33]  ( .CR(1'b1), .D(y[1]), .LD(n235), .CP(clk), .Q(data[33]) );
  FDS2L \data_reg[31]  ( .CR(1'b1), .D(datain[31]), .LD(n233), .CP(clk), .Q(
        data[31]) );
  FDS2L \data_reg[29]  ( .CR(1'b1), .D(datain[29]), .LD(n235), .CP(clk), .Q(
        data[29]) );
  FDS2L \data_reg[27]  ( .CR(1'b1), .D(datain[27]), .LD(n233), .CP(clk), .Q(
        data[27]) );
  FDS2L \data_reg[25]  ( .CR(1'b1), .D(datain[25]), .LD(n233), .CP(clk), .Q(
        data[25]) );
  FDS2L \data_reg[23]  ( .CR(1'b1), .D(datain[23]), .LD(n235), .CP(clk), .Q(
        data[23]) );
  FDS2L \data_reg[21]  ( .CR(1'b1), .D(datain[21]), .LD(n233), .CP(clk), .Q(
        data[21]) );
  FDS2L \data_reg[19]  ( .CR(1'b1), .D(datain[19]), .LD(n235), .CP(clk), .Q(
        data[19]) );
  FDS2L \data_reg[17]  ( .CR(1'b1), .D(datain[17]), .LD(n233), .CP(clk), .Q(
        data[17]) );
  FDS2L \data_reg[15]  ( .CR(1'b1), .D(datain[15]), .LD(n235), .CP(clk), .Q(
        data[15]) );
  FDS2L \data_reg[13]  ( .CR(1'b1), .D(datain[13]), .LD(n233), .CP(clk), .Q(
        data[13]) );
  FDS2L \data_reg[11]  ( .CR(1'b1), .D(datain[11]), .LD(n235), .CP(clk), .Q(
        data[11]) );
  FDS2L \data_reg[9]  ( .CR(1'b1), .D(datain[9]), .LD(n233), .CP(clk), .Q(
        data[9]) );
  FDS2L \data_reg[7]  ( .CR(1'b1), .D(datain[7]), .LD(n235), .CP(clk), .Q(
        data[7]) );
  FDS2L \data_reg[5]  ( .CR(1'b1), .D(datain[5]), .LD(n233), .CP(clk), .Q(
        data[5]) );
  FDS2L \data_reg[3]  ( .CR(1'b1), .D(datain[3]), .LD(n235), .CP(clk), .Q(
        data[3]) );
  FDS2L \data_reg[1]  ( .CR(1'b1), .D(datain[1]), .LD(n235), .CP(clk), .Q(
        data[1]) );
  FDS2L \data_reg[0]  ( .CR(1'b1), .D(datain[0]), .LD(n235), .CP(clk), .Q(
        data[0]) );
  FDS2L \data_reg[2]  ( .CR(1'b1), .D(datain[2]), .LD(n235), .CP(clk), .Q(
        data[2]) );
  FDS2L \data_reg[4]  ( .CR(1'b1), .D(datain[4]), .LD(n235), .CP(clk), .Q(
        data[4]) );
  FDS2L \data_reg[6]  ( .CR(1'b1), .D(datain[6]), .LD(n235), .CP(clk), .Q(
        data[6]) );
  FDS2L \data_reg[8]  ( .CR(1'b1), .D(datain[8]), .LD(n235), .CP(clk), .Q(
        data[8]) );
  FDS2L \data_reg[10]  ( .CR(1'b1), .D(datain[10]), .LD(n234), .CP(clk), .Q(
        data[10]) );
  FDS2L \data_reg[12]  ( .CR(1'b1), .D(datain[12]), .LD(n234), .CP(clk), .Q(
        data[12]) );
  FDS2L \data_reg[14]  ( .CR(1'b1), .D(datain[14]), .LD(n234), .CP(clk), .Q(
        data[14]) );
  FDS2L \data_reg[16]  ( .CR(1'b1), .D(datain[16]), .LD(n234), .CP(clk), .Q(
        data[16]) );
  FDS2L \data_reg[18]  ( .CR(1'b1), .D(datain[18]), .LD(n234), .CP(clk), .Q(
        data[18]) );
  FDS2L \data_reg[20]  ( .CR(1'b1), .D(datain[20]), .LD(n234), .CP(clk), .Q(
        data[20]) );
  FDS2L \data_reg[22]  ( .CR(1'b1), .D(datain[22]), .LD(n234), .CP(clk), .Q(
        data[22]) );
  FDS2L \data_reg[24]  ( .CR(1'b1), .D(datain[24]), .LD(n234), .CP(clk), .Q(
        data[24]) );
  FDS2L \data_reg[26]  ( .CR(1'b1), .D(datain[26]), .LD(n234), .CP(clk), .Q(
        data[26]) );
  FDS2L \data_reg[28]  ( .CR(1'b1), .D(datain[28]), .LD(n234), .CP(clk), .Q(
        data[28]) );
  FDS2L \data_reg[30]  ( .CR(1'b1), .D(datain[30]), .LD(n234), .CP(clk), .Q(
        data[30]) );
  FDS2L \data_reg[32]  ( .CR(1'b1), .D(y[0]), .LD(n234), .CP(clk), .Q(data[32]) );
  FDS2L \data_reg[34]  ( .CR(1'b1), .D(y[2]), .LD(n234), .CP(clk), .Q(data[34]) );
  FDS2L \data_reg[36]  ( .CR(1'b1), .D(y[4]), .LD(n234), .CP(clk), .Q(data[36]) );
  FDS2L \data_reg[38]  ( .CR(1'b1), .D(y[6]), .LD(n234), .CP(clk), .Q(data[38]) );
  FDS2L \data_reg[40]  ( .CR(1'b1), .D(y[8]), .LD(n234), .CP(clk), .Q(data[40]) );
  FDS2L \data_reg[42]  ( .CR(1'b1), .D(x[1]), .LD(n234), .CP(clk), .Q(data[42]) );
  FDS2L \data_reg[44]  ( .CR(1'b1), .D(x[3]), .LD(n234), .CP(clk), .Q(data[44]) );
  FDS2L \data_reg[46]  ( .CR(1'b1), .D(x[5]), .LD(n234), .CP(clk), .Q(data[46]) );
  FDS2L \data_reg[48]  ( .CR(1'b1), .D(x[7]), .LD(n234), .CP(clk), .Q(data[48]) );
  FD2 \index_reg[29]  ( .D(n422), .CP(clk), .CD(rst_n), .Q(index[29]) );
  FD2 \index_reg[30]  ( .D(n423), .CP(clk), .CD(rst_n), .Q(index[30]) );
  FD2 \index_reg[31]  ( .D(n424), .CP(clk), .CD(rst_n), .Q(index[31]) );
  NR2 U84 ( .A(n240), .B(n291), .Z(n1) );
  OR2P U85 ( .A(n240), .B(n352), .Z(n5) );
  IVP U86 ( .A(n229), .Z(n226) );
  IVDA U87 ( .A(n296), .Z(n222) );
  IVDA U88 ( .A(n296), .Z(n223) );
  IVP U89 ( .A(n306), .Z(n435) );
  IVP U90 ( .A(n232), .Z(n230) );
  IVDA U91 ( .A(n309), .Z(n220) );
  IVDA U92 ( .A(n309), .Z(n221) );
  IVP U93 ( .A(n370), .Z(n426) );
  IVP U94 ( .A(n229), .Z(n227) );
  IVP U95 ( .A(n5), .Z(n234) );
  IVP U96 ( .A(n232), .Z(n231) );
  IVP U97 ( .A(n5), .Z(n233) );
  NR2 U98 ( .A(n226), .B(n240), .Z(n306) );
  AO7 U99 ( .A(n445), .B(n305), .C(n306), .Z(n296) );
  IVP U100 ( .A(n307), .Z(n445) );
  IVP U101 ( .A(n224), .Z(n225) );
  IVP U102 ( .A(n295), .Z(n224) );
  NR2 U103 ( .A(n222), .B(n305), .Z(n295) );
  IVP U104 ( .A(valid_n), .Z(n446) );
  ND2 U105 ( .A(n305), .B(n352), .Z(n371) );
  ND2 U106 ( .A(n291), .B(n355), .Z(n293) );
  AN3 U107 ( .A(n239), .B(n307), .C(n230), .Z(n309) );
  ND2 U108 ( .A(index[30]), .B(n228), .Z(n245) );
  ND2 U109 ( .A(n243), .B(n242), .Z(n424) );
  ND2 U110 ( .A(index[31]), .B(n228), .Z(n243) );
  ND2 U112 ( .A(index[29]), .B(n228), .Z(n247) );
  ND2 U113 ( .A(index[28]), .B(n228), .Z(n249) );
  ND2 U114 ( .A(index[27]), .B(n228), .Z(n251) );
  ND2 U115 ( .A(index[26]), .B(n228), .Z(n253) );
  ND2 U116 ( .A(index[25]), .B(n227), .Z(n255) );
  ND2 U117 ( .A(index[24]), .B(n227), .Z(n257) );
  ND2 U118 ( .A(index[23]), .B(n227), .Z(n259) );
  ND2 U119 ( .A(index[22]), .B(n227), .Z(n261) );
  ND2 U120 ( .A(index[21]), .B(n227), .Z(n263) );
  ND2 U121 ( .A(index[20]), .B(n227), .Z(n265) );
  ND2 U122 ( .A(index[19]), .B(n227), .Z(n267) );
  ND2 U123 ( .A(index[18]), .B(n227), .Z(n288) );
  ND2 U124 ( .A(index[17]), .B(n227), .Z(n290) );
  ND2 U125 ( .A(index[16]), .B(n227), .Z(n317) );
  ND2 U126 ( .A(index[15]), .B(n227), .Z(n321) );
  ND2 U127 ( .A(index[14]), .B(n227), .Z(n323) );
  ND2 U128 ( .A(index[13]), .B(n226), .Z(n325) );
  ND2 U129 ( .A(index[12]), .B(n226), .Z(n327) );
  AO7 U130 ( .A(n350), .B(state[1]), .C(n305), .Z(n349) );
  ND2 U131 ( .A(n3), .B(n241), .Z(n305) );
  NR2 U132 ( .A(state[0]), .B(n268), .Z(n241) );
  ND2 U133 ( .A(index[11]), .B(n226), .Z(n329) );
  IVP U134 ( .A(n294), .Z(n444) );
  AO2 U135 ( .A(N89), .B(n225), .C(y[7]), .D(n223), .Z(n294) );
  IVP U136 ( .A(n297), .Z(n443) );
  AO2 U137 ( .A(N88), .B(n225), .C(y[6]), .D(n222), .Z(n297) );
  IVP U138 ( .A(n298), .Z(n442) );
  AO2 U139 ( .A(N87), .B(n225), .C(y[5]), .D(n223), .Z(n298) );
  IVP U140 ( .A(n299), .Z(n441) );
  AO2 U141 ( .A(N86), .B(n225), .C(y[4]), .D(n222), .Z(n299) );
  IVP U142 ( .A(n300), .Z(n440) );
  AO2 U143 ( .A(N85), .B(n225), .C(y[3]), .D(n223), .Z(n300) );
  IVP U144 ( .A(n301), .Z(n439) );
  AO2 U145 ( .A(N84), .B(n225), .C(y[2]), .D(n222), .Z(n301) );
  IVP U146 ( .A(n302), .Z(n438) );
  AO2 U147 ( .A(N83), .B(n225), .C(y[1]), .D(n223), .Z(n302) );
  IVP U148 ( .A(n303), .Z(n437) );
  AO2 U149 ( .A(N82), .B(n225), .C(y[0]), .D(n222), .Z(n303) );
  IVP U150 ( .A(n304), .Z(n436) );
  AO2 U151 ( .A(N90), .B(n225), .C(y[8]), .D(n223), .Z(n304) );
  ND2 U152 ( .A(index[10]), .B(n226), .Z(n331) );
  ND2 U153 ( .A(index[9]), .B(n226), .Z(n333) );
  ND2 U154 ( .A(index[8]), .B(n226), .Z(n335) );
  IVP U155 ( .A(n308), .Z(n427) );
  AO2 U156 ( .A(n435), .B(x[8]), .C(N72), .D(n221), .Z(n308) );
  IVP U157 ( .A(n310), .Z(n428) );
  AO2 U158 ( .A(n435), .B(x[7]), .C(N71), .D(n220), .Z(n310) );
  IVP U159 ( .A(n311), .Z(n429) );
  AO2 U160 ( .A(n435), .B(x[6]), .C(N70), .D(n221), .Z(n311) );
  IVP U161 ( .A(n312), .Z(n430) );
  AO2 U162 ( .A(n435), .B(x[5]), .C(N69), .D(n220), .Z(n312) );
  IVP U163 ( .A(n313), .Z(n431) );
  AO2 U164 ( .A(n435), .B(x[4]), .C(N68), .D(n221), .Z(n313) );
  IVP U165 ( .A(n314), .Z(n432) );
  AO2 U166 ( .A(n435), .B(x[3]), .C(N67), .D(n220), .Z(n314) );
  IVP U168 ( .A(n315), .Z(n433) );
  AO2 U169 ( .A(n435), .B(x[2]), .C(N66), .D(n221), .Z(n315) );
  IVP U170 ( .A(n316), .Z(n434) );
  AO2 U171 ( .A(n435), .B(x[1]), .C(N65), .D(n220), .Z(n316) );
  ND2 U172 ( .A(index[7]), .B(n226), .Z(n337) );
  EON1 U173 ( .A(n306), .B(n285), .C(N64), .D(n220), .Z(n376) );
  ND4 U174 ( .A(x[2]), .B(x[1]), .C(n318), .D(n319), .Z(n307) );
  NR3 U175 ( .A(x[6]), .B(x[7]), .C(n285), .Z(n318) );
  ND2 U176 ( .A(state[1]), .B(n356), .Z(n355) );
  ND2 U177 ( .A(index[6]), .B(n226), .Z(n339) );
  AO4 U179 ( .A(n352), .B(n370), .C(n426), .D(n268), .Z(n393) );
  AO4 U180 ( .A(n426), .B(n269), .C(n357), .D(n370), .Z(n394) );
  NR2 U181 ( .A(n358), .B(n356), .Z(n357) );
  AO6 U182 ( .A(n360), .B(n361), .C(n305), .Z(n358) );
  AO3 U183 ( .A(n291), .B(n370), .C(n354), .D(n355), .Z(n392) );
  ND2 U184 ( .A(state[1]), .B(n370), .Z(n354) );
  ND3 U185 ( .A(state[0]), .B(n268), .C(state[1]), .Z(n352) );
  ND2 U186 ( .A(index[5]), .B(n226), .Z(n341) );
  ND2 U187 ( .A(index[4]), .B(n226), .Z(n343) );
  ND2 U188 ( .A(index[3]), .B(n226), .Z(n345) );
  AO7 U189 ( .A(n349), .B(n9), .C(n346), .Z(n395) );
  AO7 U190 ( .A(n349), .B(n11), .C(n347), .Z(n373) );
  ND2 U191 ( .A(N33), .B(n230), .Z(n347) );
  AO7 U192 ( .A(n349), .B(n7), .C(n348), .Z(n372) );
  ND2 U193 ( .A(N32), .B(n230), .Z(n348) );
  NR4 U194 ( .A(n366), .B(n367), .C(n368), .D(n369), .Z(n360) );
  ND4 U195 ( .A(index[8]), .B(index[9]), .C(index[10]), .D(index[11]), .Z(n367) );
  ND4 U196 ( .A(index[4]), .B(index[5]), .C(index[6]), .D(index[7]), .Z(n368)
         );
  ND4 U197 ( .A(index[15]), .B(index[16]), .C(n287), .D(n286), .Z(n366) );
  NR4 U198 ( .A(index[14]), .B(index[17]), .C(index[18]), .D(index[19]), .Z(
        n365) );
  NR4 U199 ( .A(index[20]), .B(index[21]), .C(index[22]), .D(index[23]), .Z(
        n364) );
  NR4 U200 ( .A(index[24]), .B(index[25]), .C(index[26]), .D(index[27]), .Z(
        n363) );
  NR4 U201 ( .A(index[28]), .B(index[29]), .C(index[30]), .D(index[31]), .Z(
        n362) );
  ND4 U202 ( .A(index[0]), .B(index[1]), .C(index[2]), .D(index[3]), .Z(n369)
         );
  AO4 U203 ( .A(n291), .B(n425), .C(n168), .D(n293), .Z(n375) );
  IVP U204 ( .A(n293), .Z(n425) );
  NR2 U205 ( .A(n283), .B(n234), .Z(n391) );
  NR2 U206 ( .A(n282), .B(n234), .Z(n390) );
  NR2 U207 ( .A(n281), .B(n233), .Z(n389) );
  NR2 U208 ( .A(n280), .B(n233), .Z(n388) );
  NR2 U209 ( .A(n279), .B(n233), .Z(n387) );
  NR2 U210 ( .A(n278), .B(n233), .Z(n386) );
  NR2 U211 ( .A(n277), .B(n233), .Z(n385) );
  NR2 U212 ( .A(n276), .B(n233), .Z(n384) );
  NR2 U213 ( .A(n275), .B(n233), .Z(n383) );
  NR2 U214 ( .A(n274), .B(n233), .Z(n382) );
  NR2 U215 ( .A(n273), .B(n233), .Z(n381) );
  NR2 U216 ( .A(n272), .B(n233), .Z(n380) );
  NR2 U217 ( .A(n271), .B(n233), .Z(n379) );
  NR2 U218 ( .A(n270), .B(n233), .Z(n378) );
  ND2 U219 ( .A(n352), .B(n353), .Z(n377) );
  ND2 U220 ( .A(n305), .B(data_valid), .Z(n353) );
  IVA U221 ( .A(n229), .Z(n228) );
  IVA U222 ( .A(n351), .Z(n229) );
  IVA U223 ( .A(n359), .Z(n232) );
  IVA U224 ( .A(n5), .Z(n235) );
  IVA U225 ( .A(n240), .Z(n236) );
  IVA U226 ( .A(n240), .Z(n237) );
  IVA U227 ( .A(n240), .Z(n238) );
  IVA U228 ( .A(n240), .Z(n239) );
  IVP U229 ( .A(rst_n), .Z(n240) );
  OR2 U230 ( .A(state[0]), .B(state[2]), .Z(n350) );
  IVA U231 ( .A(n349), .Z(n351) );
  IVA U232 ( .A(n305), .Z(n359) );
  ND2 U233 ( .A(N63), .B(n230), .Z(n242) );
  ND2 U234 ( .A(N62), .B(n230), .Z(n244) );
  ND2 U235 ( .A(n245), .B(n244), .Z(n423) );
  ND2 U236 ( .A(N61), .B(n230), .Z(n246) );
  ND2 U237 ( .A(n247), .B(n246), .Z(n422) );
  ND2 U238 ( .A(N60), .B(n230), .Z(n248) );
  ND2 U239 ( .A(n249), .B(n248), .Z(n421) );
  ND2 U240 ( .A(N59), .B(n230), .Z(n250) );
  ND2 U241 ( .A(n251), .B(n250), .Z(n420) );
  ND2 U242 ( .A(N58), .B(n230), .Z(n252) );
  ND2 U243 ( .A(n253), .B(n252), .Z(n419) );
  ND2 U246 ( .A(N57), .B(n230), .Z(n254) );
  ND2 U247 ( .A(n255), .B(n254), .Z(n418) );
  ND2 U248 ( .A(N56), .B(n230), .Z(n256) );
  ND2 U249 ( .A(n257), .B(n256), .Z(n417) );
  ND2 U250 ( .A(N55), .B(n230), .Z(n258) );
  ND2 U251 ( .A(n259), .B(n258), .Z(n416) );
  ND2 U252 ( .A(N54), .B(n230), .Z(n260) );
  ND2 U253 ( .A(n261), .B(n260), .Z(n415) );
  ND2 U254 ( .A(N53), .B(n230), .Z(n262) );
  ND2 U255 ( .A(n263), .B(n262), .Z(n414) );
  ND2 U256 ( .A(N52), .B(n230), .Z(n264) );
  ND2 U257 ( .A(n265), .B(n264), .Z(n413) );
  ND2 U258 ( .A(N51), .B(n230), .Z(n266) );
  ND2 U259 ( .A(n267), .B(n266), .Z(n412) );
  ND2 U260 ( .A(N50), .B(n230), .Z(n284) );
  ND2 U261 ( .A(n288), .B(n284), .Z(n411) );
  ND2 U262 ( .A(N49), .B(n230), .Z(n289) );
  ND2 U263 ( .A(n290), .B(n289), .Z(n410) );
  ND2 U264 ( .A(N48), .B(n230), .Z(n292) );
  ND2 U265 ( .A(n317), .B(n292), .Z(n409) );
  ND2 U266 ( .A(N47), .B(n230), .Z(n320) );
  ND2 U267 ( .A(n321), .B(n320), .Z(n408) );
  ND2 U268 ( .A(N46), .B(n230), .Z(n322) );
  ND2 U269 ( .A(n323), .B(n322), .Z(n407) );
  ND2 U270 ( .A(N45), .B(n230), .Z(n324) );
  ND2 U271 ( .A(n325), .B(n324), .Z(n406) );
  ND2 U272 ( .A(N44), .B(n231), .Z(n326) );
  ND2 U273 ( .A(n327), .B(n326), .Z(n405) );
  ND2 U274 ( .A(N43), .B(n231), .Z(n328) );
  ND2 U275 ( .A(n329), .B(n328), .Z(n404) );
  ND2 U276 ( .A(N42), .B(n231), .Z(n330) );
  ND2 U277 ( .A(n331), .B(n330), .Z(n403) );
  ND2 U278 ( .A(N41), .B(n231), .Z(n332) );
  ND2 U279 ( .A(n333), .B(n332), .Z(n402) );
  ND2 U280 ( .A(N40), .B(n231), .Z(n334) );
  ND2 U281 ( .A(n335), .B(n334), .Z(n401) );
  ND2 U282 ( .A(N39), .B(n231), .Z(n336) );
  ND2 U283 ( .A(n337), .B(n336), .Z(n400) );
  ND2 U284 ( .A(N38), .B(n231), .Z(n338) );
  ND2 U285 ( .A(n339), .B(n338), .Z(n399) );
  ND2 U286 ( .A(N37), .B(n231), .Z(n340) );
  ND2 U287 ( .A(n341), .B(n340), .Z(n398) );
  ND2 U288 ( .A(N36), .B(n231), .Z(n342) );
  ND2 U289 ( .A(n343), .B(n342), .Z(n397) );
  ND2 U290 ( .A(N35), .B(n231), .Z(n344) );
  ND2 U291 ( .A(n345), .B(n344), .Z(n396) );
  ND2 U292 ( .A(N34), .B(n231), .Z(n346) );
  IVA U293 ( .A(n350), .Z(n356) );
endmodule


module write_mem_scratch_20000 ( clk, valid_n, data, valid, WRITE, addr, 
        datain, pixel_count );
  input [63:0] data;
  output [31:0] addr;
  output [31:0] datain;
  output [31:0] pixel_count;
  input clk, valid_n, valid;
  output WRITE;
  wire   N21, N22, N23, N24, N25, N26, N27, N28, N29, N30, N31, N32, N33, N34,
         N35, N36, N37, N38, N41, N42, N43, N44, N45, N46, N47, N48, N49, N50,
         N51, N52, N53, N54, N55, N56, N57, N58, N59, N60, N61, N62, N63, N64,
         N65, N66, N67, N68, N69, N70, N71, N72, n117, n118, n119, n121, n122,
         n123, n124, n125, n126, n127, n128, n129, n130, n131, n132, n133,
         n134, n135, n136, n137, n138, n139, n140, n141, n142, n143, n144,
         n145, n146, n147, n148, n149, n150, n151, n152, n153, n154, n155,
         n156, n157, n158, n159, n160, n161, n162, n163, N9, N19, N18, N17,
         N16, N15, N14, N13, N12, N11, N10, \add_67/carry[17] ,
         \add_67/carry[16] , \add_67/carry[15] , \add_67/carry[14] ,
         \add_67/carry[13] , \add_67/carry[12] , \add_67/carry[11] ,
         \add_67/carry[10] , \add_67/carry[9] , \add_67/carry[8] ,
         \add_67/carry[7] , \mult_67/A2[14] , \mult_67/CARRYB[2][6] ,
         \mult_67/CARRYB[3][6] , \mult_67/CARRYB[4][6] ,
         \mult_67/CARRYB[5][6] , \mult_67/CARRYB[6][6] ,
         \mult_67/CARRYB[7][6] , \mult_67/CARRYB[8][6] , n2, n3, n83, n84, n85,
         n86, n87, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100,
         n101, n102, n103, n104, n105, n106, n107, n108, n109, n110, n111,
         n112, n113, n114, n115, n116, n120, n164, n165, n166, n167;
  assign addr[31] = 1'b0;
  assign addr[30] = 1'b0;
  assign addr[29] = 1'b0;
  assign addr[28] = 1'b0;
  assign addr[27] = 1'b0;
  assign addr[26] = 1'b0;
  assign addr[25] = 1'b0;
  assign addr[24] = 1'b0;
  assign addr[23] = 1'b0;
  assign addr[22] = 1'b0;
  assign addr[21] = 1'b0;
  assign addr[20] = 1'b0;
  assign addr[19] = 1'b0;
  assign addr[17] = 1'b1;
  assign N21 = data[41];
  assign N22 = data[42];
  assign N23 = data[43];
  assign N24 = data[44];
  assign N25 = data[45];
  assign N26 = data[46];
  assign N9 = data[32];
  assign N10 = data[33];

  AN4P U55 ( .A(n160), .B(n161), .C(n162), .D(n163), .Z(n153) );
  FD1P \pixel_count_reg[0]  ( .D(n90), .CP(clk), .Q(pixel_count[0]) );
  FD1P \pixel_count_reg[1]  ( .D(n167), .CP(clk), .Q(pixel_count[1]) );
  write_mem_scratch_20000_DW01_inc_0 add_75 ( .A(pixel_count), .SUM({N72, N71, 
        N70, N69, N68, N67, N66, N65, N64, N63, N62, N61, N60, N59, N58, N57, 
        N56, N55, N54, N53, N52, N51, N50, N49, N48, N47, N46, N45, N44, N43, 
        N42, N41}) );
  FD1 \pixel_count_reg[26]  ( .D(n96), .CP(clk), .Q(pixel_count[26]) );
  FD1 \pixel_count_reg[27]  ( .D(n95), .CP(clk), .Q(pixel_count[27]) );
  FD1 \pixel_count_reg[28]  ( .D(n94), .CP(clk), .Q(pixel_count[28]) );
  FD1 \pixel_count_reg[25]  ( .D(n97), .CP(clk), .Q(pixel_count[25]) );
  FD1 \pixel_count_reg[24]  ( .D(n98), .CP(clk), .Q(pixel_count[24]) );
  FD1 \pixel_count_reg[23]  ( .D(n99), .CP(clk), .Q(pixel_count[23]) );
  FD1 \pixel_count_reg[22]  ( .D(n100), .CP(clk), .Q(pixel_count[22]) );
  FD1 \pixel_count_reg[21]  ( .D(n101), .CP(clk), .Q(pixel_count[21]) );
  FD1 \pixel_count_reg[20]  ( .D(n102), .CP(clk), .Q(pixel_count[20]) );
  FD1 \pixel_count_reg[19]  ( .D(n103), .CP(clk), .Q(pixel_count[19]) );
  FD1 \pixel_count_reg[18]  ( .D(n104), .CP(clk), .Q(pixel_count[18]) );
  FD1 \pixel_count_reg[17]  ( .D(n105), .CP(clk), .Q(pixel_count[17]) );
  FD1 \pixel_count_reg[16]  ( .D(n106), .CP(clk), .Q(pixel_count[16]) );
  FD1 \pixel_count_reg[15]  ( .D(n107), .CP(clk), .Q(pixel_count[15]) );
  FD1 \pixel_count_reg[13]  ( .D(n109), .CP(clk), .Q(pixel_count[13]), .QN(
        n117) );
  FD1 \pixel_count_reg[14]  ( .D(n108), .CP(clk), .Q(pixel_count[14]) );
  FD1 \pixel_count_reg[12]  ( .D(n110), .CP(clk), .Q(pixel_count[12]), .QN(
        n118) );
  FD1 \pixel_count_reg[11]  ( .D(n111), .CP(clk), .Q(pixel_count[11]) );
  FD1 \pixel_count_reg[10]  ( .D(n112), .CP(clk), .Q(pixel_count[10]) );
  FD1 \pixel_count_reg[9]  ( .D(n113), .CP(clk), .Q(pixel_count[9]) );
  FD1 \pixel_count_reg[8]  ( .D(n114), .CP(clk), .Q(pixel_count[8]) );
  FD1 \pixel_count_reg[7]  ( .D(n115), .CP(clk), .Q(pixel_count[7]) );
  FD1 \pixel_count_reg[6]  ( .D(n116), .CP(clk), .Q(pixel_count[6]) );
  FD1 \pixel_count_reg[5]  ( .D(n120), .CP(clk), .Q(pixel_count[5]) );
  FA1A \add_67/U1_8  ( .A(data[49]), .B(N11), .CI(\add_67/carry[8] ), .CO(
        \add_67/carry[9] ), .S(N29) );
  FA1A \add_67/U1_7  ( .A(data[48]), .B(N10), .CI(\add_67/carry[7] ), .CO(
        \add_67/carry[8] ), .S(N28) );
  FA1A \mult_67/S2_7_6  ( .A(data[39]), .B(\mult_67/CARRYB[6][6] ), .CI(
        data[37]), .CO(\mult_67/CARRYB[7][6] ), .S(N16) );
  FA1A \mult_67/S2_6_6  ( .A(data[38]), .B(\mult_67/CARRYB[5][6] ), .CI(
        data[36]), .CO(\mult_67/CARRYB[6][6] ), .S(N15) );
  FA1A \mult_67/S2_5_6  ( .A(data[37]), .B(\mult_67/CARRYB[4][6] ), .CI(
        data[35]), .CO(\mult_67/CARRYB[5][6] ), .S(N14) );
  FA1A \mult_67/S2_4_6  ( .A(data[36]), .B(\mult_67/CARRYB[3][6] ), .CI(
        data[34]), .CO(\mult_67/CARRYB[4][6] ), .S(N13) );
  FA1A \mult_67/S2_3_6  ( .A(data[35]), .B(\mult_67/CARRYB[2][6] ), .CI(N10), 
        .CO(\mult_67/CARRYB[3][6] ), .S(N12) );
  FA1A \mult_67/S4_6  ( .A(data[40]), .B(\mult_67/CARRYB[7][6] ), .CI(data[38]), .CO(\mult_67/CARRYB[8][6] ), .S(N17) );
  FD1 \addr_reg[8]  ( .D(N29), .CP(clk), .Q(addr[8]) );
  FD1 \addr_reg[7]  ( .D(N28), .CP(clk), .Q(addr[7]) );
  FD1 \addr_reg[6]  ( .D(N27), .CP(clk), .Q(addr[6]) );
  FD1 \addr_reg[18]  ( .D(N38), .CP(clk), .Q(addr[18]) );
  FD1 \addr_reg[16]  ( .D(N37), .CP(clk), .Q(addr[16]) );
  FD1 \addr_reg[15]  ( .D(N36), .CP(clk), .Q(addr[15]) );
  FD1 \addr_reg[14]  ( .D(N35), .CP(clk), .Q(addr[14]) );
  FD1 \addr_reg[13]  ( .D(N34), .CP(clk), .Q(addr[13]) );
  FD1 \addr_reg[12]  ( .D(N33), .CP(clk), .Q(addr[12]) );
  FD1 \addr_reg[11]  ( .D(N32), .CP(clk), .Q(addr[11]) );
  FD1 \addr_reg[10]  ( .D(N31), .CP(clk), .Q(addr[10]) );
  FD1 \addr_reg[9]  ( .D(N30), .CP(clk), .Q(addr[9]) );
  FD1 WRITE_reg ( .D(valid), .CP(clk), .Q(WRITE) );
  FD1 \addr_reg[5]  ( .D(N26), .CP(clk), .Q(addr[5]) );
  FD1 \addr_reg[4]  ( .D(N25), .CP(clk), .Q(addr[4]) );
  FD1 \addr_reg[3]  ( .D(N24), .CP(clk), .Q(addr[3]) );
  FD1 \addr_reg[2]  ( .D(N23), .CP(clk), .Q(addr[2]) );
  FD1 \addr_reg[1]  ( .D(N22), .CP(clk), .Q(addr[1]) );
  FD1 \addr_reg[0]  ( .D(N21), .CP(clk), .Q(addr[0]) );
  FD1 \datain_reg[31]  ( .D(data[31]), .CP(clk), .Q(datain[31]) );
  FD1 \datain_reg[30]  ( .D(data[30]), .CP(clk), .Q(datain[30]) );
  FD1 \datain_reg[29]  ( .D(data[29]), .CP(clk), .Q(datain[29]) );
  FD1 \datain_reg[28]  ( .D(data[28]), .CP(clk), .Q(datain[28]) );
  FD1 \datain_reg[27]  ( .D(data[27]), .CP(clk), .Q(datain[27]) );
  FD1 \datain_reg[26]  ( .D(data[26]), .CP(clk), .Q(datain[26]) );
  FD1 \datain_reg[25]  ( .D(data[25]), .CP(clk), .Q(datain[25]) );
  FD1 \datain_reg[24]  ( .D(data[24]), .CP(clk), .Q(datain[24]) );
  FD1 \datain_reg[23]  ( .D(data[23]), .CP(clk), .Q(datain[23]) );
  FD1 \datain_reg[22]  ( .D(data[22]), .CP(clk), .Q(datain[22]) );
  FD1 \datain_reg[21]  ( .D(data[21]), .CP(clk), .Q(datain[21]) );
  FD1 \datain_reg[20]  ( .D(data[20]), .CP(clk), .Q(datain[20]) );
  FD1 \datain_reg[19]  ( .D(data[19]), .CP(clk), .Q(datain[19]) );
  FD1 \datain_reg[18]  ( .D(data[18]), .CP(clk), .Q(datain[18]) );
  FD1 \datain_reg[17]  ( .D(data[17]), .CP(clk), .Q(datain[17]) );
  FD1 \datain_reg[16]  ( .D(data[16]), .CP(clk), .Q(datain[16]) );
  FD1 \datain_reg[15]  ( .D(data[15]), .CP(clk), .Q(datain[15]) );
  FD1 \datain_reg[14]  ( .D(data[14]), .CP(clk), .Q(datain[14]) );
  FD1 \datain_reg[13]  ( .D(data[13]), .CP(clk), .Q(datain[13]) );
  FD1 \datain_reg[12]  ( .D(data[12]), .CP(clk), .Q(datain[12]) );
  FD1 \datain_reg[11]  ( .D(data[11]), .CP(clk), .Q(datain[11]) );
  FD1 \datain_reg[10]  ( .D(data[10]), .CP(clk), .Q(datain[10]) );
  FD1 \datain_reg[9]  ( .D(data[9]), .CP(clk), .Q(datain[9]) );
  FD1 \datain_reg[8]  ( .D(data[8]), .CP(clk), .Q(datain[8]) );
  FD1 \datain_reg[7]  ( .D(data[7]), .CP(clk), .Q(datain[7]) );
  FD1 \datain_reg[6]  ( .D(data[6]), .CP(clk), .Q(datain[6]) );
  FD1 \datain_reg[5]  ( .D(data[5]), .CP(clk), .Q(datain[5]) );
  FD1 \datain_reg[4]  ( .D(data[4]), .CP(clk), .Q(datain[4]) );
  FD1 \datain_reg[3]  ( .D(data[3]), .CP(clk), .Q(datain[3]) );
  FD1 \datain_reg[2]  ( .D(data[2]), .CP(clk), .Q(datain[2]) );
  FD1 \datain_reg[1]  ( .D(data[1]), .CP(clk), .Q(datain[1]) );
  FD1 \datain_reg[0]  ( .D(data[0]), .CP(clk), .Q(datain[0]) );
  FD1 \pixel_count_reg[3]  ( .D(n165), .CP(clk), .Q(pixel_count[3]) );
  FD1 \pixel_count_reg[29]  ( .D(n93), .CP(clk), .Q(pixel_count[29]) );
  FD1 \pixel_count_reg[2]  ( .D(n166), .CP(clk), .Q(pixel_count[2]) );
  FD1 \pixel_count_reg[30]  ( .D(n92), .CP(clk), .Q(pixel_count[30]) );
  FD1 \pixel_count_reg[31]  ( .D(n91), .CP(clk), .Q(pixel_count[31]) );
  FD1 \pixel_count_reg[4]  ( .D(n164), .CP(clk), .Q(pixel_count[4]) );
  AN2P U17 ( .A(valid_n), .B(n155), .Z(n2) );
  AN2P U18 ( .A(\mult_67/A2[14] ), .B(data[40]), .Z(n3) );
  AO2P U19 ( .A(n2), .B(pixel_count[31]), .C(N72), .D(n83), .Z(n119) );
  IV U20 ( .A(n119), .Z(n91) );
  IV U21 ( .A(n123), .Z(n93) );
  IVDA U22 ( .A(n124), .Y(n94) );
  IVP U23 ( .A(n86), .Z(n83) );
  IVP U24 ( .A(n86), .Z(n84) );
  AO6 U25 ( .A(n153), .B(n154), .C(n155), .Z(n121) );
  NR4 U26 ( .A(n156), .B(n157), .C(n158), .D(n159), .Z(n154) );
  IVP U27 ( .A(n122), .Z(n92) );
  AO2 U28 ( .A(n2), .B(pixel_count[30]), .C(N71), .D(n83), .Z(n122) );
  AO2 U29 ( .A(n2), .B(pixel_count[29]), .C(N70), .D(n83), .Z(n123) );
  AO2 U30 ( .A(n2), .B(pixel_count[28]), .C(N69), .D(n83), .Z(n124) );
  IVP U31 ( .A(n125), .Z(n95) );
  AO2 U32 ( .A(n2), .B(pixel_count[27]), .C(N68), .D(n83), .Z(n125) );
  IVP U33 ( .A(n126), .Z(n96) );
  AO2 U34 ( .A(n2), .B(pixel_count[26]), .C(N67), .D(n83), .Z(n126) );
  IVP U35 ( .A(n127), .Z(n97) );
  AO2 U36 ( .A(n2), .B(pixel_count[25]), .C(N66), .D(n83), .Z(n127) );
  IVP U37 ( .A(n128), .Z(n98) );
  AO2 U38 ( .A(n2), .B(pixel_count[24]), .C(N65), .D(n83), .Z(n128) );
  IVP U39 ( .A(n129), .Z(n99) );
  AO2 U40 ( .A(n2), .B(pixel_count[23]), .C(N64), .D(n83), .Z(n129) );
  IVP U41 ( .A(n130), .Z(n100) );
  AO2 U42 ( .A(n2), .B(pixel_count[22]), .C(N63), .D(n83), .Z(n130) );
  IVP U43 ( .A(n131), .Z(n101) );
  AO2 U44 ( .A(n2), .B(pixel_count[21]), .C(N62), .D(n83), .Z(n131) );
  IVP U45 ( .A(n132), .Z(n102) );
  AO2 U46 ( .A(n2), .B(pixel_count[20]), .C(N61), .D(n83), .Z(n132) );
  IVP U47 ( .A(n133), .Z(n103) );
  AO2 U48 ( .A(n2), .B(pixel_count[19]), .C(N60), .D(n84), .Z(n133) );
  IVP U49 ( .A(n134), .Z(n104) );
  AO2 U50 ( .A(n2), .B(pixel_count[18]), .C(N59), .D(n84), .Z(n134) );
  IVP U51 ( .A(n135), .Z(n105) );
  AO2 U52 ( .A(n2), .B(pixel_count[17]), .C(N58), .D(n84), .Z(n135) );
  IVP U53 ( .A(n136), .Z(n106) );
  AO2 U54 ( .A(pixel_count[16]), .B(n2), .C(N57), .D(n84), .Z(n136) );
  IVP U56 ( .A(n137), .Z(n107) );
  AO2 U57 ( .A(pixel_count[15]), .B(n2), .C(N56), .D(n84), .Z(n137) );
  IVP U58 ( .A(n138), .Z(n108) );
  AO2 U59 ( .A(n2), .B(pixel_count[14]), .C(N55), .D(n84), .Z(n138) );
  IVP U60 ( .A(n139), .Z(n109) );
  AO2 U61 ( .A(n2), .B(pixel_count[13]), .C(N54), .D(n84), .Z(n139) );
  IVP U62 ( .A(n140), .Z(n110) );
  AO2 U63 ( .A(n2), .B(pixel_count[12]), .C(N53), .D(n84), .Z(n140) );
  IVP U64 ( .A(n141), .Z(n111) );
  AO2 U65 ( .A(pixel_count[11]), .B(n2), .C(N52), .D(n84), .Z(n141) );
  IVP U66 ( .A(n142), .Z(n112) );
  AO2 U67 ( .A(pixel_count[10]), .B(n2), .C(N51), .D(n84), .Z(n142) );
  IVP U68 ( .A(n143), .Z(n113) );
  AO2 U69 ( .A(pixel_count[9]), .B(n2), .C(N50), .D(n84), .Z(n143) );
  IVP U70 ( .A(n144), .Z(n114) );
  AO2 U71 ( .A(pixel_count[8]), .B(n2), .C(N49), .D(n84), .Z(n144) );
  IVP U72 ( .A(n145), .Z(n115) );
  AO2 U73 ( .A(pixel_count[7]), .B(n2), .C(N48), .D(n85), .Z(n145) );
  NR4 U74 ( .A(pixel_count[31]), .B(pixel_count[30]), .C(pixel_count[29]), .D(
        pixel_count[28]), .Z(n163) );
  NR4 U75 ( .A(pixel_count[27]), .B(pixel_count[26]), .C(pixel_count[25]), .D(
        pixel_count[24]), .Z(n162) );
  NR4 U76 ( .A(pixel_count[23]), .B(pixel_count[22]), .C(pixel_count[21]), .D(
        pixel_count[20]), .Z(n161) );
  NR4 U77 ( .A(pixel_count[19]), .B(pixel_count[18]), .C(pixel_count[17]), .D(
        pixel_count[14]), .Z(n160) );
  IVP U78 ( .A(n146), .Z(n116) );
  AO2 U79 ( .A(pixel_count[6]), .B(n2), .C(N47), .D(n85), .Z(n146) );
  IVP U80 ( .A(n147), .Z(n120) );
  AO2 U81 ( .A(pixel_count[5]), .B(n2), .C(N46), .D(n85), .Z(n147) );
  IVP U82 ( .A(n148), .Z(n164) );
  AO2 U83 ( .A(pixel_count[4]), .B(n2), .C(N45), .D(n85), .Z(n148) );
  IVP U84 ( .A(n149), .Z(n165) );
  AO2 U85 ( .A(pixel_count[3]), .B(n2), .C(N44), .D(n85), .Z(n149) );
  IVP U86 ( .A(n150), .Z(n166) );
  AO2 U87 ( .A(pixel_count[2]), .B(n2), .C(N43), .D(n85), .Z(n150) );
  IVP U88 ( .A(n151), .Z(n167) );
  AO2 U89 ( .A(pixel_count[1]), .B(n2), .C(N42), .D(n85), .Z(n151) );
  IVP U90 ( .A(n152), .Z(n90) );
  AO2 U91 ( .A(pixel_count[0]), .B(n2), .C(N41), .D(n85), .Z(n152) );
  ND4 U92 ( .A(n117), .B(n118), .C(pixel_count[0]), .D(pixel_count[10]), .Z(
        n159) );
  ND4 U93 ( .A(pixel_count[11]), .B(pixel_count[15]), .C(pixel_count[16]), .D(
        pixel_count[1]), .Z(n158) );
  ND4 U94 ( .A(pixel_count[2]), .B(pixel_count[3]), .C(pixel_count[4]), .D(
        pixel_count[5]), .Z(n157) );
  ND4 U95 ( .A(pixel_count[6]), .B(pixel_count[7]), .C(pixel_count[8]), .D(
        pixel_count[9]), .Z(n156) );
  ND2 U96 ( .A(valid_n), .B(valid), .Z(n155) );
  IVA U97 ( .A(n86), .Z(n85) );
  IVA U98 ( .A(n121), .Z(n86) );
  AN2 U99 ( .A(N19), .B(\add_67/carry[16] ), .Z(\add_67/carry[17] ) );
  EO U100 ( .A(\add_67/carry[16] ), .B(N19), .Z(N37) );
  AN2 U101 ( .A(N18), .B(\add_67/carry[15] ), .Z(\add_67/carry[16] ) );
  EO U102 ( .A(\add_67/carry[15] ), .B(N18), .Z(N36) );
  AN2 U103 ( .A(N17), .B(\add_67/carry[14] ), .Z(\add_67/carry[15] ) );
  EO U104 ( .A(\add_67/carry[14] ), .B(N17), .Z(N35) );
  AN2 U105 ( .A(N16), .B(\add_67/carry[13] ), .Z(\add_67/carry[14] ) );
  EO U106 ( .A(\add_67/carry[13] ), .B(N16), .Z(N34) );
  AN2 U107 ( .A(N15), .B(\add_67/carry[12] ), .Z(\add_67/carry[13] ) );
  EO U108 ( .A(\add_67/carry[12] ), .B(N15), .Z(N33) );
  AN2 U109 ( .A(N14), .B(\add_67/carry[11] ), .Z(\add_67/carry[12] ) );
  EO U110 ( .A(\add_67/carry[11] ), .B(N14), .Z(N32) );
  AN2 U111 ( .A(N13), .B(\add_67/carry[10] ), .Z(\add_67/carry[11] ) );
  EO U112 ( .A(\add_67/carry[10] ), .B(N13), .Z(N31) );
  AN2 U113 ( .A(N12), .B(\add_67/carry[9] ), .Z(\add_67/carry[10] ) );
  EO U114 ( .A(\add_67/carry[9] ), .B(N12), .Z(N30) );
  AN2 U115 ( .A(N9), .B(data[47]), .Z(\add_67/carry[7] ) );
  EO U116 ( .A(data[47]), .B(N9), .Z(N27) );
  AN2 U117 ( .A(\mult_67/CARRYB[8][6] ), .B(data[39]), .Z(\mult_67/A2[14] ) );
  EO U118 ( .A(data[39]), .B(\mult_67/CARRYB[8][6] ), .Z(N18) );
  AN2 U119 ( .A(N9), .B(data[34]), .Z(\mult_67/CARRYB[2][6] ) );
  EO U120 ( .A(data[34]), .B(N9), .Z(N11) );
  NR2 U121 ( .A(n3), .B(n87), .Z(N19) );
  NR2 U122 ( .A(\mult_67/A2[14] ), .B(data[40]), .Z(n87) );
  AN2 U123 ( .A(\add_67/carry[17] ), .B(n3), .Z(N38) );
endmodule


module read_mem ( clk, rst_n, valid_n, READ, addr, datain, data, data_valid );
  output [31:0] addr;
  input [31:0] datain;
  output [63:0] data;
  input clk, rst_n, valid_n;
  output READ, data_valid;
  wire   N32, N33, N34, N35, N36, N37, N38, N39, N40, N41, N42, N43, N44, N45,
         N46, N47, N48, N49, N50, N51, N52, N53, N54, N55, N56, N57, N58, N59,
         N60, N61, N62, N63, N64, N65, N66, N67, N68, N69, N70, N71, N72, N82,
         N83, N84, N85, N86, N87, N88, N89, N90, n269, n270, n271, n272, n273,
         n274, n275, n276, n277, n278, n279, n280, n281, n282, n283, n284,
         n286, n287, n288, n292, n294, n295, n296, n297, n298, n299, n300,
         n301, n302, n303, n304, n305, n306, n307, n308, n309, n310, n311,
         n312, n313, n314, n315, n316, n317, n319, n320, n353, n354, n355,
         n356, n358, n359, n361, n362, n363, n364, n365, n366, n367, n368,
         n369, n370, n371, n372, n375, n376, n377, n378, n379, n380, n381,
         n382, n383, n384, n385, n386, n387, n388, n389, n390, n391, n392,
         n393, n394, n3, n5, n7, n9, n11, n13, n154, n221, n222, n223, n224,
         n225, n226, n227, n228, n229, n230, n231, n232, n233, n234, n235,
         n236, n237, n238, n239, n240, n241, n242, n243, n244, n245, n246,
         n247, n248, n249, n250, n251, n252, n253, n254, n255, n256, n257,
         n258, n259, n260, n261, n262, n263, n264, n265, n266, n267, n268,
         n285, n289, n290, n291, n293, n318, n321, n322, n323, n324, n325,
         n326, n327, n328, n329, n330, n331, n332, n333, n334, n335, n336,
         n337, n338, n339, n340, n341, n342, n343, n344, n345, n346, n347,
         n348, n349, n350, n351, n352, n357, n360, n373, n374, n395, n396,
         n397, n398, n399, n400, n401, n402, n403, n404, n405, n406, n407,
         n408, n409, n410, n411, n412, n413, n414, n415, n416, n417, n418,
         n419, n420, n421, n422, n423, n424, n425, n426, n427, n428, n429,
         n430, n431, n432, n433, n434, n435, n436, n437, n438, n439, n440,
         n441, n442, n443, n444, n445, n446;
  wire   [3:0] state;
  wire   [31:0] index;
  wire   [8:0] x;
  wire   [8:0] y;

  AN4P U112 ( .A(x[3]), .B(x[4]), .C(x[5]), .D(x[8]), .Z(n320) );
  AN4P U168 ( .A(n363), .B(n364), .C(n365), .D(n366), .Z(n362) );
  AO1P U179 ( .A(n446), .B(n357), .C(n294), .D(n372), .Z(n371) );
  OR3 U245 ( .A(state[1]), .B(state[2]), .C(n270), .Z(n292) );
  FD2P \index_reg[0]  ( .D(n373), .CP(clk), .CD(n237), .Q(index[0]), .QN(n13)
         );
  FD2P \index_reg[1]  ( .D(n374), .CP(clk), .CD(n237), .Q(index[1]), .QN(n11)
         );
  read_mem_DW01_inc_0 add_309 ( .A(y), .SUM({N90, N89, N88, N87, N86, N85, N84, 
        N83, N82}) );
  read_mem_DW01_inc_1 add_308 ( .A(x), .SUM({N72, N71, N70, N69, N68, N67, N66, 
        N65, N64}) );
  read_mem_DW01_inc_2 add_307 ( .A(index), .SUM({N63, N62, N61, N60, N59, N58, 
        N57, N56, N55, N54, N53, N52, N51, N50, N49, N48, N47, N46, N45, N44, 
        N43, N42, N41, N40, N39, N38, N37, N36, N35, N34, N33, N32}) );
  FD2 READ_reg ( .D(n375), .CP(clk), .CD(n240), .Q(READ), .QN(n154) );
  FD1 \data_reg[63]  ( .D(n378), .CP(clk), .Q(data[63]), .QN(n271) );
  FD1 \data_reg[62]  ( .D(n379), .CP(clk), .Q(data[62]), .QN(n272) );
  FD1 \data_reg[61]  ( .D(n380), .CP(clk), .Q(data[61]), .QN(n273) );
  FD1 \data_reg[60]  ( .D(n381), .CP(clk), .Q(data[60]), .QN(n274) );
  FD1 \data_reg[59]  ( .D(n382), .CP(clk), .Q(data[59]), .QN(n275) );
  FD1 \data_reg[58]  ( .D(n383), .CP(clk), .Q(data[58]), .QN(n276) );
  FD1 \data_reg[57]  ( .D(n384), .CP(clk), .Q(data[57]), .QN(n277) );
  FD1 \data_reg[56]  ( .D(n385), .CP(clk), .Q(data[56]), .QN(n278) );
  FD1 \data_reg[55]  ( .D(n386), .CP(clk), .Q(data[55]), .QN(n279) );
  FD1 \data_reg[54]  ( .D(n387), .CP(clk), .Q(data[54]), .QN(n280) );
  FD1 \data_reg[53]  ( .D(n388), .CP(clk), .Q(data[53]), .QN(n281) );
  FD1 \data_reg[52]  ( .D(n389), .CP(clk), .Q(data[52]), .QN(n282) );
  FD1 \data_reg[51]  ( .D(n390), .CP(clk), .Q(data[51]), .QN(n283) );
  FD1 \data_reg[50]  ( .D(n391), .CP(clk), .Q(data[50]), .QN(n284) );
  FD1 \y_reg[8]  ( .D(n436), .CP(clk), .Q(y[8]) );
  FD1 \y_reg[7]  ( .D(n444), .CP(clk), .Q(y[7]) );
  FD1 \y_reg[6]  ( .D(n443), .CP(clk), .Q(y[6]) );
  FD1 \y_reg[5]  ( .D(n442), .CP(clk), .Q(y[5]) );
  FD2 data_valid_reg ( .D(n377), .CP(clk), .CD(n237), .Q(data_valid) );
  FD2 \index_reg[28]  ( .D(n421), .CP(clk), .CD(n239), .Q(index[28]) );
  FD1 \y_reg[4]  ( .D(n441), .CP(clk), .Q(y[4]) );
  FD2 \index_reg[27]  ( .D(n420), .CP(clk), .CD(n239), .Q(index[27]) );
  FDS2L \data_reg[40]  ( .CR(1'b1), .D(y[8]), .LD(n235), .CP(clk), .Q(data[40]) );
  FD1 \y_reg[3]  ( .D(n440), .CP(clk), .Q(y[3]) );
  FD1 \x_reg[3]  ( .D(n432), .CP(clk), .Q(x[3]) );
  FD1 \x_reg[4]  ( .D(n431), .CP(clk), .Q(x[4]) );
  FD1 \x_reg[5]  ( .D(n430), .CP(clk), .Q(x[5]) );
  FD1 \x_reg[8]  ( .D(n427), .CP(clk), .Q(x[8]) );
  FD1 \x_reg[6]  ( .D(n429), .CP(clk), .Q(x[6]) );
  FD1 \x_reg[7]  ( .D(n428), .CP(clk), .Q(x[7]) );
  FD2 \index_reg[26]  ( .D(n419), .CP(clk), .CD(n239), .Q(index[26]) );
  FD1 \y_reg[2]  ( .D(n439), .CP(clk), .Q(y[2]) );
  FD1 \x_reg[2]  ( .D(n433), .CP(clk), .Q(x[2]) );
  FD2 \index_reg[25]  ( .D(n418), .CP(clk), .CD(n239), .Q(index[25]) );
  FDS2L \data_reg[39]  ( .CR(1'b1), .D(y[7]), .LD(n236), .CP(clk), .Q(data[39]) );
  FD1 \y_reg[1]  ( .D(n438), .CP(clk), .Q(y[1]) );
  FD1 \x_reg[0]  ( .D(n376), .CP(clk), .Q(x[0]), .QN(n286) );
  FD1 \x_reg[1]  ( .D(n434), .CP(clk), .Q(x[1]) );
  FD2 \index_reg[24]  ( .D(n417), .CP(clk), .CD(n239), .Q(index[24]) );
  FD1 \y_reg[0]  ( .D(n437), .CP(clk), .Q(y[0]) );
  FDS2L \data_reg[49]  ( .CR(1'b1), .D(x[8]), .LD(n236), .CP(clk), .Q(data[49]) );
  FD2 \index_reg[22]  ( .D(n415), .CP(clk), .CD(n239), .Q(index[22]) );
  FD2 \index_reg[23]  ( .D(n416), .CP(clk), .CD(n239), .Q(index[23]) );
  FDS2L \data_reg[48]  ( .CR(1'b1), .D(x[7]), .LD(n235), .CP(clk), .Q(data[48]) );
  FDS2L \data_reg[47]  ( .CR(1'b1), .D(x[6]), .LD(n236), .CP(clk), .Q(data[47]) );
  FDS2L \data_reg[38]  ( .CR(1'b1), .D(y[6]), .LD(n235), .CP(clk), .Q(data[38]) );
  FD2 \state_reg[0]  ( .D(n394), .CP(clk), .CD(n237), .Q(state[0]), .QN(n270)
         );
  FD2 \index_reg[21]  ( .D(n414), .CP(clk), .CD(n239), .Q(index[21]) );
  FD2 \state_reg[1]  ( .D(n392), .CP(clk), .CD(n237), .Q(state[1]), .QN(n5) );
  FD2 \state_reg[2]  ( .D(n393), .CP(clk), .CD(n237), .Q(state[2]), .QN(n269)
         );
  FD2 \index_reg[20]  ( .D(n413), .CP(clk), .CD(n239), .Q(index[20]) );
  FD2 \index_reg[19]  ( .D(n412), .CP(clk), .CD(n238), .Q(index[19]) );
  FDS2L \data_reg[37]  ( .CR(1'b1), .D(y[5]), .LD(n236), .CP(clk), .Q(data[37]) );
  FD2 \index_reg[18]  ( .D(n411), .CP(clk), .CD(n238), .Q(index[18]) );
  FD2 \index_reg[17]  ( .D(n410), .CP(clk), .CD(n238), .Q(index[17]) );
  FDS2L \data_reg[36]  ( .CR(1'b1), .D(y[4]), .LD(n235), .CP(clk), .Q(data[36]) );
  FD2 \index_reg[16]  ( .D(n409), .CP(clk), .CD(n238), .Q(index[16]) );
  FDS2L \data_reg[33]  ( .CR(1'b1), .D(y[1]), .LD(n234), .CP(clk), .Q(data[33]) );
  FD2 \index_reg[15]  ( .D(n408), .CP(clk), .CD(n238), .Q(index[15]) );
  FDS2L \data_reg[35]  ( .CR(1'b1), .D(y[3]), .LD(n236), .CP(clk), .Q(data[35]) );
  FD2 \index_reg[14]  ( .D(n407), .CP(clk), .CD(n238), .Q(index[14]) );
  FDS2L \data_reg[34]  ( .CR(1'b1), .D(y[2]), .LD(n235), .CP(clk), .Q(data[34]) );
  FD2 \index_reg[13]  ( .D(n406), .CP(clk), .CD(n238), .Q(index[13]), .QN(n287) );
  FDS2L \data_reg[32]  ( .CR(1'b1), .D(y[0]), .LD(n235), .CP(clk), .Q(data[32]) );
  FD2 \index_reg[12]  ( .D(n405), .CP(clk), .CD(n238), .Q(index[12]), .QN(n288) );
  FD2 \index_reg[11]  ( .D(n404), .CP(clk), .CD(n238), .Q(index[11]) );
  FD2 \index_reg[10]  ( .D(n403), .CP(clk), .CD(n238), .Q(index[10]) );
  FD2 \index_reg[9]  ( .D(n402), .CP(clk), .CD(n238), .Q(index[9]) );
  FD2 \index_reg[8]  ( .D(n401), .CP(clk), .CD(n238), .Q(index[8]) );
  FD2 \index_reg[7]  ( .D(n400), .CP(clk), .CD(n237), .Q(index[7]) );
  FD2 \index_reg[6]  ( .D(n399), .CP(clk), .CD(n237), .Q(index[6]) );
  FD2 \index_reg[5]  ( .D(n398), .CP(clk), .CD(n237), .Q(index[5]) );
  FDS2L \addr_reg[0]  ( .CR(1'b1), .D(index[0]), .LD(n3), .CP(clk), .Q(addr[0]) );
  FDS2L \addr_reg[2]  ( .CR(1'b1), .D(index[2]), .LD(n3), .CP(clk), .Q(addr[2]) );
  FDS2L \addr_reg[3]  ( .CR(1'b1), .D(index[3]), .LD(n3), .CP(clk), .Q(addr[3]) );
  FDS2L \addr_reg[4]  ( .CR(1'b1), .D(index[4]), .LD(n3), .CP(clk), .Q(addr[4]) );
  FDS2L \addr_reg[5]  ( .CR(1'b1), .D(index[5]), .LD(n3), .CP(clk), .Q(addr[5]) );
  FDS2L \addr_reg[6]  ( .CR(1'b1), .D(index[6]), .LD(n3), .CP(clk), .Q(addr[6]) );
  FDS2L \addr_reg[7]  ( .CR(1'b1), .D(index[7]), .LD(n3), .CP(clk), .Q(addr[7]) );
  FDS2L \addr_reg[8]  ( .CR(1'b1), .D(index[8]), .LD(n3), .CP(clk), .Q(addr[8]) );
  FDS2L \addr_reg[9]  ( .CR(1'b1), .D(index[9]), .LD(n3), .CP(clk), .Q(addr[9]) );
  FDS2L \addr_reg[10]  ( .CR(1'b1), .D(index[10]), .LD(n3), .CP(clk), .Q(
        addr[10]) );
  FDS2L \addr_reg[11]  ( .CR(1'b1), .D(index[11]), .LD(n3), .CP(clk), .Q(
        addr[11]) );
  FDS2L \addr_reg[12]  ( .CR(1'b1), .D(index[12]), .LD(n3), .CP(clk), .Q(
        addr[12]) );
  FDS2L \addr_reg[13]  ( .CR(1'b1), .D(index[13]), .LD(n3), .CP(clk), .Q(
        addr[13]) );
  FDS2L \addr_reg[14]  ( .CR(1'b1), .D(index[14]), .LD(n3), .CP(clk), .Q(
        addr[14]) );
  FDS2L \addr_reg[15]  ( .CR(1'b1), .D(index[15]), .LD(n3), .CP(clk), .Q(
        addr[15]) );
  FDS2L \addr_reg[16]  ( .CR(1'b1), .D(index[16]), .LD(n3), .CP(clk), .Q(
        addr[16]) );
  FDS2L \addr_reg[17]  ( .CR(1'b1), .D(index[17]), .LD(n3), .CP(clk), .Q(
        addr[17]) );
  FDS2L \addr_reg[18]  ( .CR(1'b1), .D(index[18]), .LD(n3), .CP(clk), .Q(
        addr[18]) );
  FDS2L \addr_reg[19]  ( .CR(1'b1), .D(index[19]), .LD(n3), .CP(clk), .Q(
        addr[19]) );
  FDS2L \addr_reg[20]  ( .CR(1'b1), .D(index[20]), .LD(n3), .CP(clk), .Q(
        addr[20]) );
  FDS2L \addr_reg[21]  ( .CR(1'b1), .D(index[21]), .LD(n3), .CP(clk), .Q(
        addr[21]) );
  FDS2L \addr_reg[22]  ( .CR(1'b1), .D(index[22]), .LD(n3), .CP(clk), .Q(
        addr[22]) );
  FDS2L \addr_reg[23]  ( .CR(1'b1), .D(index[23]), .LD(n3), .CP(clk), .Q(
        addr[23]) );
  FDS2L \addr_reg[24]  ( .CR(1'b1), .D(index[24]), .LD(n3), .CP(clk), .Q(
        addr[24]) );
  FDS2L \addr_reg[25]  ( .CR(1'b1), .D(index[25]), .LD(n3), .CP(clk), .Q(
        addr[25]) );
  FDS2L \addr_reg[26]  ( .CR(1'b1), .D(index[26]), .LD(n3), .CP(clk), .Q(
        addr[26]) );
  FDS2L \addr_reg[27]  ( .CR(1'b1), .D(index[27]), .LD(n3), .CP(clk), .Q(
        addr[27]) );
  FDS2L \addr_reg[28]  ( .CR(1'b1), .D(index[28]), .LD(n3), .CP(clk), .Q(
        addr[28]) );
  FDS2L \addr_reg[29]  ( .CR(1'b1), .D(index[29]), .LD(n3), .CP(clk), .Q(
        addr[29]) );
  FDS2L \addr_reg[30]  ( .CR(1'b1), .D(index[30]), .LD(n3), .CP(clk), .Q(
        addr[30]) );
  FDS2L \addr_reg[31]  ( .CR(1'b1), .D(index[31]), .LD(n3), .CP(clk), .Q(
        addr[31]) );
  FDS2L \addr_reg[1]  ( .CR(1'b1), .D(index[1]), .LD(n3), .CP(clk), .Q(addr[1]) );
  FDS2L \data_reg[45]  ( .CR(1'b1), .D(x[4]), .LD(n236), .CP(clk), .Q(data[45]) );
  FDS2L \data_reg[43]  ( .CR(1'b1), .D(x[2]), .LD(n236), .CP(clk), .Q(data[43]) );
  FDS2L \data_reg[41]  ( .CR(1'b1), .D(x[0]), .LD(n236), .CP(clk), .Q(data[41]) );
  FDS2L \data_reg[31]  ( .CR(1'b1), .D(datain[31]), .LD(n234), .CP(clk), .Q(
        data[31]) );
  FDS2L \data_reg[29]  ( .CR(1'b1), .D(datain[29]), .LD(n236), .CP(clk), .Q(
        data[29]) );
  FDS2L \data_reg[27]  ( .CR(1'b1), .D(datain[27]), .LD(n234), .CP(clk), .Q(
        data[27]) );
  FDS2L \data_reg[25]  ( .CR(1'b1), .D(datain[25]), .LD(n234), .CP(clk), .Q(
        data[25]) );
  FDS2L \data_reg[23]  ( .CR(1'b1), .D(datain[23]), .LD(n236), .CP(clk), .Q(
        data[23]) );
  FDS2L \data_reg[21]  ( .CR(1'b1), .D(datain[21]), .LD(n234), .CP(clk), .Q(
        data[21]) );
  FDS2L \data_reg[19]  ( .CR(1'b1), .D(datain[19]), .LD(n236), .CP(clk), .Q(
        data[19]) );
  FDS2L \data_reg[17]  ( .CR(1'b1), .D(datain[17]), .LD(n234), .CP(clk), .Q(
        data[17]) );
  FDS2L \data_reg[15]  ( .CR(1'b1), .D(datain[15]), .LD(n236), .CP(clk), .Q(
        data[15]) );
  FDS2L \data_reg[13]  ( .CR(1'b1), .D(datain[13]), .LD(n234), .CP(clk), .Q(
        data[13]) );
  FDS2L \data_reg[11]  ( .CR(1'b1), .D(datain[11]), .LD(n236), .CP(clk), .Q(
        data[11]) );
  FDS2L \data_reg[9]  ( .CR(1'b1), .D(datain[9]), .LD(n234), .CP(clk), .Q(
        data[9]) );
  FDS2L \data_reg[7]  ( .CR(1'b1), .D(datain[7]), .LD(n236), .CP(clk), .Q(
        data[7]) );
  FDS2L \data_reg[5]  ( .CR(1'b1), .D(datain[5]), .LD(n234), .CP(clk), .Q(
        data[5]) );
  FDS2L \data_reg[3]  ( .CR(1'b1), .D(datain[3]), .LD(n236), .CP(clk), .Q(
        data[3]) );
  FDS2L \data_reg[1]  ( .CR(1'b1), .D(datain[1]), .LD(n236), .CP(clk), .Q(
        data[1]) );
  FDS2L \data_reg[0]  ( .CR(1'b1), .D(datain[0]), .LD(n236), .CP(clk), .Q(
        data[0]) );
  FDS2L \data_reg[2]  ( .CR(1'b1), .D(datain[2]), .LD(n236), .CP(clk), .Q(
        data[2]) );
  FDS2L \data_reg[4]  ( .CR(1'b1), .D(datain[4]), .LD(n236), .CP(clk), .Q(
        data[4]) );
  FDS2L \data_reg[6]  ( .CR(1'b1), .D(datain[6]), .LD(n236), .CP(clk), .Q(
        data[6]) );
  FDS2L \data_reg[8]  ( .CR(1'b1), .D(datain[8]), .LD(n236), .CP(clk), .Q(
        data[8]) );
  FDS2L \data_reg[10]  ( .CR(1'b1), .D(datain[10]), .LD(n235), .CP(clk), .Q(
        data[10]) );
  FDS2L \data_reg[12]  ( .CR(1'b1), .D(datain[12]), .LD(n235), .CP(clk), .Q(
        data[12]) );
  FDS2L \data_reg[14]  ( .CR(1'b1), .D(datain[14]), .LD(n235), .CP(clk), .Q(
        data[14]) );
  FDS2L \data_reg[16]  ( .CR(1'b1), .D(datain[16]), .LD(n235), .CP(clk), .Q(
        data[16]) );
  FDS2L \data_reg[18]  ( .CR(1'b1), .D(datain[18]), .LD(n235), .CP(clk), .Q(
        data[18]) );
  FDS2L \data_reg[20]  ( .CR(1'b1), .D(datain[20]), .LD(n235), .CP(clk), .Q(
        data[20]) );
  FDS2L \data_reg[22]  ( .CR(1'b1), .D(datain[22]), .LD(n235), .CP(clk), .Q(
        data[22]) );
  FDS2L \data_reg[24]  ( .CR(1'b1), .D(datain[24]), .LD(n235), .CP(clk), .Q(
        data[24]) );
  FDS2L \data_reg[26]  ( .CR(1'b1), .D(datain[26]), .LD(n235), .CP(clk), .Q(
        data[26]) );
  FDS2L \data_reg[28]  ( .CR(1'b1), .D(datain[28]), .LD(n235), .CP(clk), .Q(
        data[28]) );
  FDS2L \data_reg[30]  ( .CR(1'b1), .D(datain[30]), .LD(n235), .CP(clk), .Q(
        data[30]) );
  FDS2L \data_reg[42]  ( .CR(1'b1), .D(x[1]), .LD(n235), .CP(clk), .Q(data[42]) );
  FDS2L \data_reg[44]  ( .CR(1'b1), .D(x[3]), .LD(n235), .CP(clk), .Q(data[44]) );
  FDS2L \data_reg[46]  ( .CR(1'b1), .D(x[5]), .LD(n235), .CP(clk), .Q(data[46]) );
  FD2 \index_reg[3]  ( .D(n396), .CP(clk), .CD(rst_n), .Q(index[3]) );
  FD2 \index_reg[29]  ( .D(n422), .CP(clk), .CD(rst_n), .Q(index[29]) );
  FD2 \index_reg[2]  ( .D(n395), .CP(clk), .CD(rst_n), .Q(index[2]), .QN(n9)
         );
  FD2 \index_reg[30]  ( .D(n423), .CP(clk), .CD(rst_n), .Q(index[30]) );
  FD2 \index_reg[31]  ( .D(n424), .CP(clk), .CD(rst_n), .Q(index[31]) );
  FD2 \index_reg[4]  ( .D(n397), .CP(clk), .CD(n237), .Q(index[4]) );
  NR2 U85 ( .A(n241), .B(n292), .Z(n3) );
  OR2P U86 ( .A(n241), .B(n353), .Z(n7) );
  IVP U87 ( .A(n230), .Z(n227) );
  IVDA U88 ( .A(n297), .Z(n223) );
  IVDA U89 ( .A(n297), .Z(n224) );
  IVP U90 ( .A(n307), .Z(n435) );
  IVP U91 ( .A(n233), .Z(n231) );
  IVDA U92 ( .A(n310), .Z(n221) );
  IVDA U93 ( .A(n310), .Z(n222) );
  IVP U94 ( .A(n230), .Z(n228) );
  IVP U95 ( .A(n7), .Z(n235) );
  IVP U96 ( .A(n233), .Z(n232) );
  IVP U97 ( .A(n7), .Z(n234) );
  NR2 U98 ( .A(n227), .B(n241), .Z(n307) );
  AO7 U99 ( .A(n445), .B(n306), .C(n307), .Z(n297) );
  IVP U100 ( .A(n308), .Z(n445) );
  IVP U101 ( .A(n225), .Z(n226) );
  IVP U102 ( .A(n296), .Z(n225) );
  NR2 U103 ( .A(n223), .B(n306), .Z(n296) );
  ND2 U104 ( .A(n292), .B(n356), .Z(n294) );
  AN3 U105 ( .A(n240), .B(n308), .C(n231), .Z(n310) );
  IVP U106 ( .A(n371), .Z(n426) );
  ND2 U107 ( .A(index[30]), .B(n229), .Z(n246) );
  ND2 U108 ( .A(n244), .B(n243), .Z(n424) );
  ND2 U109 ( .A(index[31]), .B(n229), .Z(n244) );
  ND2 U110 ( .A(index[29]), .B(n229), .Z(n248) );
  ND2 U111 ( .A(index[28]), .B(n229), .Z(n250) );
  ND2 U113 ( .A(index[27]), .B(n229), .Z(n252) );
  ND2 U114 ( .A(index[26]), .B(n229), .Z(n254) );
  ND2 U115 ( .A(index[25]), .B(n228), .Z(n256) );
  ND2 U116 ( .A(index[24]), .B(n228), .Z(n258) );
  ND2 U117 ( .A(index[23]), .B(n228), .Z(n260) );
  ND2 U118 ( .A(index[22]), .B(n228), .Z(n262) );
  ND2 U119 ( .A(index[21]), .B(n228), .Z(n264) );
  ND2 U120 ( .A(index[20]), .B(n228), .Z(n266) );
  ND2 U121 ( .A(index[19]), .B(n228), .Z(n268) );
  ND2 U122 ( .A(index[18]), .B(n228), .Z(n289) );
  ND2 U123 ( .A(index[17]), .B(n228), .Z(n291) );
  ND2 U124 ( .A(index[16]), .B(n228), .Z(n318) );
  ND2 U125 ( .A(index[15]), .B(n228), .Z(n322) );
  ND2 U126 ( .A(index[14]), .B(n228), .Z(n324) );
  ND2 U127 ( .A(index[13]), .B(n227), .Z(n326) );
  ND2 U128 ( .A(index[12]), .B(n227), .Z(n328) );
  AO7 U129 ( .A(n351), .B(state[1]), .C(n306), .Z(n350) );
  ND2 U130 ( .A(n5), .B(n242), .Z(n306) );
  NR2 U131 ( .A(state[0]), .B(n269), .Z(n242) );
  ND2 U132 ( .A(index[11]), .B(n227), .Z(n330) );
  IVP U133 ( .A(n295), .Z(n444) );
  AO2 U134 ( .A(N89), .B(n226), .C(y[7]), .D(n224), .Z(n295) );
  IVP U135 ( .A(n298), .Z(n443) );
  AO2 U136 ( .A(N88), .B(n226), .C(y[6]), .D(n223), .Z(n298) );
  IVP U137 ( .A(n299), .Z(n442) );
  AO2 U138 ( .A(N87), .B(n226), .C(y[5]), .D(n224), .Z(n299) );
  IVP U139 ( .A(n300), .Z(n441) );
  AO2 U140 ( .A(N86), .B(n226), .C(y[4]), .D(n223), .Z(n300) );
  IVP U141 ( .A(n301), .Z(n440) );
  AO2 U142 ( .A(N85), .B(n226), .C(y[3]), .D(n224), .Z(n301) );
  IVP U143 ( .A(n302), .Z(n439) );
  AO2 U144 ( .A(N84), .B(n226), .C(y[2]), .D(n223), .Z(n302) );
  IVP U145 ( .A(n303), .Z(n438) );
  AO2 U146 ( .A(N83), .B(n226), .C(y[1]), .D(n224), .Z(n303) );
  IVP U147 ( .A(n304), .Z(n437) );
  AO2 U148 ( .A(N82), .B(n226), .C(y[0]), .D(n223), .Z(n304) );
  IVP U149 ( .A(n305), .Z(n436) );
  AO2 U150 ( .A(N90), .B(n226), .C(y[8]), .D(n224), .Z(n305) );
  ND2 U151 ( .A(index[10]), .B(n227), .Z(n332) );
  ND2 U152 ( .A(index[9]), .B(n227), .Z(n334) );
  ND2 U153 ( .A(index[8]), .B(n227), .Z(n336) );
  IVP U154 ( .A(n309), .Z(n427) );
  AO2 U155 ( .A(n435), .B(x[8]), .C(N72), .D(n222), .Z(n309) );
  IVP U156 ( .A(n311), .Z(n428) );
  AO2 U157 ( .A(n435), .B(x[7]), .C(N71), .D(n221), .Z(n311) );
  IVP U158 ( .A(n312), .Z(n429) );
  AO2 U159 ( .A(n435), .B(x[6]), .C(N70), .D(n222), .Z(n312) );
  IVP U160 ( .A(n313), .Z(n430) );
  AO2 U161 ( .A(n435), .B(x[5]), .C(N69), .D(n221), .Z(n313) );
  IVP U162 ( .A(n314), .Z(n431) );
  AO2 U163 ( .A(n435), .B(x[4]), .C(N68), .D(n222), .Z(n314) );
  IVP U164 ( .A(n315), .Z(n432) );
  AO2 U165 ( .A(n435), .B(x[3]), .C(N67), .D(n221), .Z(n315) );
  IVP U166 ( .A(n316), .Z(n433) );
  AO2 U167 ( .A(n435), .B(x[2]), .C(N66), .D(n222), .Z(n316) );
  IVP U169 ( .A(n317), .Z(n434) );
  AO2 U170 ( .A(n435), .B(x[1]), .C(N65), .D(n221), .Z(n317) );
  ND2 U171 ( .A(index[7]), .B(n227), .Z(n338) );
  EON1 U172 ( .A(n307), .B(n286), .C(N64), .D(n221), .Z(n376) );
  IVP U173 ( .A(valid_n), .Z(n446) );
  ND2 U174 ( .A(n306), .B(n353), .Z(n372) );
  ND4 U175 ( .A(x[2]), .B(x[1]), .C(n319), .D(n320), .Z(n308) );
  NR3 U176 ( .A(x[6]), .B(x[7]), .C(n286), .Z(n319) );
  ND2 U177 ( .A(state[1]), .B(n357), .Z(n356) );
  ND2 U178 ( .A(index[6]), .B(n227), .Z(n340) );
  AO4 U180 ( .A(n353), .B(n371), .C(n426), .D(n269), .Z(n393) );
  AO4 U181 ( .A(n426), .B(n270), .C(n358), .D(n371), .Z(n394) );
  NR2 U182 ( .A(n359), .B(n357), .Z(n358) );
  AO6 U183 ( .A(n361), .B(n362), .C(n306), .Z(n359) );
  AO3 U184 ( .A(n292), .B(n371), .C(n355), .D(n356), .Z(n392) );
  ND2 U185 ( .A(state[1]), .B(n371), .Z(n355) );
  ND3 U186 ( .A(state[0]), .B(n269), .C(state[1]), .Z(n353) );
  ND2 U187 ( .A(index[5]), .B(n227), .Z(n342) );
  ND2 U188 ( .A(index[4]), .B(n227), .Z(n344) );
  ND2 U189 ( .A(index[3]), .B(n227), .Z(n346) );
  AO7 U190 ( .A(n350), .B(n9), .C(n347), .Z(n395) );
  AO7 U191 ( .A(n350), .B(n11), .C(n348), .Z(n374) );
  ND2 U192 ( .A(N33), .B(n231), .Z(n348) );
  AO7 U193 ( .A(n350), .B(n13), .C(n349), .Z(n373) );
  ND2 U194 ( .A(N32), .B(n231), .Z(n349) );
  NR4 U195 ( .A(n367), .B(n368), .C(n369), .D(n370), .Z(n361) );
  ND4 U196 ( .A(index[8]), .B(index[9]), .C(index[10]), .D(index[11]), .Z(n368) );
  ND4 U197 ( .A(index[4]), .B(index[5]), .C(index[6]), .D(index[7]), .Z(n369)
         );
  ND4 U198 ( .A(index[15]), .B(index[16]), .C(n288), .D(n287), .Z(n367) );
  NR4 U199 ( .A(index[14]), .B(index[17]), .C(index[18]), .D(index[19]), .Z(
        n366) );
  NR4 U200 ( .A(index[20]), .B(index[21]), .C(index[22]), .D(index[23]), .Z(
        n365) );
  NR4 U201 ( .A(index[24]), .B(index[25]), .C(index[26]), .D(index[27]), .Z(
        n364) );
  NR4 U202 ( .A(index[28]), .B(index[29]), .C(index[30]), .D(index[31]), .Z(
        n363) );
  ND4 U203 ( .A(index[0]), .B(index[1]), .C(index[2]), .D(index[3]), .Z(n370)
         );
  AO4 U204 ( .A(n292), .B(n425), .C(n154), .D(n294), .Z(n375) );
  IVP U205 ( .A(n294), .Z(n425) );
  NR2 U206 ( .A(n284), .B(n235), .Z(n391) );
  NR2 U207 ( .A(n283), .B(n235), .Z(n390) );
  NR2 U208 ( .A(n282), .B(n234), .Z(n389) );
  NR2 U209 ( .A(n281), .B(n234), .Z(n388) );
  NR2 U210 ( .A(n280), .B(n234), .Z(n387) );
  NR2 U211 ( .A(n279), .B(n234), .Z(n386) );
  NR2 U212 ( .A(n278), .B(n234), .Z(n385) );
  NR2 U213 ( .A(n277), .B(n234), .Z(n384) );
  NR2 U214 ( .A(n276), .B(n234), .Z(n383) );
  NR2 U215 ( .A(n275), .B(n234), .Z(n382) );
  NR2 U216 ( .A(n274), .B(n234), .Z(n381) );
  NR2 U217 ( .A(n273), .B(n234), .Z(n380) );
  NR2 U218 ( .A(n272), .B(n234), .Z(n379) );
  NR2 U219 ( .A(n271), .B(n234), .Z(n378) );
  ND2 U220 ( .A(n353), .B(n354), .Z(n377) );
  ND2 U221 ( .A(n306), .B(data_valid), .Z(n354) );
  IVA U222 ( .A(n230), .Z(n229) );
  IVA U223 ( .A(n352), .Z(n230) );
  IVA U224 ( .A(n360), .Z(n233) );
  IVA U225 ( .A(n7), .Z(n236) );
  IVA U226 ( .A(n241), .Z(n237) );
  IVA U227 ( .A(n241), .Z(n238) );
  IVA U228 ( .A(n241), .Z(n239) );
  IVA U229 ( .A(n241), .Z(n240) );
  IVP U230 ( .A(rst_n), .Z(n241) );
  OR2 U231 ( .A(state[0]), .B(state[2]), .Z(n351) );
  IVA U232 ( .A(n350), .Z(n352) );
  IVA U233 ( .A(n306), .Z(n360) );
  ND2 U234 ( .A(N63), .B(n231), .Z(n243) );
  ND2 U235 ( .A(N62), .B(n231), .Z(n245) );
  ND2 U236 ( .A(n246), .B(n245), .Z(n423) );
  ND2 U237 ( .A(N61), .B(n231), .Z(n247) );
  ND2 U238 ( .A(n248), .B(n247), .Z(n422) );
  ND2 U239 ( .A(N60), .B(n231), .Z(n249) );
  ND2 U240 ( .A(n250), .B(n249), .Z(n421) );
  ND2 U241 ( .A(N59), .B(n231), .Z(n251) );
  ND2 U242 ( .A(n252), .B(n251), .Z(n420) );
  ND2 U243 ( .A(N58), .B(n231), .Z(n253) );
  ND2 U244 ( .A(n254), .B(n253), .Z(n419) );
  ND2 U246 ( .A(N57), .B(n231), .Z(n255) );
  ND2 U247 ( .A(n256), .B(n255), .Z(n418) );
  ND2 U248 ( .A(N56), .B(n231), .Z(n257) );
  ND2 U249 ( .A(n258), .B(n257), .Z(n417) );
  ND2 U250 ( .A(N55), .B(n231), .Z(n259) );
  ND2 U251 ( .A(n260), .B(n259), .Z(n416) );
  ND2 U252 ( .A(N54), .B(n231), .Z(n261) );
  ND2 U253 ( .A(n262), .B(n261), .Z(n415) );
  ND2 U254 ( .A(N53), .B(n231), .Z(n263) );
  ND2 U255 ( .A(n264), .B(n263), .Z(n414) );
  ND2 U256 ( .A(N52), .B(n231), .Z(n265) );
  ND2 U257 ( .A(n266), .B(n265), .Z(n413) );
  ND2 U258 ( .A(N51), .B(n231), .Z(n267) );
  ND2 U259 ( .A(n268), .B(n267), .Z(n412) );
  ND2 U260 ( .A(N50), .B(n231), .Z(n285) );
  ND2 U261 ( .A(n289), .B(n285), .Z(n411) );
  ND2 U262 ( .A(N49), .B(n231), .Z(n290) );
  ND2 U263 ( .A(n291), .B(n290), .Z(n410) );
  ND2 U264 ( .A(N48), .B(n231), .Z(n293) );
  ND2 U265 ( .A(n318), .B(n293), .Z(n409) );
  ND2 U266 ( .A(N47), .B(n231), .Z(n321) );
  ND2 U267 ( .A(n322), .B(n321), .Z(n408) );
  ND2 U268 ( .A(N46), .B(n231), .Z(n323) );
  ND2 U269 ( .A(n324), .B(n323), .Z(n407) );
  ND2 U270 ( .A(N45), .B(n231), .Z(n325) );
  ND2 U271 ( .A(n326), .B(n325), .Z(n406) );
  ND2 U272 ( .A(N44), .B(n232), .Z(n327) );
  ND2 U273 ( .A(n328), .B(n327), .Z(n405) );
  ND2 U274 ( .A(N43), .B(n232), .Z(n329) );
  ND2 U275 ( .A(n330), .B(n329), .Z(n404) );
  ND2 U276 ( .A(N42), .B(n232), .Z(n331) );
  ND2 U277 ( .A(n332), .B(n331), .Z(n403) );
  ND2 U278 ( .A(N41), .B(n232), .Z(n333) );
  ND2 U279 ( .A(n334), .B(n333), .Z(n402) );
  ND2 U280 ( .A(N40), .B(n232), .Z(n335) );
  ND2 U281 ( .A(n336), .B(n335), .Z(n401) );
  ND2 U282 ( .A(N39), .B(n232), .Z(n337) );
  ND2 U283 ( .A(n338), .B(n337), .Z(n400) );
  ND2 U284 ( .A(N38), .B(n232), .Z(n339) );
  ND2 U285 ( .A(n340), .B(n339), .Z(n399) );
  ND2 U286 ( .A(N37), .B(n232), .Z(n341) );
  ND2 U287 ( .A(n342), .B(n341), .Z(n398) );
  ND2 U288 ( .A(N36), .B(n232), .Z(n343) );
  ND2 U289 ( .A(n344), .B(n343), .Z(n397) );
  ND2 U290 ( .A(N35), .B(n232), .Z(n345) );
  ND2 U291 ( .A(n346), .B(n345), .Z(n396) );
  ND2 U292 ( .A(N34), .B(n232), .Z(n347) );
  IVA U293 ( .A(n351), .Z(n357) );
endmodule


module rotate ( clk, rst_n, valid_n, ready_n, angle, WRITE0, WRITE1, READ0, 
        READ1, addr_wr0, addr_wr1, addr_rd0, addr_rd1, datain0, datain1, 
        dataout0, dataout1 );
  input [7:0] angle;
  output [31:0] addr_wr0;
  output [31:0] addr_wr1;
  output [31:0] addr_rd0;
  output [31:0] addr_rd1;
  output [31:0] datain0;
  output [31:0] datain1;
  input [31:0] dataout0;
  input [31:0] dataout1;
  input clk, rst_n, valid_n;
  output ready_n, WRITE0, WRITE1, READ0, READ1;
  wire   data_valid, data_valid2, data_valid3, N126, n6, n7, n8, n9, n10, n11,
         n12, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28,
         n29, n30, n31, n32, n33, n34, n35, n36, n37, n39, n40, n41;
  wire   [4:0] state;
  wire   [31:0] pixel_count2;
  wire   [63:0] data;
  wire   [31:0] pixel_count;
  wire   [63:0] data2;
  wire   [63:0] data3;
  wire   SYNOPSYS_UNCONNECTED__0, SYNOPSYS_UNCONNECTED__1, 
        SYNOPSYS_UNCONNECTED__2, SYNOPSYS_UNCONNECTED__3, 
        SYNOPSYS_UNCONNECTED__4, SYNOPSYS_UNCONNECTED__5, 
        SYNOPSYS_UNCONNECTED__6, SYNOPSYS_UNCONNECTED__7, 
        SYNOPSYS_UNCONNECTED__8, SYNOPSYS_UNCONNECTED__9, 
        SYNOPSYS_UNCONNECTED__10, SYNOPSYS_UNCONNECTED__11, 
        SYNOPSYS_UNCONNECTED__12, SYNOPSYS_UNCONNECTED__13, 
        SYNOPSYS_UNCONNECTED__14, SYNOPSYS_UNCONNECTED__15, 
        SYNOPSYS_UNCONNECTED__16, SYNOPSYS_UNCONNECTED__17, 
        SYNOPSYS_UNCONNECTED__18, SYNOPSYS_UNCONNECTED__19, 
        SYNOPSYS_UNCONNECTED__20, SYNOPSYS_UNCONNECTED__21, 
        SYNOPSYS_UNCONNECTED__22, SYNOPSYS_UNCONNECTED__23, 
        SYNOPSYS_UNCONNECTED__24, SYNOPSYS_UNCONNECTED__25, 
        SYNOPSYS_UNCONNECTED__26, SYNOPSYS_UNCONNECTED__27, 
        SYNOPSYS_UNCONNECTED__28, SYNOPSYS_UNCONNECTED__29, 
        SYNOPSYS_UNCONNECTED__30, SYNOPSYS_UNCONNECTED__31, 
        SYNOPSYS_UNCONNECTED__32, SYNOPSYS_UNCONNECTED__33, 
        SYNOPSYS_UNCONNECTED__34, SYNOPSYS_UNCONNECTED__35, 
        SYNOPSYS_UNCONNECTED__36, SYNOPSYS_UNCONNECTED__37, 
        SYNOPSYS_UNCONNECTED__38, SYNOPSYS_UNCONNECTED__39, 
        SYNOPSYS_UNCONNECTED__40;
  assign addr_wr0[31] = 1'b0;
  assign addr_wr0[30] = 1'b0;
  assign addr_wr0[29] = 1'b0;
  assign addr_wr0[28] = 1'b0;
  assign addr_wr0[27] = 1'b0;
  assign addr_wr0[26] = 1'b0;
  assign addr_wr0[25] = 1'b0;
  assign addr_wr0[24] = 1'b0;
  assign addr_wr0[23] = 1'b0;
  assign addr_wr0[22] = 1'b0;
  assign addr_wr0[21] = 1'b0;
  assign addr_wr0[20] = 1'b0;
  assign addr_wr0[19] = 1'b0;
  assign addr_wr0[17] = 1'b1;
  assign addr_wr1[31] = 1'b0;
  assign addr_wr1[30] = 1'b0;
  assign addr_wr1[29] = 1'b0;
  assign addr_wr1[28] = 1'b0;
  assign addr_wr1[27] = 1'b0;
  assign addr_wr1[26] = 1'b0;
  assign addr_wr1[25] = 1'b0;
  assign addr_wr1[24] = 1'b0;
  assign addr_wr1[23] = 1'b0;
  assign addr_wr1[22] = 1'b0;
  assign addr_wr1[21] = 1'b0;
  assign addr_wr1[20] = 1'b0;
  assign addr_wr1[19] = 1'b0;

  NR16P U25 ( .I0(pixel_count[31]), .I1(pixel_count[30]), .I10(pixel_count[29]), .I11(pixel_count[28]), .I12(pixel_count[27]), .I13(pixel_count[26]), .I14(
        pixel_count[25]), .I15(pixel_count[24]), .I2(pixel_count[23]), .I3(
        pixel_count[22]), .I4(pixel_count[21]), .I5(pixel_count[20]), .I6(
        pixel_count[19]), .I7(pixel_count[18]), .I8(pixel_count[17]), .I9(
        pixel_count[14]), .Z(n28) );
  OR4 U37 ( .A(pixel_count2[12]), .B(n9), .C(n20), .D(n21), .Z(n17) );
  read_mem read_orig ( .clk(clk), .rst_n(rst_n), .valid_n(valid_n), .READ(
        READ0), .addr(addr_rd0), .datain(dataout0), .data(data), .data_valid(
        data_valid) );
  write_mem_scratch_20000 write_scratch ( .clk(clk), .valid_n(valid_n), .data(
        data), .valid(data_valid), .WRITE(WRITE0), .addr({
        SYNOPSYS_UNCONNECTED__0, SYNOPSYS_UNCONNECTED__1, 
        SYNOPSYS_UNCONNECTED__2, SYNOPSYS_UNCONNECTED__3, 
        SYNOPSYS_UNCONNECTED__4, SYNOPSYS_UNCONNECTED__5, 
        SYNOPSYS_UNCONNECTED__6, SYNOPSYS_UNCONNECTED__7, 
        SYNOPSYS_UNCONNECTED__8, SYNOPSYS_UNCONNECTED__9, 
        SYNOPSYS_UNCONNECTED__10, SYNOPSYS_UNCONNECTED__11, 
        SYNOPSYS_UNCONNECTED__12, addr_wr0[18], SYNOPSYS_UNCONNECTED__13, 
        addr_wr0[16:0]}), .datain(datain0), .pixel_count(pixel_count) );
  read_mem_20000 read_scratch ( .clk(clk), .rst_n(rst_n), .valid_n(N126), 
        .READ(READ1), .addr(addr_rd1), .datain(dataout1), .data(data2), 
        .data_valid(data_valid2) );
  compute_xy_center rotated_scratch ( .clk(clk), .valid_n(valid_n), .data(
        data2), .angle(angle), .data_valid(data_valid2), .data2({
        SYNOPSYS_UNCONNECTED__14, SYNOPSYS_UNCONNECTED__15, 
        SYNOPSYS_UNCONNECTED__16, SYNOPSYS_UNCONNECTED__17, 
        SYNOPSYS_UNCONNECTED__18, SYNOPSYS_UNCONNECTED__19, 
        SYNOPSYS_UNCONNECTED__20, SYNOPSYS_UNCONNECTED__21, 
        SYNOPSYS_UNCONNECTED__22, SYNOPSYS_UNCONNECTED__23, 
        SYNOPSYS_UNCONNECTED__24, SYNOPSYS_UNCONNECTED__25, 
        SYNOPSYS_UNCONNECTED__26, SYNOPSYS_UNCONNECTED__27, data3[49:0]}), 
        .data_valid2(data_valid3), .pixel_count(pixel_count2) );
  write_mem write_orig ( .clk(clk), .data({1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 
        1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, 1'b0, data3[49:0]}), .valid(
        data_valid3), .WRITE(WRITE1), .addr({SYNOPSYS_UNCONNECTED__28, 
        SYNOPSYS_UNCONNECTED__29, SYNOPSYS_UNCONNECTED__30, 
        SYNOPSYS_UNCONNECTED__31, SYNOPSYS_UNCONNECTED__32, 
        SYNOPSYS_UNCONNECTED__33, SYNOPSYS_UNCONNECTED__34, 
        SYNOPSYS_UNCONNECTED__35, SYNOPSYS_UNCONNECTED__36, 
        SYNOPSYS_UNCONNECTED__37, SYNOPSYS_UNCONNECTED__38, 
        SYNOPSYS_UNCONNECTED__39, SYNOPSYS_UNCONNECTED__40, addr_wr1[18:0]}), 
        .datain(datain1) );
  FD4 ready_n_reg ( .D(n33), .CP(clk), .SD(rst_n), .Q(ready_n) );
  FD2 \state_reg[0]  ( .D(n35), .CP(clk), .CD(rst_n), .Q(state[0]), .QN(n7) );
  FD2 \state_reg[1]  ( .D(n34), .CP(clk), .CD(rst_n), .Q(state[1]), .QN(n6) );
  AN2P U38 ( .A(n37), .B(n8), .Z(n36) );
  OR4P U39 ( .A(n16), .B(n17), .C(n18), .D(n19), .Z(n37) );
  IVP U40 ( .A(n10), .Z(n39) );
  ND3 U41 ( .A(n26), .B(n27), .C(n28), .Z(N126) );
  ND2 U42 ( .A(n36), .B(n12), .Z(n10) );
  ND2 U43 ( .A(n40), .B(n41), .Z(n12) );
  IVP U44 ( .A(valid_n), .Z(n40) );
  IVP U45 ( .A(n11), .Z(n41) );
  ND2 U46 ( .A(pixel_count2[10]), .B(pixel_count2[0]), .Z(n20) );
  ND4 U47 ( .A(pixel_count2[1]), .B(pixel_count2[16]), .C(pixel_count2[15]), 
        .D(pixel_count2[11]), .Z(n21) );
  ND2 U48 ( .A(state[0]), .B(n6), .Z(n9) );
  ND4 U49 ( .A(n22), .B(n23), .C(n24), .D(n25), .Z(n16) );
  NR4 U50 ( .A(pixel_count2[18]), .B(pixel_count2[17]), .C(pixel_count2[14]), 
        .D(pixel_count2[13]), .Z(n22) );
  NR4 U51 ( .A(pixel_count2[22]), .B(pixel_count2[21]), .C(pixel_count2[20]), 
        .D(pixel_count2[19]), .Z(n23) );
  NR4 U52 ( .A(pixel_count2[26]), .B(pixel_count2[25]), .C(pixel_count2[24]), 
        .D(pixel_count2[23]), .Z(n24) );
  NR5 U53 ( .A(pixel_count2[28]), .B(pixel_count2[27]), .C(pixel_count2[29]), 
        .D(pixel_count2[31]), .E(pixel_count2[30]), .Z(n25) );
  AO4 U54 ( .A(n39), .B(n9), .C(n6), .D(n10), .Z(n34) );
  AO4 U55 ( .A(n39), .B(n11), .C(n7), .D(n10), .Z(n35) );
  NR4 U56 ( .A(n29), .B(n30), .C(pixel_count[13]), .D(pixel_count[12]), .Z(n27) );
  ND2 U57 ( .A(pixel_count[10]), .B(pixel_count[0]), .Z(n30) );
  ND4 U58 ( .A(pixel_count[1]), .B(pixel_count[16]), .C(pixel_count[15]), .D(
        pixel_count[11]), .Z(n29) );
  NR2 U59 ( .A(n31), .B(n32), .Z(n26) );
  ND4 U60 ( .A(pixel_count[5]), .B(pixel_count[4]), .C(pixel_count[3]), .D(
        pixel_count[2]), .Z(n31) );
  ND4 U61 ( .A(pixel_count[9]), .B(pixel_count[8]), .C(pixel_count[7]), .D(
        pixel_count[6]), .Z(n32) );
  EON1 U62 ( .A(n8), .B(n36), .C(ready_n), .D(n36), .Z(n33) );
  ND4 U63 ( .A(pixel_count2[9]), .B(pixel_count2[8]), .C(pixel_count2[7]), .D(
        pixel_count2[6]), .Z(n19) );
  ND4 U64 ( .A(pixel_count2[5]), .B(pixel_count2[4]), .C(pixel_count2[3]), .D(
        pixel_count2[2]), .Z(n18) );
  ND2 U65 ( .A(n7), .B(n6), .Z(n11) );
  ND2 U66 ( .A(state[1]), .B(n7), .Z(n8) );
endmodule

